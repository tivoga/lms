<?php
App::import('Vendor', 'isdk', array('file' => 'infusionsoft' . DS . 'isdk.php'));
/**
 * TODO:
 * need an importer to get in the latest n000 contacts.
*/


/**
 * Import Contacts Shell.
 *
 * imports all data from infusion soft data base given the id to
 * begin from if no id is given or is Null, the function will import
 * the set number of contacts ($import_on_empty) data from infusion soft.
 *
 * we request $Max_Results number of contatcs from inf.s at a time and
 * and process them untill the time we find a contact in the data base.
 * If we find that a contact exists in the database we stop the process of
 * importing.
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

class ImportContactsShell extends AppShell {
    public $uses = array('Contact', 'CustomField',
                         'SegregationRule', 'CustomFieldValue');

    private $num_digits_to_remove =2;
    private $Max_Results = 50; //per page
    private $import_on_empty = 3000; // no of contacts to import when db is clean
    private $cust_fields = [];
    private $rule_data=[];
    private $seg_rules;

    public function main() {
        $conn = new iSDK();
        $conn->cfgCon("infusion_befault");
//        $this->loadModel('CustomField');
//        $this->loadModel('SegregationRule');
//        $this->loadModel('CustomFieldValue');

        $fields_array =[];
        $this->cust_fields = $this->CustomField->find('all');
        foreach ($this->cust_fields as $field){
            array_push($fields_array,'_'.$field['CustomField']['name']);
        }
        $return_fields = $this->Contact->INFSCoulmuns;
        $return_fields = array_merge($return_fields,$fields_array);
        $max_results =$this->Max_Results;
        $page =0;
        $query = array('Id' => '%');
        $loop = true;
        $lastCreated = $this->Contact->find('first', array(
            'order' => array('Contact.Id' => 'desc'),
            'fields' => array('Contact.Id')
        ));
        $this->rule_data =[];
        $this->seg_rules = $this->SegregationRule->find('all',array('order' =>
            array('SegregationRule.over_ride DESC', 'SegregationRule.id DESC')));
        do{
            $contacts = $conn->
                dsQueryOrderBy("Contact", $max_results, $page, $query, $return_fields, 'Id', false); //, false
            foreach($contacts as &$contact){
                if(!$this->segregate_contact($contact)){
                    $loop= false;
                    break;
                }
            }
            if(!$lastCreated and $page>=($this->import_on_empty/$this->Max_Results))$loop=false;
            $page++;
//            $loop = false;
        }while ($loop);

        $this->out(count($contacts));

    }
/**
 * Legacy code used to import contacts - convaluted and complicted.
 *
*/

//    private function import_first_nthousand($n){
//        $lastCreated = $this->Contact->find('first', array(
//            'order' => array('Contact.Id' => 'desc'),
//            'fields' => array('Contact.Id')
//        ));
////        if(!$lastCreated){
//        $this->import_first_fivethousand();
//        $this->out('imported contacts');
////            exit;
////        }
//        $conn = new iSDK();
//        $conn->cfgCon("infusion_befault");
////        $this->loadModel('CustomField');
////        $this->loadModel('SegregationRule');
////        $this->loadModel('CustomFieldValue');
//
//        $fields_array =[];
//        $this->cust_fields = $this->CustomField->find('all');
//        foreach ($this->cust_fields as $field){
//            array_push($fields_array,'_'.$field['CustomField']['name']);
//        }
//        $last_id = $lastCreated['Contact']['Id'];
//        $last_id = substr("$last_id", 0, -1);
////        $return_fields = array_keys($this->Contact->getColumnTypes());
////        $this->out($return_fields);
//        // inf.s needs to be told what fields to return.
//        $return_fields = $this->Contact->INFSCoulmuns;
//        $return_fields = array_merge($return_fields,$fields_array);
//        $max_results =$this->Max_Results;
//        $page = 0;
//        $contacts_success = [];
//        $ret_cont = 0;
//        $save_count = 0;
//        $this->rule_data =[];
//        $this->seg_rules = $this->SegregationRule->find('all',array('order' =>
//            array('SegregationRule.over_ride DESC', 'SegregationRule.id DESC')));
//        do {
//            $repeat = FALSE;
//            $query = array('Id' => $last_id.'%');
////            $query = array('Id' => '%');
////            $this->out($query);
//            $contacts = $conn->dsQueryOrderBy("Contact", $max_results, $page, $query, $return_fields, 'Id'); //, false
////            $this->out(count($contacts));
//            $ret_cont += count($contacts);
//            $page++;
//            $contact=null;
//            foreach($contacts as &$contact){
//                $contact['DateCreated'] = DboSource::expression('NOW()');
//                if ($this->Contact->exists($contact['Id'])){
//                    $this->out('contactid: '.$contact['Id'].' already exists');
//                    continue;
//                }
//                $this->Contact->create();
//                foreach( $seg_rules as $mainquery){
//                    $query = $mainquery['SegregationRule']['query'];
//                    // each segregation rule may have multiple query sets separated by 'OR'
//                    $all_query =explode('OR',$query);
//                    foreach($all_query as $each_query){
//                        $query_set = explode('\n',$each_query);
//                        // each of those query sets may have multiple queries separated by a newline (\n)
//                        $and_flag = True;
//                        foreach ($query_set as $rule){ //'table = [\'va l\', \'ue\']'
//                            // this is the structure of each query - field operator value
//                            preg_match('/(?P<name>\w+) (?P<operator>\W+) (?P<value>.*)/', $rule , $matches);
//                            $matches['value'] = str_replace('[','',$matches['value']);
//                            $matches['value'] = str_replace(']','',$matches['value']);
//                            $matches['value'] = str_replace('\'','',$matches['value']);
//                            $matches['value'] = explode(', ',$matches['value']);
//                            $rule_data = array_merge($rule_data, $matches);
//                            if (array_key_exists('_'.$matches['name'],$contact))
//                                if (($matches['operator'] == '=' and in_array($contact['_'.$matches['name']], $matches['value']))
//                                    or ($matches['operator'] == '!=' and !in_array($contact['_'.$matches['name']], $matches['value']))
//                                    or ($matches['operator'] == '<' and (int)$contact['_'.$matches['name']] < (int)$matches['value'])
//                                    or ($matches['operator'] == '>' and (int)$contact['_'.$matches['name']] > (int)$matches['value'])
//                                    //TODO: A better way of doing the operator matchings.
//                                ){
//                                    // we have a match: we do nothing. we let the
//                                    //and_flag (compound match) as is and just move on to the next statement
//                                    continue;
//                                }
//                            // no match found. which means this query_set will not be true.
//                            // set the and_flag to false and break the loop.-
//                            // move on to the next OR structure.
//                            $and_flag = False;
//                            break;
//                        }
//                        // if the loop made it all the way and a queryset has found this contact true,
//                        // theres no need for us to run any further rules on this contact.
//                        // assign the contacts user_id and break to the outer most loop. - next contact.
//                        if ($and_flag){
//                            $contact['user_id'] = $mainquery['SegregationRule']['user_id'];
//                            break 2;
//                        }
//                    }
//                }
////                $this->checkGroup($contact);
//
//                if ($this->Contact->save($contact)){
//                    $this->out('saved contact: '.$contact['Id']);
////                    $this->Session->setFlash(__('The contact has been saved.'));
//                }
//                foreach ($cust_fields as $field){
//                    if (array_key_exists('_'.$field['CustomField']['name'], $contact)){
//                        $this->CustomFieldValue->create();
//                        $this->CustomFieldValue->save(['contactId' => $contact['Id'],
//                            'custom_field_id' => $field['CustomField']['id'],
//                            'value'=> $contact['_'.$field['CustomField']['name']]]);
//                    }
//                }
//            }
//            if ($contact and substr($contact['Id'], -1) >= 8  ){
//                $page =0;
//                $last_id = substr($contact['Id'] , 0, -1)+1;
//                $repeat = TRUE;
//                $this->out('redo');
//            }
//        }while (count($contacts) == $max_results || $repeat);
////        $this->out($contacts);
//    }

/**
 * The section that actually segregates and saves the contact.
 * All the necessary ingredients are pass as class variables.
 *
 * returns 0 if the contact exists in the database and 1 for
 * a successful save.
 *
*/
    private function segregate_contact($contact){
        if ($this->Contact->exists($contact['Id'])){
            $this->out('contactid: '.$contact['Id'].' already exists');
            return 0;
        }
        $contact['DateCreated'] = DboSource::expression('NOW()');
        $this->Contact->create();
        foreach( $this->seg_rules as $mainquery){
            $query = $mainquery['SegregationRule']['query'];
            // each segregation rule may have multiple query sets separated by 'OR'
            $all_query =explode('OR',$query);
            foreach($all_query as $each_query){
                $query_set = explode('\n',$each_query);
                // each of those query sets may have multiple queries separated by a newline (\n)
                $and_flag = True;
                foreach ($query_set as $rule){ //'table = [\'va l\', \'ue\']'
                    // this is the structure of each query - field operator value
                    preg_match('/(?P<name>\w+) (?P<operator>\W+) (?P<value>.*)/', $rule , $matches);
                    $matches['value'] = str_replace('[','',$matches['value']);
                    $matches['value'] = str_replace(']','',$matches['value']);
                    $matches['value'] = str_replace('\'','',$matches['value']);
                    $matches['value'] = explode(', ',$matches['value']);
                    $this->rule_data = array_merge($this->rule_data, $matches);
                    if (array_key_exists('_'.$matches['name'],$contact))
                        if (($matches['operator'] == '=' and in_array($contact['_'.$matches['name']], $matches['value']))
                            or ($matches['operator'] == '!=' and !in_array($contact['_'.$matches['name']], $matches['value']))
                            or ($matches['operator'] == '<' and (int)$contact['_'.$matches['name']] < (int)$matches['value'])
                            or ($matches['operator'] == '>' and (int)$contact['_'.$matches['name']] > (int)$matches['value'])
                            //TODO: A better way of doing the operator matchings.
                        ){
                            // we have a match: we do nothing. we let the
                            //and_flag (compound match) as is and just move on to the next statement
                            continue;
                        }
                    // no match found. which means this query_set will not be true.
                    // set the and_flag to false and break the loop.-
                    // move on to the next OR structure.
                    $and_flag = False;
                    break;
                }
                // if the loop made it all the way and a queryset has found this contact true,
                // theres no need for us to run any further rules on this contact.
                // assign the contacts user_id and break to the outer most loop. - next contact.
                if ($and_flag){
                    $contact['user_id'] = $mainquery['SegregationRule']['user_id'];
                    break 2;
                }
            }
        }
//                $this->checkGroup($contact);

        if ($this->Contact->save($contact)){
            $this->out('saved contact: '.$contact['Id']);
//                    $this->Session->setFlash(__('The contact has been saved.'));
        }
        foreach ($this->cust_fields as $field){
            if (array_key_exists('_'.$field['CustomField']['name'], $contact)){
                $this->CustomFieldValue->create();
                $this->CustomFieldValue->save(['contactId' => $contact['Id'],
                    'custom_field_id' => $field['CustomField']['id'],
                    'value'=> $contact['_'.$field['CustomField']['name']]]);
            }
        }
        return 1;
    }

/**
 * checking if the contact can be grouped
 * based on their email domain
 *
 * Not used any more: function shifted to model save function.
 *
 */
//    private function checkGroup($contact){
//        $domain = substr(strrchr($contact['Email'], "@"), 1);
//        $this->out($domain);
//        $group_members = $this->Contact->find('all',array(
//            'conditions' => array('Contact.email LIKE' => "%$domain%")));
//        foreach($group_members as $g){
//            $this->out($g['Email']);
//        }
//        if($group_members){
//            $this->loadModel('ContactGroup');
//            $group = $this->ContactGroup->find('first',array(
//                'conditions' => array('ContactGroup.group' => $domain)));
//            if(! $group){
//                $this->ContactGroup->create();
//                $group = $this->ContactGroup->save(array('group'=>$domain));
//            }
//            $this->Contact->updateAll(array('Contact.group' => $group), array('Contact.email LIKE' => "%$domain%"));
//        }
//    }
}
