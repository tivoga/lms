<?php
App::uses('AppModel', 'Model');
/**
 * EmailTemplate Model
 *
 * @property User $User
 */
class EmailTemplate extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'subject';


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
