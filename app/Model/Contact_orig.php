<?php
App::uses('AppModel', 'Model');
App::import('model','SegregationRule');
/**
 * Contact Model
 *
 */
class Contact extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Contacts';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'Id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'FirstName';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'Id' => array(
			'naturalNumber' => array(
				'rule' => array('naturalNumber'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'customfield' => array(
            'className' => 'CustomFieldValue',
            'foreignKey' => 'contactId',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function getContactByCustomValues($customFieldName, $fieldValues){
        if(gettype($fieldValues) == 'array'){
            $fieldValues = implode('\', \'', $fieldValues);
        }
        $query = 'select Contact.* from Contacts as Contact, custom_field_values, custom_fields
                where Contact.Id = custom_field_values.contactId and
                                    custom_field_values.custom_field_id = custom_fields.Id and
                custom_fields.name = \''.$customFieldName.'\' and value in (\''. $fieldValues.'\' );';
        return $this->query($query);
//        return $this->find('all', compact('conditions'));
    }

}
