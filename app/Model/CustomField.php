<?php
App::uses('AppModel', 'Model');
/**
 * CustomField Model
 *
 */
class CustomField extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'Label';

    public function get_industry_options(){
        $ind = $this->find('first',array(
            'conditions' => array('CustomField.id' => 54)));
        $ind = explode("\n", trim($ind['CustomField']['Values']));
        return $ind;
    }
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'Id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
