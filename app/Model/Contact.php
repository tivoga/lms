<?php
App::uses('AppModel', 'Model');
App::uses('ContactGroup', 'Model');
/**
 * Contact Model
 *
 * @property Communication $Communication
 * @property Email $Email
 * @property ProductFieldsValue $ProductFieldsValue
 * @property UploadedFile $UploadedFile
 * @property Product $Product
 */
class Contact extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Contacts';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'Id';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'FirstName';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
        'ContactsProduct' => array(
            'className' => 'ContactsProduct',
            'foreignKey' => 'contact_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
		'Communication' => array(
			'className' => 'Communication',
			'foreignKey' => 'contact_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Email' => array(
			'className' => 'Email',
			'foreignKey' => 'contact_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ProductFieldsValue' => array(
			'className' => 'ProductFieldsValue',
			'foreignKey' => 'contact_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UploadedFile' => array(
			'className' => 'UploadedFile',
			'foreignKey' => 'contact_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'customfield' => array(
            'className' => 'CustomFieldValue',
            'foreignKey' => 'contactId',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
	);

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Group' => array(
            'className' => 'ContactGroup',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )

    );



/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Product' => array(
			'className' => 'Product',
			'joinTable' => 'Contacts_products',
			'foreignKey' => 'contact_id',
			'associationForeignKey' => 'product_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

    public $INFSCoulmuns = array(
        'Id', 'JobTitle', 'LastName', 'FirstName', 'MiddleName', 'Nickname',
        'OwnerID', 'Groups', 'LastUpdated', 'LastUpdatedBy', 'Leadsource',
        'LeadSourceId', 'Phone1', 'Phone1Ext', 'Phone1Type', 'Phone2',
        'Phone2Ext', 'Phone2Type', 'Phone3', 'Phone3Ext', 'Phone3Type', 'Phone4',
        'Phone4Ext', 'Phone4Type', 'Phone5', 'Phone5Ext', 'Phone5Type',
        'PostalCode', 'PostalCode2', 'PostalCode3', 'ReferralCode', 'SpouseName',
        'State', 'State2', 'State3', 'StreetAddress1', 'StreetAddress2', 'Suffix',
        'Title', 'Username', 'Validated', 'Website', 'Address1Type',
        'Address2Street1', 'Address2Street2', 'Address2Type', 'Address3Street1',
        'Address3Street2', 'Address3Type', 'Anniversary', 'AssistantName',
        'AssistantPhone', 'BillingInformation', 'Birthday', 'City', 'City2',
        'City3', 'Company', 'AccountId', 'CompanyID', 'ContactNotes',
        'ContactType', 'Country', 'Country2', 'Country3', 'CreatedBy',
        'DateCreated', 'Email', 'EmailAddress2', 'EmailAddress3', 'Fax1', 'Fax1Type', 'Fax2', 'Fax2Type', 'ZipFour1', 'ZipFour2', 'ZipFour3'
    );

    public $exceptedDomains = array('gmail.com','yahoo.com','hotmail.com','sky.com','live.co.uk');

    public function beforeSave($options=array()) {
//        return true;
//        debug($this->data);
        if(!array_key_exists('DateCreated',$this->data['Contact'])){
            $this->data['Contact']['DateCreated']= DboSource::expression('NOW()');
        }

        $this->recursive = -1;
        // check to see if the contact has email in the details
        $domain = substr(strrchr($this->data['Contact']['Email'], "@"), 1);
        $domain = $domain;
        // if one of the free domains, dont bother
        if (array_key_exists($domain, $this->exceptedDomains)) return true;
        // lets see if any more contacts have the same mail domain.
        $group_members = $this->find('count',array(
            'conditions' => array('Contact.email LIKE' => "%@.$domain%")));
//        debug($group_members);
        if($group_members){
            //yes we do. the count can be 1 or more.
            // lets see if there's a group with the domain name
            $cg = new ContactGroup();
            $group = $cg->find('first',array(
                'conditions' => array('ContactGroup.name' => $domain)));
            if(! $group){
                // no there isn't. create and assign all contacts with the
                // email domains to this group
                $cg->create();
                if(!$cg->save(array('name'=>$domain))){
                    // sorry something went wrong.
                    if($this->Session){
                        $this->Session->setFlash(__('Un able to create New Contact Group'));
                    }else{
//                        $this->out('unable to create new contact group');
                    }
                }else{
                    // successfully created the contact group. Now get all contacts
                    // of this domain to the group
                    $this->updateAll(array('Contact.group_id' => $cg->getLastInsertId(),
                            'Contact.gp_primary' => 0),
                        array('Contact.email LIKE' => "%$domain%"));
                    $this->data['Contact']['group_id'] = $cg->getLastInsertId();
                }
            }else{
                // yes there is a group just assign the group id to the contact.
                // but this being the latest being added to the contact this becomes the
                // group primary so set the rest
                $this->data['Contact']['group_id'] = $group['ContactGroup']['id'];
                $this->updateAll(array('Contact.gp_primary' => 0),
                    array('Contact.email LIKE' => "%$domain%"));
            }
        }
        // $changed_fields is an array of fields that changed
        return true;
    }
}
