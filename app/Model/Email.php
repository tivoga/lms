<?php
App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');

App::uses('Contact', 'Model');
App::uses('UploadedFile', 'Model');
/**
 * Email Model
 *
 * @property User $User
 * @property Contact $Contact
 * @property UploadedFile $UploadedFile
 */
class Email extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'UploadedFile' => array(
			'className' => 'UploadedFile',
			'joinTable' => 'emails_uploaded_files',
			'foreignKey' => 'email_id',
			'associationForeignKey' => 'uploaded_file_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

    public function beforeSave($options=array()){
        $user = CakeSession::read("Auth.User");
        $Email = new CakeEmail('sevron');
        $contact = new Contact();
        $conts=$contact->find('first',array(
            'conditions' => array('Contact.id' => $this->data['Email']['contact_id'])));
//        debug($conts);
        $upf = new UploadedFile();
        if($this->data['UploadedFile']['UploadedFile']){
            $upf_res = $upf->find('all',array(
                'conditions' => array('UploadedFile.id' => $this->data['UploadedFile']['UploadedFile'])));
        }
        $att =[];
        foreach($upf_res as $file){
            $att[$file['UploadedFile']['filename']] =$file['UploadedFile']['location'];
        }
//        debug($att);

        $Email->from(array($user['senders_email'] =>'Sevron' ))
            ->replyTo($user['senders_email'])

            ->to($conts['Contact']['Email'])
            ->subject($this->data['Email']['subject'])
            ->emailformat('html');
        if($att){
            $Email->attachments($att);
        }
        $Email->send($this->data['Email']['body']);
//        debug($this->data);
//        debug($Email);
        return true;
    }
}
