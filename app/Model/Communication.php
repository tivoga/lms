<?php
App::uses('AppModel', 'Model');
/**
 * Communication Model
 *
 * @property User $User
 * @property Contact $Contact
 */
class Communication extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

    public function get_enum_communication_types(){
        return array('Support', 'Accounts', 'Training', 'Sales', 'Other');
    }
}
