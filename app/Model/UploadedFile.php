<?php
App::uses('AppModel', 'Model');
/**
 * UploadedFile Model
 *
 * @property User $User
 * @property Contact $Contact
 * @property Email $Email
 */
class UploadedFile extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'filename';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Email' => array(
			'className' => 'Email',
			'joinTable' => 'emails_uploaded_files',
			'foreignKey' => 'uploaded_file_id',
			'associationForeignKey' => 'email_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
