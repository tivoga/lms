
<?php
App::uses('AppModel', 'Model');
App::uses('Contact', 'Model');
App::uses('CustomField', 'Model');
/**
 * Postcodelatlng Model
 *
 */
class Postcodelatlng extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'postcodelatlng';

    /**
     * Custom function to help the proximity maps get all contacts
     * with in the range and criterion submitted.
     *
     * We Filter the contacts based on the conditions, get their
     * addresses and filter the ones which are with in the
     * distance range
    */
    public function get_postalcodes_in_range(){
        $data = Router::getRequest()->data;
        $code =$data['Proximity']['code'];
        $code = str_replace(" ", "", $code);
        $range =$data['Proximity']['distance'];
        $product =$data['Proximity']['product'];
        $industry =$data['Proximity']['industry'];
        $contract =$data['Proximity']['contract'];
        $user =$data['Proximity']['user'];

        $Contact = new Contact();
        $options = array(
            'conditions'=>array('Contact.Zipfour1 is NOT NULL'),
            'fields' => array('DISTINCT Contact.Zipfour1'));
        if($user){$options['conditions']['Contact.user_id'] = $user;}
        $options['joins']=array();
        $cond=array();
        if($product or $contract){
            array_push($cond,'ContactsProduct.contact_id = Contact.Id');
            if($product){
                $cond['ContactsProduct.product_id']=$product;
            }
            if($contract){
                $cond['ContactsProduct.contract_status']=$contract;
            }
            array_push($options['joins'],array('table' => 'Contacts_products',
                'alias' => 'ContactsProduct',
                'type' => 'INNER',
                'conditions' => $cond
            ));

        }
        if($industry){
            $customProd = new CustomField();
            $ind_sel = $customProd->get_industry_options();
            $ind_sel = $ind_sel[$industry];
            array_push($options['joins'],array('table' => 'custom_field_values',
                'alias' => 'ContactsCustomValues',
                'type' => 'INNER',
                'conditions' => array(
                    'ContactsCustomValues.contactId = Contact.Id',
                    'ContactsCustomValues.custom_field_id = 54',
                    'ContactsCustomValues.value = \''.$ind_sel.'\''
                )
            ));
        }

        $z_codes = $Contact->find('all', $options);
        $zip_list = array();
        foreach ($z_codes as $zip){
            array_push($zip_list,$zip['Contact']['Zipfour1']);
        }
//        debug($options);
        $code_values = $this->find('first',array('conditions'=>array('postcode'=>$code)));
        if(!$code_values)return;
        $query = 'SELECT *, ( 3959 * acos( cos( radians('.$code_values['Postcodelatlng']['latitude'].
            ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$code_values['Postcodelatlng']['longitude'].
            ') ) + sin( radians('.$code_values['Postcodelatlng']['latitude'].
            ') ) * sin( radians( latitude ) ) ) ) AS distance FROM postcodelatlng where postcode in(\''.implode('\', \'',$zip_list).'\')HAVING distance < '.$range.'  ORDER BY distance;';
//        debug($query);
//        debug($this->query($query) );
        return [$this->query($query),$code_values] ;
//        return $this->find('all', compact('conditions'));
    }
}