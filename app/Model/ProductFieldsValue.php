<?php
App::uses('AppModel', 'Model');
/**
 * ProductFieldsValue Model
 *
 * @property ProductField $ProductField
 * @property Contact $Contact
 */
class ProductFieldsValue extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'ProductField' => array(
			'className' => 'ProductField',
			'foreignKey' => 'product_field_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
