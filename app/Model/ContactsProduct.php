<?php
App::uses('AppModel', 'Model');
/**
 * ContactsProduct Model
 *
 * @property Contact $Contact
 * @property Product $Product
 */
class ContactsProduct extends AppModel {

/**
 *
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'Contacts_products';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'contact_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'product_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'sales_person',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
//    public $virtualFields = array(
//        'contract_end' => 'contract_start + interval contract_period year'
//    );
    public function get_enum_contract_status(){
        return array(null, 'Prospect', 'Customer', 'Cancelled', 'Ex Customer');
    }

    public function get_enum_contract_length(){
        return array(null,'1', '2',  '3', '4', '5');
    }

    /**
 * getActiveCustomersDueForRenewal method
 * used to get all active contacts (contract status = 2 for customer)
 * and their due for renewal in three months.
 */
    public function getActiveCustomersDueForRenewal(){
//        bebug('here');
//        $query ="select Contact.*, ContactsProduct.*  from Contacts as Contact, Contacts_products as ContactsProduct
//            where ContactsProduct.contract_start + interval ContactsProduct.contract_period year > curdate()
//            and Contact.Id = ContactsProduct.contact_id";
        $query ="SELECT * FROM Contacts_products;";
        bebug($this->query($query));
        return;
    }

    public function renewalMail_sent($id_){
        $obj = $this->find('first', array('conditions'=>array($this->alias.'.'.$this->primaryKey => $id_)));
        if($obj){
            $obj[$this->alias]['renew_letter_sent']=true;
            $this->save($obj);
            return true;
        }
        return false;
    }

    public function moved_to_cancel($id_){
        $obj = $this->find('first', array('conditions'=>array($this->alias.'.'.$this->primaryKey => $id_)));
        if($obj){
            $obj[$this->alias]['contract_status']=3;
            $this->save($obj);
            return true;
        }
        return false;
    }

    public function beforeSave($options=array()) {
//        return true;
//        debug($this->data);
        $this->recursive = -1;
//        $domain = $this->data['ContactsProduct']['Email'];
        $this->old = $this->find('first', array('conditions'=>array($this->alias.'.'.$this->primaryKey => $this->id)));
        if ($this->old){
            if ($this->old[$this->alias]['contract_start'] != $this->data[$this->alias]['contract_start'] or
                $this->old[$this->alias]['contract_period'] != $this->data[$this->alias]['contract_period']) {
                // we've determined that the contract has been changed..
//                $this->data[$this->alias]['contract_end'] = date("Y-m-d", strtotime($this->data[$this->alias]['contract_start'] .
//                    '+ '.$this->data[$this->alias]['contract_period']. 'year'));
                $this->data[$this->alias]['renew_letter_sent']=false;
            }
        }
//        debug($this->);
        // $changed_fields is an array of fields that changed
        return true;
    }
}
