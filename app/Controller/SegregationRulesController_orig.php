<?php
App::uses('AppController', 'Controller');
/**
 * SegregationRules Controller
 *
 * Admin CRUD views
 * @property SegregationRule $SegregationRule
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SegregationRulesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->SegregationRule->recursive = 0;
		$this->set('segregationRules', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->SegregationRule->exists($id)) {
			throw new NotFoundException(__('Invalid segregation rule'));
		}
		$options = array('conditions' => array('SegregationRule.' . $this->SegregationRule->primaryKey => $id));
		$this->set('segregationRule', $this->SegregationRule->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
        if ($this->request->is('post')) {
			$this->SegregationRule->create();
			if ($this->SegregationRule->save($this->request->data)) {
				$this->Session->setFlash(__('The segregation rule has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The segregation rule could not be saved. Please, try again.'));
			}
		}
        $this->loadModel('CustomField');
        $this->set('fields', $this->CustomField->find('all'));
        $user_group = $this->SegregationRule->User->find('list');
        $this->set(compact('user_group'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->SegregationRule->exists($id)) {
			throw new NotFoundException(__('Invalid segregation rule'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SegregationRule->save($this->request->data)) {
				$this->Session->setFlash(__('The segregation rule has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The segregation rule could not be saved. Please, try again.'));
			}
		} else {
            $this->loadModel('CustomField');
            $this->set('fields', $this->CustomField->find('all'));
			$options = array('conditions' => array('SegregationRule.' . $this->SegregationRule->primaryKey => $id));
			$this->request->data = $this->SegregationRule->find('first', $options);
            $user_group = $this->SegregationRule->User->find('list');
            $this->set(compact('user_group'));
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->SegregationRule->id = $id;
		if (!$this->SegregationRule->exists()) {
			throw new NotFoundException(__('Invalid segregation rule'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->SegregationRule->delete()) {
			$this->Session->setFlash(__('The segregation rule has been deleted.'));
		} else {
			$this->Session->setFlash(__('The segregation rule could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
