<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Emails Controller
 *
 * @property Email $Email
 * @property PaginatorComponent $Paginator
 */
class EmailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Email->recursive = 0;
		$this->set('emails', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Email->exists($id)) {
			throw new NotFoundException(__('Invalid email'));
		}
		$options = array('conditions' => array('Email.' . $this->Email->primaryKey => $id));
		$this->set('email', $this->Email->find('first', $options));
	}

    private function load_add_email_options(){
        $this->loadModel('EmailTemplate');
        $email_templates = $this->EmailTemplate->find('all');
        $options = array('conditions' => array('Contact.Id' => $this->params->named['contact_id']));
        $contacts = $this->Email->Contact->find('first',$options);
        $options = array('conditions' => array(
            'OR'=>array('UploadedFile.contact_id is null',
                'UploadedFile.contact_id' => $this->params->named['contact_id'])));
        $uploadedFiles = $this->Email->UploadedFile->find('list',$options);
        $this->set('templates', $email_templates);
        $this->set(compact('contacts', 'uploadedFiles'));
    }
/**
 * add method
 *
 * @return void
 */
	public function add() {
        if ($this->request->is('post')) {
            $this->Email->create();
            if ($this->Email->save($this->request->data)) {
                $this->Session->setFlash(__('The email has been saved.'));
                return $this->redirect(array('controller' =>'Contacts','action' => 'view',$this->params->named['contact_id']));
            } else {
                $this->Session->setFlash(__('The email could not be saved. Please, try again.'));
            }
        }
        $this->load_add_email_options();

    }

    public function add_invoice(){
        if ($this->request->is('post')) {
            $this->Email->create();
            if ($this->Email->save($this->request->data)) {
                $this->loadModel('ContactsProduct');
//                $this->ContactsProduct->renewalMail_sent($this->params->named['contract_id']);
//                TODO: update the contract that a renewal proposal has been sent.
//                $this->params->named['contract_id']
                $this->Session->setFlash(__('The email has been Sent.'));
                return $this->redirect(array('controller' =>'Contacts','action' => 'inv_reports'));
            } else {
                $this->Session->setFlash(__('The email could not be saved. Please, try again.'));
            }
        }
        $this->load_add_email_options();
        $this->set('page_title','Send Invoice');
        $this->render('add_renew');
    }

/**
 * add method
 *
 * @return void
 */
    public function add_renew() {
        if ($this->request->is('post')) {
            $this->Email->create();
            if ($this->Email->save($this->request->data)) {
                $this->loadModel('ContactsProduct');
                $this->ContactsProduct->renewalMail_sent($this->params->named['contract_id']);
//                TODO: update the contract that a renewal proposal has been sent.
//                $this->params->named['contract_id']
                $this->Session->setFlash(__('The Renewal email has been Sent.'));
                return $this->redirect(array('controller' =>'Contacts','action' => 'view',$this->params->named['contact_id']));
            } else {
                $this->Session->setFlash(__('The email could not be saved. Please, try again.'));
            }
        }
        $this->load_add_email_options();
        $this->set('page_title','Send Renewal Email');
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Email->exists($id)) {
			throw new NotFoundException(__('Invalid email'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Email->save($this->request->data)) {
				$this->Session->setFlash(__('The email has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Email.' . $this->Email->primaryKey => $id));
			$this->request->data = $this->Email->find('first', $options);
		}
		$users = $this->Email->User->find('list');
		$contacts = $this->Email->Contact->find('list');
		$uploadedFiles = $this->Email->UploadedFile->find('list');
		$this->set(compact('users', 'contacts', 'uploadedFiles'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Email->id = $id;
		if (!$this->Email->exists()) {
			throw new NotFoundException(__('Invalid email'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Email->delete()) {
			$this->Session->setFlash(__('The email has been deleted.'));
		} else {
			$this->Session->setFlash(__('The email could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Email->recursive = 0;
		$this->set('emails', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Email->exists($id)) {
			throw new NotFoundException(__('Invalid email'));
		}
		$options = array('conditions' => array('Email.' . $this->Email->primaryKey => $id));
		$this->set('email', $this->Email->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Email->create();
			if ($this->Email->save($this->request->data)) {
				$this->Session->setFlash(__('The email has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email could not be saved. Please, try again.'));
			}
		}
		$users = $this->Email->User->find('list');
		$contacts = $this->Email->Contact->find('list');
		$uploadedFiles = $this->Email->UploadedFile->find('list');
		$this->set(compact('users', 'contacts', 'uploadedFiles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Email->exists($id)) {
			throw new NotFoundException(__('Invalid email'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Email->save($this->request->data)) {
				$this->Session->setFlash(__('The email has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Email.' . $this->Email->primaryKey => $id));
			$this->request->data = $this->Email->find('first', $options);
		}
		$users = $this->Email->User->find('list');
		$contacts = $this->Email->Contact->find('list');
		$uploadedFiles = $this->Email->UploadedFile->find('list');
		$this->set(compact('users', 'contacts', 'uploadedFiles'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Email->id = $id;
		if (!$this->Email->exists()) {
			throw new NotFoundException(__('Invalid email'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Email->delete()) {
			$this->Session->setFlash(__('The email has been deleted.'));
		} else {
			$this->Session->setFlash(__('The email could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
