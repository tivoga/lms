<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'isdk', array('file' => 'infusionsoft' . DS . 'isdk.php'));
/**
 * CustomFields Controller
 *
 * @property CustomField $CustomField
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CustomFieldsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
//        $conn = new iSDK();
//        $conn->cfgCon("infusion_befault");
//        $max_results =1000;
//        $page =0;
//        // infusion soft api: we need to look for FormId = -1:
//        // that will give us all the custom data form fields for contact field currenly set up in infusion soft.
//        $return_fields = ['DataType', 'Id', 'FormId', 'GroupId', 'Name', 'Label', 'DefaultValue', 'Values', 'ListRows'];
//        $data_fields = $conn->dsQuery("DataFormField", $max_results, $page, ['FormId' => '-1'], $return_fields);
//        // now the 'values' returned would be an array of all possible values the field
//        // can take. we will be stoing that a csv in the value field.
//        // the fields which have such 'values' have a 'DataType'
//        foreach($data_fields as $field){
//            $this->CustomField->create();
//            if ($this->CustomField->save($field)){
//                $this->Session->setFlash(__('The contact has been saved.'));
//            }
//        }
		$this->CustomField->recursive = 0;
		$this->set('customFields', $this->Paginator->paginate());
//        $this->set('data', $data_fields);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CustomField->exists($id)) {
			throw new NotFoundException(__('Invalid custom field'));
		}
		$options = array('conditions' => array('CustomField.' . $this->CustomField->primaryKey => $id));
		$this->set('customField', $this->CustomField->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CustomField->create();
			if ($this->CustomField->save($this->request->data)) {
				$this->Session->setFlash(__('The custom field has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The custom field could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CustomField->exists($id)) {
			throw new NotFoundException(__('Invalid custom field'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomField->save($this->request->data)) {
				$this->Session->setFlash(__('The custom field has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The custom field could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CustomField.' . $this->CustomField->primaryKey => $id));
			$this->request->data = $this->CustomField->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CustomField->id = $id;
		if (!$this->CustomField->exists()) {
			throw new NotFoundException(__('Invalid custom field'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomField->delete()) {
			$this->Session->setFlash(__('The custom field has been deleted.'));
		} else {
			$this->Session->setFlash(__('The custom field could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
        $this->CustomField->recursive = 0;
        $this->set('customFields', $this->Paginator->paginate());
	}

/**
 * admin_import method
 * the method imports all custom data fields currently at infusion soft
 * and loads them in to the db.
 * @return void
 */
    public function admin_import() {
        $conn = new iSDK();
        $conn->cfgCon("infusion_befault");
        $max_results =1000;
        $page =0;
        // infusion soft api: we need to look for FormId = -1:
        // that will give us all the custom data form fields for contact field currenly set up in infusion soft.
        $return_fields = ['DataType', 'Id', 'FormId', 'GroupId', 'Name', 'Label', 'DefaultValue', 'Values', 'ListRows'];
        $data_fields = $conn->dsQuery("DataFormField", $max_results, $page, ['FormId' => '-1'], $return_fields);
        // now the 'values' returned would be an array of all possible values the field
        // can take. we will be storing that a csv in the value field.
        // the fields which have such 'values' have a 'DataType'
//        debug($data_fields);
        foreach($data_fields as $field){
            $field['name'] = $field['Name'];
            if (!$this->CustomField->exists($field['Id'])){
                $this->CustomField->create();
                $field['id'] = $field['Id'];
                if ($this->CustomField->save($field)){
                    $this->Session->setFlash(__('The Custom field has been saved.'));
                }
            }
        }
        $this->Session->setFlash(__('All Custom fields have been Synced with Infusion soft.'));
        return $this->redirect(array('controller'=>'SegregationRules','action' => 'admin_index'));
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CustomField->exists($id)) {
			throw new NotFoundException(__('Invalid custom field'));
		}
		$options = array('conditions' => array('CustomField.' . $this->CustomField->primaryKey => $id));
		$this->set('customField', $this->CustomField->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CustomField->create();
			if ($this->CustomField->save($this->request->data)) {
				$this->Session->setFlash(__('The custom field has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The custom field could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CustomField->exists($id)) {
			throw new NotFoundException(__('Invalid custom field'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CustomField->save($this->request->data)) {
				$this->Session->setFlash(__('The custom field has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The custom field could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CustomField.' . $this->CustomField->primaryKey => $id));
			$this->request->data = $this->CustomField->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CustomField->id = $id;
		if (!$this->CustomField->exists()) {
			throw new NotFoundException(__('Invalid custom field'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomField->delete()) {
			$this->Session->setFlash(__('The custom field has been deleted.'));
		} else {
			$this->Session->setFlash(__('The custom field could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
