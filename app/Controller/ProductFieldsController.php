<?php
App::uses('AppController', 'Controller');
/**
 * ProductFields Controller
 *
 * Admin CRUD operations
 *
 * @property ProductField $ProductField
 * @property PaginatorComponent $Paginator
 */
class ProductFieldsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProductField->recursive = 0;
		$this->set('productFields', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProductField->exists($id)) {
			throw new NotFoundException(__('Invalid product field'));
		}
		$options = array('conditions' => array('ProductField.' . $this->ProductField->primaryKey => $id));
		$this->set('productField', $this->ProductField->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProductField->create();
			if ($this->ProductField->save($this->request->data)) {
				$this->Session->setFlash(__('The product field has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product field could not be saved. Please, try again.'));
			}
		}
		$products = $this->ProductField->Product->find('list');
		$this->set(compact('products'));
	}

    /**
     * admin_add method called from admin_product_details page
     *
     * @return void
     */
    public function admin_add_from_products() {
        if ($this->request->is('post')) {
            $this->ProductField->create();
            $this->request->data['ProductField']['product_id'] = $this->request->data['ProductField']['pro_id'];
//            debug($this->request->data);
            if ($this->ProductField->save($this->request->data)) {
                $this->Session->setFlash(__('The product field has been saved.'));
                return $this->redirect(array('controller' => 'Products','action' => 'admin_view',$this->request->data['ProductField']['product_id']));
            } else {
                $this->Session->setFlash(__('The product field could not be saved. Please, try again.'));
            }
        }
//        $products = $this->ProductField->Product->find('list');
//        $this->set(compact('products'));
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ProductField->exists($id)) {
			throw new NotFoundException(__('Invalid product field'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProductField->save($this->request->data)) {
				$this->Session->setFlash(__('The product field has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product field could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProductField.' . $this->ProductField->primaryKey => $id));
			$this->request->data = $this->ProductField->find('first', $options);
		}
		$products = $this->ProductField->Product->find('list');
		$this->set(compact('products'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProductField->id = $id;
		if (!$this->ProductField->exists()) {
			throw new NotFoundException(__('Invalid product field'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProductField->delete()) {
			$this->Session->setFlash(__('The product field has been deleted.'));
		} else {
			$this->Session->setFlash(__('The product field could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
