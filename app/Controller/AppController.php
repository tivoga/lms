<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
//    var $layout = 'default2';
    public $components = array('DebugKit.Toolbar',
        'Acl',
        'Auth' => array(
            'flash' => array(
                'element' => 'alert',
                'key' => 'auth',
                'params' => array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-error'
                )
            )
        ),
        'Session',
        'RememberMe'
    );

    public function isAuthorized($user) {
        if (!empty($this->request->params['admin'])) {
            return $user['role'] === 'admin';
        }
        $perm = $this->Acl->check(array(
            'model' => 'User',
            'foreign_key' => $user['id']
        ), $this->request->controller.'/'.$this->request->action);
        if (!$perm){
            throw new NotFoundException('You are not permitted to view this Page');
        }
        return !empty($user);
    }

    function beforeFilter() {
        parent::beforeFilter();
//        $this->Auth->deny();
        $this->Auth->authorize = 'Controller';
        $this->Auth->fields = array('username' => 'email', 'password' => 'passwd');
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin' => false);
        $this->Auth->loginRedirect = '/';
        $this->Auth->logoutRedirect = '/';
        $this->Auth->authError = __('Sorry, but you need to login to access this location.', true);
        $this->Auth->loginError = __('Invalid e-mail / password combination.  Please try again', true);
        $this->Auth->autoRedirect = true;
        $this->Auth->userModel = 'User';
        $this->Auth->userScope = array('User.active' => 1);
        if ($this->Auth->user()) {
            $this->set('userData', $this->Auth->user());
//            $this->set('isAuthorized', ($this->Auth->user('id') != ''));
        }
//        if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
//            $this->layout = 'admin';
//        }
    }

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );
}
