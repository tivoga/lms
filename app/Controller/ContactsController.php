<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'isdk', array('file' => 'infusionsoft' . DS . 'isdk.php'));
/**
 * Contacts Controller
 *
 * @property Contact $Contact
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContactsController extends AppController {
    public $actsAs = array('Acl' => array('type' => 'controlled'));


    public function beforeFilter() {
        if (isset($this->Auth)) {
            $this->Auth->allowedActions = array('import_contacts');
        }
        parent::beforeFilter();
    }
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'RequestHandler');
    public $paginate_options = array(
        'limit' => 500,
        'order' => array(
            'Contact.Id' => 'DESC'
        )
    );

    /**
     * index method (live leads)
     * Nothing much here, get the list of contacts assigned to the
     * current user with the status of
     * @return void
     */
	public function index() {
//        if($this->isAuthorized($this->user)){
//            $this->Session->setFlash(__('permission found'));
//        }
        $this->Contact->recursive = 2;
//		$this->set('contacts', $this->Paginator->paginate());
//        $options = array('conditions' => $conditions);
        $this->set('contacts', $this->Contact->find('all',array(
            'conditions' => array('Contact.user_id' => $this->Auth->user('id')),
            'order' => 'Contact.Id DESC','limit' =>1000)));
//        $data = $this->Contact->getContactByCustomValues('IndustryType1',['Energy industry','Chemical industry']);
//        $this->set('contacts', $data);
//        $this->set('contact', $contacts);
        $this->set('page_head', 'Live Leads');
        $this->set('nav_id','nav_leads');
        $this->set('_serialize', array('contacts'));
    }

    /**
     * archives method
     * shows the archived contact lists either for this or all the users.
     * @param bool $all
     * @return void
     */
    public function archives($all = false) {
        if(!$all){
            $this->archived_main_contacts(true, $all,'My Archived Contacts');
        }else{
            $this->archived_main_contacts(true, $all,'All Archived Contacts');
        }
    }

    /**
     * unsegregated contacts method
     *
     * shows the list of all contacts that haven't been assigned to
     * any user. (Unsegregated)
     *
     * the function uses the archives template
     *
     * @return void
     */
    public function unsegregated() {
//        $conditions=array();
        $this->Contact->recursive = 2;
        $conditions = array('Contact.user_id IS NULL');
        $conditions['Contact.gp_primary'] = 1;
//        $conditions['NOT'] = array('Contact.archived'=>1);
        $conditions['OR'] = array('Contact.archived is NULL','Contact.archived'=>0);
        $options = $this->paginate_options;
        $options['fields']=array('Contact.group_id', 'Contact.gp_primary', 'Contact.Phone1', 'Contact.Company', 'Contact.Email', 'Contact.LastName', 'Contact.FirstName');
        $this->Paginator->settings = $options;
//        debug($conditions);
        $data = $this->Paginator->paginate(
            'Contact',
            $conditions
        );
        $this->set('contacts', $data);
        $this->set('page_head', 'Archives');
        $this->set('nav_id','nav_archives');
        $this->set('page_title','Un Segregated Contacts');
        $this->render('archives');
    }


    /**
     * mainContacts method
     *
     *
     * the function uses the archives template
     * @param bool $all
     * @return void
     */
    public function mainContacts($all = false) {
        if(!$all){
            $this->set('nav_id','nav_main_cont');
            $this->archived_main_contacts(false, $all, 'My Main Contacts');
        }else{
            $this->set('nav_id','nav_all_main');
            $this->archived_main_contacts(false, $all, 'All Main Contacts');
        }
    }


    /**
     * archived_main_contacts method
     * used to display archived or main contacts
     * either assigned to the user or all contacts.
     * @param bool $archived
     * @param bool $all
     * @param string $title
     * @return void
     */
    private function archived_main_contacts($archived = true, $all = false,$title = 'Contacts List') {
        $this->Contact->recursive = 2;
        $conditions=array();
        $conditions['Contact.archived'] = (int)$archived;
        $conditions['Contact.gp_primary'] = 1;
        if(!$all){
            $conditions['Contact.user_id'] = $this->Auth->user('id');
        }
        $this->Paginator->settings = $this->paginate_options;
        $this->Paginator->settings['group'] ='Contact.group_id';
        $data = $this->Paginator->paginate(
            'Contact',
            $conditions
        );
        $this->set('contacts', $data);
        $this->set('page_head', 'Archives');
        $this->set('page_title',$title);
        if (!$this->Session->check('nav_id')) {
            $this->set('nav_id','nav_archives');
        }
        $this->render('archives');
    }


    /**
     * grouped_contacts method
     *
     * @param null $id
     * @return void
     */
    public function grouped_contacts($id = null) {
        $this->loadModel('ContactGroup');
        $this->Contact->recursive = 1;
        $gp = $this->ContactGroup->find('first',
            array('conditions'=>array('ContactGroup.id'=>$id)));
        $conditions=array();
        $conditions['Contact.group_id'] = $id;
        $this->Paginator->settings = $this->paginate_options;
        $data = $this->Paginator->paginate(
            'Contact',
            $conditions
        );
        $this->set('contacts', $data);
        $this->set('page_head', 'Archives');
        $this->set('nav_id','nav_archives');
        $this->set('page_title', 'Contacts in group '.$gp['ContactGroup']['name']);
        $this->set('groups', true);
        $this->render('archives');
    }


    /**
     * move this contact method
     * move the contact to archives, main list or the live feed table
     * if  archives == null => contact appears in live feed table
     * if  archives == 0 => contact appears in main contacts table
     * if  archives == 1 => contact appears in archived table
     *
     * @param null $id
     * @param int $archive_status
     * @throws NotFoundException
     * @return void
     */
    private function move_contact($id = null, $archive_status =1) {
        if (gettype($id)!='array'){
            if (!$this->Contact->exists($id)) {
                throw new NotFoundException(__('Invalid contact'));
            }
        }
        $data = array();
        debug($id);
        debug($archive_status);

        $data['Id']=$id;
        $data['archived']=$archive_status;
        $this->Contact->updateAll(array('Contact.archived' => $archive_status),
            array('Contact.Id ' => $id));
//        if ($this->Contact->save($data)) {
//            $this->Session->setFlash(__('The contact has been archived.'));
//        } else {
//            $this->Session->setFlash(__('The contact could not be saved. Please, try again.'));
//        }
//        $options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
//        $this->request->data = $this->Contact->find('first', $options);
        if (gettype($id)!='array'){
            return $this->redirect(array('action' => 'view',$id));
        }
        return;
//        }
    }

    /**
     * archive this contact method
     *
     * move this contact to archive (set archive to 1)
     * @param null $id
     * @return void
     */
    public function archive_contact($id = null) {
        if ($this->request->is(array('post', 'put'))) {
//            debug($this->request->data);
            $id =$this->request->data['id'];
        }
//        debug(gettype($id));
        $this->move_contact($id);
    }

    /**
     * unarchive this contact method
     *
     * move the contact back to the main contacts list
     * i.e 'archives' =0
     *
     * @param null $id
     * @return void
     */
    public function unarchive_contact($id = null) {
        $this->move_contact($id, 0);
    }

    /**
     * proximity method
     *
     * The page is to display all contacts with in a specified radius of a
     * uk postal code.
     *
     * There exists a database backend having the lat,lng of all
     * uk postal addresses
     *
     * its just a matter of querying and getting the ones in the desired radius.
     *
     * google api needs the lat,lng of the map points to display,
     * so we send that to the view to be rendered along with the contact data.
     *
     * @return void
     */
    public function proximity() {
        $this->loadModel('Products');
        $this->loadModel('ContactsProduct');
        $this->loadModel('User');
        $contract_status_enums = $this->ContactsProduct->get_enum_contract_status();
        $this->Products->recursive = -1;
        $products = $this->Products->find('list');
        $industry_options = $this->get_industry_options();
        $users = $this->User->find('list');
        $this->set(compact('products','users'));
        $this->set('contracts',$contract_status_enums);
        $this->set('industries',$industry_options);

        if ($this->request->is(array('post', 'put'))) {
//            debug($this->request->data);
            $postcode =$this->request->data['Proximity']['code'];
            $distance =$this->request->data['Proximity']['distance'];
            $postcode = str_replace(" ", "", $postcode);
            if(!$postcode or !$distance)return;
            $this->loadModel('Postcodelatlng');
            //call the model function with the center code and the distance,
            $codes = $this->Postcodelatlng->get_postalcodes_in_range($postcode,$distance);
            // and we are done. the rest is data formatting.
//            debug($codes);
            $center = $codes[1];
            $codes = $codes[0];
            $this->set('codes',$codes);
            $this->set('center',$center);
            if($codes){
                $codes_list = array();
                foreach($codes as $p_code){
                    array_push($codes_list,$p_code['postcodelatlng']['postcode']);
                }
                $this->set('contacts',
                    $this->Contact->find('all',array('conditions'=>
                        array('Contact.ZipFour1' => $codes_list)
                    )));
            }else{
                $this->Session->setFlash(__('Sorry No Postal Codes were found in the radius'));

            }
        }

    }
/**
 * set of convinience functions
 *
 * get all products
 * products and services.
 * products only
 * services only
 * product types
 *
 * Industry options: options for the Industry custom field in the
 * infusion soft.
 *
*/
    private function get_all_products(){
        $this->loadModel('Products');
        return $this->Products->find('list');
    }

    private function get_products_and_services(){
        $this->loadModel('Products');
        $all['products']= $this->Products->find('list',array('conditions' =>array(
            'Products.is_service = 0'
        )));
        $all['services']= $this->get_services_only();
        return $all;
    }

    private function get_services_only(){
        $this->loadModel('Products');
        return $this->Products->find('list',array('conditions' =>array(
            'Products.is_service = 1'
        )));
    }

    private function get_products_types(){
        $this->loadModel('ProductType');
        return $this->ProductType->find('list');
    }

    private function get_industry_options(){
        $this->loadModel('CustomField');
        return $this->CustomField->get_industry_options();
    }
/**
 * ********************************
 *
 * *
*/

    /**
     * reports method
     *
     * reports takes the var in form posted uses
     * create_sql_query_for_lead_reports() to create the conditions
     * in sql
     * export_lead_report()is the function used to export the
     * results on the page.
     *
     * @return void
     */

    public function reports() {
        $options = $this->create_sql_query_for_lead_reports();
        $this->Contact->recursive = 1;
//        $this->set('firstcontact',$this->Contact->find('all',array(
//            'conditions' => array('Contact.Id' => $fil_set),
//        )));
        $this->paginate=array(
            'conditions' => array('Contact.Id' => $options),
            'limit'=>1000);
        $this->set( 'contacts', $this->paginate() );
        $ind = $this->get_industry_options();
        $this->set('industries',$ind);
        $this->set('products',$this->get_all_products());
    }

    public function export_lead_report(){
        $options = $this->create_sql_query_for_lead_reports();
        $this->Contact->recursive = 1;
        $contacts = $this->Contact->find('all',array(
            'conditions' => array('Contact.Id' => $options),
        ));
        if(!$contacts){
            $this->Session->setFlash(__('No results to Export'));
            return $this->redirect(array('action' => 'reports'));
        }
        $this->response->download("export_lead_report.csv");
        $this->set(compact('contacts'));
        $this->layout = 'ajax';
        return;
    }

    public function create_sql_query_for_lead_reports(){
//        debug($this->request->query);
        $sql_params = array('startDate', 'endDate', 'Product','lead_source','industry');
        $empty = false;
        if(!$this->request->query){$empty = true;}
        foreach($sql_params as $param){
            if(!isset($this->request->query[$param])){
                $this->request->query[$param] ='';
            }
        }
        $sql_params = $this->request->query;
        if($empty or implode("", array_values($sql_params)) == ''){
//            return array('conditions'=>array('Contact.Id =\'****\''));
            return array();
        }
//        debug(implode("", array_values($sql_params)));
        $this->Contact->recursive = -1;
        $this->loadModel('ContactsProduct');

        $options=array();
        $options['conditions'] = array();
        if ($sql_params['endDate']){$options['conditions']['Contact.DateCreated <='] = $sql_params['endDate'];}
        if ($sql_params['startDate']){$options['conditions']['Contact.DateCreated >='] = $sql_params['startDate'];}
        if ($sql_params['lead_source']){$options['conditions']['Contact.Leadsource ='] = $sql_params['lead_source'];}

        $options['joins'] = array();
        array_push($options['joins'],array('table' => 'Contacts_products',
            'alias' => 'ContactsProduct',
            'type' => 'LEFT',
            'conditions' => array(
                'ContactsProduct.contact_id = Contact.Id',
//                    'ContactsProduct.contract_status = 2',
//                    'ContactsProduct.renew_letter_sent = 0',
//                    'ContactsProduct.contract_end <='=>date("Y-m-d", strtotime('+ 3 month')),
            )
        ));

        $prod_conditions = array(
            'ContactsProduct.product_id = Product.id'
        );
        $type = 'LEFT';
        if($sql_params['Product']){
            array_push($prod_conditions,'Product.id=\''.$sql_params['Product'].'\'');
            $type = 'INNER';
        }
        array_push($options['joins'],array('table' => 'products',
            'alias' => 'Product',
            'type' => $type,
            'conditions' => $prod_conditions
        ));

        $custom_values_cond = array(
            'ContactsCustomValues.contactId = Contact.Id',
            'ContactsCustomValues.custom_field_id = 54'
        );
        $type = 'LEFT';
        if($sql_params['industry'] != ''){
            array_push($custom_values_cond, 'ContactsCustomValues.value =\''.
                $this->get_industry_options()[$sql_params['industry']].'\'');
            $type = 'INNER';
        }
        array_push($options['joins'],array('table' => 'custom_field_values',
            'alias' => 'ContactsCustomValues',
            'type' => $type,
            'conditions' => $custom_values_cond
        ));
        $options['fields'] = array('DISTINCT Contact.Id');
        $ids_set= $this->Contact->find('all',$options);
        $fil_set = array();
        foreach($ids_set as $c_id){
            array_push($fil_set, $c_id['Contact']['Id']);
        }
        return $fil_set;
    }

    /******************************
    */

    /**
     * TODO: change func to Ben's specs
     *
     * renewal_reports method
     *
     * page displays all contacts with any one of their
     * contract having 'end of contract' date
     * (start date + contract length) in the
     * next three months.
     *
     * @return void
     */
    public function renewal_reports() {
        $this->loadModel('ContactsProducts');
        $options['joins'] = array(
            array('table' => 'Contacts_products',
                'alias' => 'ContactsProduct',
                'type' => 'INNER',
                'conditions' => array(
                    'ContactsProduct.contact_id = Contact.Id',
                    'ContactsProduct.contract_status = 2',
                    'ContactsProduct.renew_letter_sent = 0',
                    'ContactsProduct.contract_end <='=>date("Y-m-d", strtotime('+ 3 month')),
                )
            ),
            array('table' => 'products',
                'alias' => 'Product',
                'type' => 'INNER',
                'conditions' => array(
                    'ContactsProduct.product_id = Product.id',
                )
            )
        );
        $options['fields'] = array(
            'DISTINCT Contact.*',
            'ContactsProduct.*',
            'Product.*'
        );
        $this->Contact->recursive = -1;
        $this->set('contacts',$this->Contact->find('all',$options));
    }

    /**
     * inv_reports method
     *
     * list all customers who have been sent a renewal letter
     * for any one of their contracts
     *
     * @return void
     */
    public function inv_reports() {
        $this->loadModel('ContactsProducts');
        $options['joins'] = array(
            array('table' => 'Contacts_products',
                'alias' => 'ContactsProduct',
                'type' => 'INNER',
                'conditions' => array(
                    'ContactsProduct.contact_id = Contact.Id',
                    'ContactsProduct.contract_status = 2',
                    'ContactsProduct.renew_letter_sent' => true,
                    'ContactsProduct.contract_end <='=>date("Y-m-d", strtotime('+ 3 month')),
                )
            ),
            array('table' => 'products',
                'alias' => 'Product',
                'type' => 'INNER',
                'conditions' => array(
                    'ContactsProduct.product_id = Product.id',
                )
            )
        );
        $options['fields'] = array(
            'DISTINCT Contact.*',
            'ContactsProduct.*',
            'Product.*'
        );
        $this->Contact->recursive = -1;
        $this->set('contacts',$this->Contact->find('all',$options));
    }

    public function move_contract_to_cancel(){
        $this->loadModel('ContactsProduct');
        if(!$this->ContactsProduct->moved_to_cancel(
            $this->params->named['contract_id'])){
            $this->Session->setFlash(__('Sorry the Product status couldn\'t be moved to cancel' ));
        }else{
            $this->Session->setFlash(__('Product status set to "Cancel"' ));
        }
        return $this->redirect(array('action' => 'inv_reports'));


    }
    /**
     * cancel_reports method
     *
     * List all customers with any of the contracts set as cancelled
     *
     * @return void
     */
    public function cancel_reports() {
        if ($this->request->is(array('post', 'put'))) {
//            debug($this->request->data);
            $startDate = $this->request->data['startDate'];
            $endDate = $this->request->data['endDate'];
            $options['conditions'] = array();
            if($endDate){$options['conditions']['Contact.DateCreated <='] = $endDate;}
            if ($startDate){$options['conditions']['Contact.DateCreated >=']=$startDate;}
        }
        $this->loadModel('ContactsProducts');

        $options['joins'] = array(
            array('table' => 'Contacts_products',
                'alias' => 'ContactsProduct',
                'type' => 'INNER',
                'conditions' => array(
                    'ContactsProduct.contact_id = Contact.Id',
                    'ContactsProduct.contract_status' => array(3,4),
                    'ContactsProduct.renew_letter_sent' => true,
//                    'ContactsProduct.contract_end <='=>date("Y-m-d", strtotime('+ 3 month')),
                )
            ),
            array('table' => 'products',
                'alias' => 'Product',
                'type' => 'INNER',
                'conditions' => array(
                    'ContactsProduct.product_id = Product.id',
                )
            )
        );
        $options['fields'] = array(
            'DISTINCT Contact.*',
            'ContactsProduct.*',
            'Product.*'
        );
        $this->Contact->recursive = -1;
        $this->set('contacts',$this->Contact->find('all',$options));
    }

    /**
     * financials method
     *
     * get search params from post request
     * search against contract and product data
     *
     * group by product type.
     *
     * the financials feature consists of set of 3 functions
     * financials which renders the page, export_financial_report
     * which exports the data displayed on the page and
     * create_sql_query_for_financial_reports - helper function used to
     * make conditions described by the form posted.
     *
     * @return void
     */
    public function financials() {
//        debug($this->request->query);
        $options = $this->create_sql_query_for_financial_reports();
//        debug($options);
        $this->Contact->recursive = 1;
        $this->paginate=array(
            'conditions' => array('Contact.Id' => $options),
            'limit'=>1000
        );
        $this->loadModel('ContactsProduct');
        $this->set('contacts', $this->paginate() );
//        $this->set('contacts',$this->Contact->find('all',$options));
        $this->set('products',$this->get_all_products());
//        $this->set('products',$this->get_products_and_services()['products']);
//        $this->set('services',$this->get_products_and_services()['services']);
        $this->set('ptypes',$this->get_products_types());
        $this->set('clengths',$this->ContactsProduct->get_enum_contract_length());
    }

    public function export_financial_report(){
        $options = $this->create_sql_query_for_financial_reports();
        $this->Contact->recursive = 1;
        $contacts = $this->Contact->find('all',array(
            'conditions' => array('Contact.Id' => $options),
        ));
        if(!$contacts){
            $this->Session->setFlash(__('No results to Export'));
            return $this->redirect(array('action' => 'financials'));
        }
        $this->response->download("export_financial_report.csv");
        $this->set(compact('contacts'));
        $this->layout = 'ajax';
        return;
    }

    private function create_sql_query_for_financial_reports(){
        $sql_params = array('Product', 'clength', 'cont_startDate', 'cont_endDate',
            'ren_startDate', 'ren_endDate', 'costs_start', 'costs_end');
        foreach($sql_params as $param){
            if(!isset($this->request->query[$param])){
                $this->request->query[$param] ='';
            }
        }
        $sql_params=$this->request->query;
        $this->Contact->recursive = -1;
        $options['fields'] = array(
            'DISTINCT Contact.Id'
        );
        $cont_conditions = array(
            'ContactsProduct.contact_id = Contact.Id',
            'ContactsProduct.renew_letter_sent' => true
        );
        $is_product = true;
        $this->loadModel('Product');
        $this->Product->recursive = -1;
        if($sql_params['Product']){
            $cont_conditions['ContactsProduct.product_id =']=$sql_params['Product'];
            $prod = $this->Product->find('first', array(
                'conditions'=>array('Product.id'=>intval($sql_params['Product']))
            ));
//            debug($prod);
            if($prod['Product']['is_service']){$is_product = false;}
//            debug($is_product);
        }
        if($sql_params['cont_endDate']){$cont_conditions['ContactsProduct.contract_start <='] = $sql_params['cont_endDate'];}
        if($sql_params['cont_startDate']){$cont_conditions['ContactsProduct.contract_start >=']=$sql_params['cont_startDate'];}
        if($is_product){
            if($sql_params['clength']){$cont_conditions['ContactsProduct.contract_period =']=$sql_params['clength'];}
            if($sql_params['ren_startDate']){$cont_conditions['ContactsProduct.contract_end >=']=$sql_params['ren_startDate'];}
            if($sql_params['ren_endDate']){$cont_conditions['ContactsProduct.contract_end <=']=$sql_params['ren_endDate'];}
        }
        if($sql_params['costs_start']){$cont_conditions['ContactsProduct.contract_paid =']=$sql_params['costs_start'];}
        if($sql_params['costs_end']){$cont_conditions['ContactsProduct.contract_paid =']=$sql_params['costs_end'];}
        $options['joins'] = array(
            array('table' => 'Contacts_products',
                'alias' => 'ContactsProduct',
                'type' => 'INNER',
                'conditions' => $cont_conditions
            ),
        );
        $ids_set= $this->Contact->find('all',$options);
        $fil_set = array();
        foreach($ids_set as $c_id){
            array_push($fil_set, $c_id['Contact']['Id']);
        }
        return $fil_set;
    }

/**
 * ***********************************************
*/

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function view($id = null) {
		if (!$this->Contact->exists($id)) {
			throw new NotFoundException(__('Invalid contact'));
		}
        $this->Contact->recursive = 2;
		$options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
		$this->set('contact', $this->Contact->find('first', $options));
        $this->loadModel('Communication');
		$this->set('communication_types', $this->Communication->get_enum_communication_types());
	}

    /**
     * add method
     *
     * Create a new Contact
     *
     * @return void
     */
	public function add() {
		if ($this->request->is('post')) {
			$this->Contact->create();
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__('The contact has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact could not be saved. Please, try again.'));
			}
		}
        $users = $this->Contact->User->find('list');
        $groups = $this->Contact->Group->find('list');
        $this->set(compact('users','groups'));
	}

    /**
     * edit method
     *
     * special privileges: if the user happens to be the admin
     * He gets to change the user and the contact group as well
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function edit($id = null) {
		if (!$this->Contact->exists($id)) {
			throw new NotFoundException(__('Invalid contact'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__('The contact has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
			$this->request->data = $this->Contact->find('first', $options);
		}
        $users = $this->Contact->User->find('list');
        $groups = $this->Contact->Group->find('list');
        $this->set(compact('users','groups'));
	}

    /**
     * delete method
     *
     * not used or linked to
     *
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function delete($id = null) {
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Contact->delete()) {
			$this->Session->setFlash(__('The contact has been deleted.'));
		} else {
			$this->Session->setFlash(__('The contact could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    /**
     * admin_index method
     *
     * nothing much and pretty
     * much never used or linked to
     *
     * @return void
     */
	public function admin_index() {
		$this->Contact->recursive = 0;
		$this->set('contacts', $this->Paginator->paginate());
	}

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function admin_view($id = null) {
		if (!$this->Contact->exists($id)) {
			throw new NotFoundException(__('Invalid contact'));
		}
		$options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
		$this->set('contact', $this->Contact->find('first', $options));
	}

    /**
     * admin_add method
     *
     * @return void
     */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Contact->create();
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__('The contact has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact could not be saved. Please, try again.'));
			}
		}
        $users = $this->Contact->User->find('list');
        $this->set(compact('users'));
        $this->render('admin_add-orig');
	}

    /**
     * admin_edit method
     *
     * since admin, we also let him change the contact user and the group
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function admin_edit($id = null) {
		if (!$this->Contact->exists($id)) {
			throw new NotFoundException(__('Invalid contact'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Contact->save($this->request->data)) {
				$this->Session->setFlash(__('The contact has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contact.' . $this->Contact->primaryKey => $id));
			$this->request->data = $this->Contact->find('first', $options);
		}
        $users = $this->Contact->User->find('list');
//        $contact_groups = $this->Contact->ContactGroup->find('list');
        $this->set(compact('users'));//,'contact_groups'
        $this->render('admin_edit-orig');
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function admin_delete($id = null) {
		$this->Contact->id = $id;
		if (!$this->Contact->exists()) {
			throw new NotFoundException(__('Invalid contact'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Contact->delete()) {
			$this->Session->setFlash(__('The contact has been deleted.'));
		} else {
			$this->Session->setFlash(__('The contact could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function import_contacts(){
        return;
    }
}
