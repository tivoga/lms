<?php
App::uses('AppController', 'Controller');
/**
 * UploadedFiles Controller
 *
 * @property UploadedFile $UploadedFile
 * @property PaginatorComponent $Paginator
 */
class UploadedFilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function deleteFile($id = null) {
        $this->UploadedFile->id = $id;
        if (!$this->UploadedFile->exists()) {
            throw new NotFoundException(__('Invalid uploaded file'));
        }
        $file_sel = $this->UploadedFile->find('first');
//        $this->request->allowMethod('post', 'delete');
//        debug($file_sel);
        if(!unlink($file_sel['UploadedFile']['location'])){
            $this->Session->setFlash(__('The uploaded file could not be deleted from the file system. Please, try again.'));
            return;
        }else{
            if ($this->UploadedFile->delete()) {
            $this->Session->setFlash(__('The uploaded file has been deleted.'));
        } else {
            $this->Session->setFlash(__('The uploaded file could not be deleted. Please, try again.'));
        }
        }
        return $this->redirect(array('controller' =>'Contacts','action' => 'view',$this->params['named']['contact_id']));
    }

/**
 * download method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function downloadFile($id = null) {
        $this->UploadedFile->id = $id;
        if (!$this->UploadedFile->exists()) {
            throw new NotFoundException(__('Invalid uploaded file'));
        }
        $options = array('conditions' => array('UploadedFile.' . $this->UploadedFile->primaryKey => $id));
        $file = $this->UploadedFile->find('first', $options);
        $path = $file['UploadedFile']['location'];
        $this->response->file($path, array(
            'download' => true,
            'name' => $file['UploadedFile']['filename'],
        ));
        return $this->response;
    }


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UploadedFile->recursive = 0;
        $this->paginate = array('conditions'=>array('UploadedFile.contact_id is null'));
		$this->set('uploadedFiles', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UploadedFile->exists($id)) {
			throw new NotFoundException(__('Invalid uploaded file'));
		}
		$options = array('conditions' => array('UploadedFile.' . $this->UploadedFile->primaryKey => $id));
		$this->set('uploadedFile', $this->UploadedFile->find('first', $options));
	}

/**
 * admin_add method
 * all files added by the admin through this interface
 * are available system wide to all users.
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
//            debug($this->request->data);
            $filename = null;

            if (!empty($this->request->data['UploadedFile']['file']['tmp_name'])
                && is_uploaded_file($this->request->data['UploadedFile']['file']['tmp_name'])
            ) {
                $folder_url = WWW_ROOT.'documents';
                if(!is_dir($folder_url)) {
                    mkdir($folder_url);
                }
                // set new relative folder
                $folder_url = WWW_ROOT.'documents'.DS .'common';
                // create directory
                if(!is_dir($folder_url)) {
                    mkdir($folder_url);
                }
                // Strip path information
                $filename = basename($this->request->data['UploadedFile']['file']['name']);
                move_uploaded_file(
                    $this->request->data['UploadedFile']['file']['tmp_name'],
                    $folder_url . DS . $filename
                );
                $data = array();
//                $data['contact_id']=$this->params['named']['contact_id'];
                $data['location'] = $folder_url . DS . $filename;
                $data['user_id'] = $this->Auth->user('id');
                $data['filename'] = $this->request->data['UploadedFile']['filename'];

                $this->UploadedFile->create();
                $this->Session->setFlash(__('The uploaded file has been saved.'));
                if($this->UploadedFile->save($data)){
                    $this->Session->setFlash(__('The uploaded file has been saved to database.'));
                }
            }

//			if ($this->UploadedFile->save($this->request->data)) {
//				$this->Session->setFlash(__('The uploaded file has been saved.'));
//				return $this->redirect(array('action' => 'index'));
//			} else {
//				$this->Session->setFlash(__('The uploaded file could not be saved. Please, try again.'));
//			}
		}
		$users = $this->UploadedFile->User->find('list');
		$contacts = $this->UploadedFile->Contact->find('list');
		$emails = $this->UploadedFile->Email->find('list');
		$this->set(compact('users', 'contacts', 'emails'));
	}

/**
 * admin_add method
 *
 * @return void
 */
    public function addFile() {
        if ($this->request->is('post')) {
//            debug($this->request->data);
//            debug(is_uploaded_file($this->request->data['UploadedFile']['file']['tmp_name']));
            $filename = null;

            if (!empty($this->request->data['UploadedFile']['file']['tmp_name'])
                && is_uploaded_file($this->request->data['UploadedFile']['file']['tmp_name'])
            ) {
                $folder_url = WWW_ROOT.'documents';
                if(!is_dir($folder_url)) {
                    mkdir($folder_url);
                }
                // set new relative folder
                $folder_url = WWW_ROOT.'documents'.DS .$this->request->data['UploadedFile']['contact_id'];
                // create directory
                if(!is_dir($folder_url)) {
                    mkdir($folder_url);
                }
                // Strip path information
                $filename = basename($this->request->data['UploadedFile']['file']['name']);
                move_uploaded_file(
                    $this->request->data['UploadedFile']['file']['tmp_name'],
                    $folder_url . DS . $filename
                );
                $data = array();
                $data['contact_id']=$this->request->data['UploadedFile']['contact_id'];
                $data['location'] = $folder_url . DS . $filename;
                $data['user_id'] = $this->Auth->user('id');
                $data['filename'] = $this->request->data['UploadedFile']['filename'];

                $this->UploadedFile->create();
                $this->Session->setFlash(__('The uploaded file has been saved.'));
                if($this->UploadedFile->save($data)){
                    $this->Session->setFlash(__('The uploaded file has been saved to database.'));
                }
            }

//			if ($this->UploadedFile->save($this->request->data)) {
//				$this->Session->setFlash(__('The uploaded file has been saved.'));
//				return $this->redirect(array('action' => 'index'));
//			} else {
//				$this->Session->setFlash(__('The uploaded file could not be saved. Please, try again.'));
//			}
        }
        return $this->redirect(array('controller' =>'Contacts','action' => 'view',$this->request->data['UploadedFile']['contact_id']));
        $users = $this->UploadedFile->User->find('list');
        $contacts = $this->UploadedFile->Contact->find('list');
        $emails = $this->UploadedFile->Email->find('list');
        $this->set(compact('users', 'contacts', 'emails'));
    }


    /**
 * admin_edit method
 * only the filename can be changed.
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UploadedFile->exists($id)) {
			throw new NotFoundException(__('Invalid uploaded file'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UploadedFile->save($this->request->data)) {
				$this->Session->setFlash(__('The uploaded file has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The uploaded file could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UploadedFile.' . $this->UploadedFile->primaryKey => $id));
			$this->request->data = $this->UploadedFile->find('first', $options);
		}
		$users = $this->UploadedFile->User->find('list');
		$contacts = $this->UploadedFile->Contact->find('list');
		$emails = $this->UploadedFile->Email->find('list');
		$this->set(compact('users', 'contacts', 'emails'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UploadedFile->id = $id;
		if (!$this->UploadedFile->exists()) {
			throw new NotFoundException(__('Invalid uploaded file'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UploadedFile->delete()) {
			$this->Session->setFlash(__('The uploaded file has been deleted.'));
		} else {
			$this->Session->setFlash(__('The uploaded file could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
