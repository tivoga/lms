<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 */
class ProductsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * add product to contact method
 * called from product view
 *
 * This method effectively creates the new contact contract with the
 * product selected
 *
 * the page has two forms in it -
 *
 * one to select the product to create the contract for *
 * and the other to actually post the filled out form for the product
 * including the product specific questions.
 *
 * When the first form is posted the page is loaded with the product/service
 * specific questions
 *
 * There's a good amount of data jugglery done to achive this.
 * Same technique is used in edit product values as well.
 *
 * @return void
 */
    public function add_product_to_contact() {
        $products = $this->Product->find('list');
//        $productTypes = $this->Product->ProductType->find('list');
//        $contacts = $this->Product->Contact->find('list');
        $this->loadModel('ContactsProduct');
//        $this->loadModel('Users');
        $sales_persons= $this->ContactsProduct->User->find('list');
        $contract_length_enum = $this->ContactsProduct->get_enum_contract_length();
        $contract_status_enum = $this->ContactsProduct->get_enum_contract_status();
        $this->set(compact('productTypes','contract_length_enum',
            'contract_status_enum', 'contacts','products','sales_persons'));
        if ($this->request->is('post')) {
//            debug($this->request->data);
            if(array_key_exists('Product', $this->request->data)){
                $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $this->request->data['Product']['Product_id']));
                $options2 = array('conditions' => array(
                    'ContactsProduct.product_id' => $this->request->data['Product']['Product_id'],
                    'ContactsProduct.Contact_id' => $this->params['named']['contact_id']
                ));
                $prod = $this->Product->find('first', $options);
                if ($this->ContactsProduct->find('first',$options2)){
                    $this->Session->setFlash(__('The Contact Already has this Product'));
                    return;
                }
                $this->set('product', $prod);
//                $this->loadModel('Contact');
//                $options = array('conditions' => array('Contact.' . $this->Product->primaryKey => $this->request->data['Product']['Product_id']));
//                $cont = $this->Product->find('first', $options);
//                $this->Contact->find('first',$options);
            }elseif(array_key_exists('Product_values', $this->request->data)){
                $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $this->request->data['Product_values']['product_id']));
                $this_product = $this->Product->find('first', $options);
                $this->ContactsProduct->create();
                $data = array();
                $data['product_id'] = $this->request->data['Product_values']['product_id'];
                $data['contact_id'] = $this->params['named']['contact_id'];
                $data['contract_start'] = $this->request->data['Product_values']['c_start'];
                $data['contract_status'] = $this->request->data['Product_values']['c_status'];
                if (array_key_exists('c_end',$this->request->data['Product_values'])){
                    $data['contract_period'] = $this->request->data['Product_values']['c_length'];
                    $data['contract_end'] = $this->request->data['Product_values']['c_end'];
                }else{
                    $data['contract_period'] = Null;
                    $data['contract_end'] = Null;
                }
                $data['contract_quoted'] = $this->request->data['Product_values']['c_quoted'];
                $data['contract_paid'] = $this->request->data['Product_values']['c_paid'];
                $data['renew_letter_sent'] = 0;
//                $data['Product'] = $this_product['Product'];
//                $data['Contact']['Contact'] = array();
//                $i =0;
//                foreach ($this_product['Contact'] as $key =>$cont){
//                    $data['Contact']['Contact'][$i] = $cont['Id'];
//                    ++$i;
//                }
//                $data['Contact']['Contact'][$i] = $this->params['named']['contact_id'];
//                debug($data);

                if ($this->ContactsProduct->save($data)) {
                    $this->Session->setFlash(__('The product has been added to the Contact.'));
                    $this->loadModel('ProductFieldsValues');
                    $values = $this->request->data['Product_values'];
                    unset($values['product_id']);
                    unset($values['c_status']);
                    unset($values['c_start']);
                    unset($values['c_end']);
                    unset($values['c_length']);
                    unset($values['c_quoted']);
                    unset($values['c_paid']);
                    foreach($values as $i => $value){
//                        if (!$value){continue;}
                        $value_data = array();
                        $value_data['product_field_id'] = $i;
                        $value_data['contact_id'] = $this->params['named']['contact_id'];
                        $value_data['value'] = $value;
                        $this->ProductFieldsValues->create();
                        $this->ProductFieldsValues->save($value_data);
                    }
                    return $this->redirect(array('controller' =>'Contacts','action' => 'view',$this->params['named']['contact_id']));
                }else {
                    $this->Session->setFlash(__('The product could not be added to the contact. Please, try again.'));
                }
            }
//            $product_fields = $this->Product->ProductField->find('all');
//            $this->Product->create();
//            if ($this->Product->save($this->request->data)) {
//                $this->Session->setFlash(__('The product has been saved.'));
//                return $this->redirect(array('action' => 'index'));
//            } else {
//                $this->Session->setFlash(__('The product could not be saved. Please, try again.'));
//            }
//            $this->set(compact('product_fields'));
        }

    }


/**
 * edit_product_values method
 * called from contact view.
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function edit_product_values($id = null) {
        if (!$this->Product->exists($id)) {
            throw new NotFoundException(__('Invalid product'));
        }
        $this->loadModel('ProductFieldsValue');
        $this->loadModel('ContactsProduct');
        $contract_length_enum = $this->ContactsProduct->get_enum_contract_length();
        $contract_status_enum = $this->ContactsProduct->get_enum_contract_status();

        if ($this->request->is(array('post', 'put'))) {
//            debug($this->request->data);
            $options2 = array('conditions' => array(
                'ContactsProduct.product_id' => $id,
                'ContactsProduct.Contact_id' => $this->params['named']['contact_id']
            ));
            $contract = $this->ContactsProduct->find('first',$options2);
            $data = array();
//            $data['product_id'] = $this->request->data['Product']['product_id'];
//            $data['contact_id'] = $this->params['named']['contact_id'];
            $data['id'] = $contract['ContactsProduct']['id'];
            $data['contract_start'] = $this->request->data['Product']['c_start'];
            if (array_key_exists('c_end',$this->request->data['Product'])){
                $data['contract_period'] = $this->request->data['Product']['c_length'];
                $data['contract_end'] = $this->request->data['Product']['c_end'];
            }else{
                $data['contract_period'] = Null;
                $data['contract_end'] = Null;
            }
            $data['contract_status'] = $this->request->data['Product']['c_status'];
            $data['contract_quoted'] = $this->request->data['Product']['c_quoted'];
            $data['contract_paid'] = $this->request->data['Product']['c_paid'];
//            $this->ContactsProduct->save($data);
//            debug();
            if ($this->ContactsProduct->save($data)) {
                $values = $this->request->data['Product'];
                unset($values['c_status']);
                unset($values['c_start']);
                unset($values['c_end']);
                unset($values['c_length']);
                unset($values['c_quoted']);
                unset($values['c_paid']);

                foreach ($values as $id => $value){
                    $data =array();
                    $data['id']=$id;
                    $data['value']=$value;
                    $this->ProductFieldsValue->save($data);
                }
                $this->Session->setFlash(__('All values have been saved.'));
                return $this->redirect(array('controller' =>'Contacts','action' => 'view',$this->params['named']['contact_id']));
            } else {
                $this->Session->setFlash(__('The product could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
            $product =$this->Product->find('first', $options);
            $options2 = array('conditions' => array(
                'ContactsProduct.product_id' => $id,
                'ContactsProduct.Contact_id' => $this->params['named']['contact_id']
            ));
            $contract = $this->ContactsProduct->find('first',$options2);
            $field_ids =array();
            foreach($product['ProductField'] as $field){
                array_push($field_ids,$field['id']);
            }

            $options = array('conditions' => array('ProductFieldsValue.product_field_id' => $field_ids,
                'ProductFieldsValue.contact_id' => $this->params['named']['contact_id']));
            $values =$this->ProductFieldsValue->find('all', $options);
//            $this->request->data = $product;
            $this->request->data['values'] = $values;
        }
        $productTypes = $this->Product->ProductType->find('list');
        $contacts = $this->Product->Contact->find('list');
        $sales_persons= $this->ContactsProduct->User->find('list');
        $this->set(compact('productTypes', 'contract','contract_length_enum','contract_status_enum'));
        $this->set(compact('product', 'product','sales_persons'));
    }

/**
 * delete_product_from_contact method
 * Called form contact view page to remove the product association from the contact.
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function delete_product_from_contact($id = null) {
        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid product'));
        }
        $options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
        $product =$this->Product->find('first', $options);
        $data = array();
        $data['Product'] = $product['Product'];
        $data['Contact']['Contact'] = array();
        $i =0;
        foreach ($product['Contact'] as $key =>$cont){
            if($cont['Id'] == $this->params['named']['contact_id']) continue;
            $data['Contact']['Contact'][$i] = $cont['Id'];
            ++$i;
        }
//        $data['Contact']['Contact'][$i] = $this->params['named']['contact_id'];
//                debug($data);
//        if ($this->Product->save($data)) {}
//        $this->request->allowMethod('post', 'delete');
        $field_ids =array();
        foreach($product['ProductField'] as $field){
            array_push($field_ids, $field['id']);
        }
//        debug($field_ids);
        $this->loadModel('ProductFieldsValue');
        $options = array('conditions' => array('ProductFieldsValue.product_field_id' => $field_ids,
            'ProductFieldsValue.contact_id' => $this->params['named']['contact_id']));
//        debug($this->ProductFieldsValue->find('all',$options));
        if($this->ProductFieldsValue->deleteAll(array('ProductFieldsValue.product_field_id' => $field_ids,
            'ProductFieldsValue.contact_id' => $this->params['named']['contact_id']))){
            $this->Session->setFlash(__('The product has been removed from contact.'));
        }
        if (!$this->Product->save($data)) {
            $this->Session->setFlash(__('The product could not be removed from contact. Please, try again.'));
        }
        return $this->redirect(array('controller' =>'Contacts','action' => 'view',$this->params['named']['contact_id']));
    }



/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Product->recursive = 0;
		$this->set('products', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
		$this->set('product', $this->Product->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
			}
		}
		$productTypes = $this->Product->ProductType->find('list');
		$contacts = $this->Product->Contact->find('list');
		$this->set(compact('productTypes', 'contacts'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
			$this->request->data = $this->Product->find('first', $options);
		}
		$productTypes = $this->Product->ProductType->find('list');
		$contacts = $this->Product->Contact->find('list');
		$this->set(compact('productTypes', 'contacts'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Product->delete()) {
			$this->Session->setFlash(__('The product has been deleted.'));
		} else {
			$this->Session->setFlash(__('The product could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
