<?php
App::uses('AppController', 'Controller');
/**
 * ContactGroups Controller
 *
 * @property ContactGroup $ContactGroup
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContactGroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ContactGroup->recursive = 0;
		$this->set('contactGroups', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->ContactGroup->exists($id)) {
			throw new NotFoundException(__('Invalid contact group'));
		}
		$options = array('conditions' => array('ContactGroup.' . $this->ContactGroup->primaryKey => $id));
		$this->set('contactGroup', $this->ContactGroup->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ContactGroup->create();
			if ($this->ContactGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The contact group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact group could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->ContactGroup->exists($id)) {
			throw new NotFoundException(__('Invalid contact group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ContactGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The contact group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact group could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ContactGroup.' . $this->ContactGroup->primaryKey => $id));
			$this->request->data = $this->ContactGroup->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->ContactGroup->id = $id;
		if (!$this->ContactGroup->exists()) {
			throw new NotFoundException(__('Invalid contact group'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ContactGroup->delete()) {
			$this->Session->setFlash(__('The contact group has been deleted.'));
		} else {
			$this->Session->setFlash(__('The contact group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ContactGroup->recursive = 0;
		$this->set('contactGroups', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ContactGroup->exists($id)) {
			throw new NotFoundException(__('Invalid contact group'));
		}
		$options = array('conditions' => array('ContactGroup.' . $this->ContactGroup->primaryKey => $id));
		$this->set('contactGroup', $this->ContactGroup->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ContactGroup->create();
			if ($this->ContactGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The contact group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact group could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ContactGroup->exists($id)) {
			throw new NotFoundException(__('Invalid contact group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ContactGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The contact group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contact group could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ContactGroup.' . $this->ContactGroup->primaryKey => $id));
			$this->request->data = $this->ContactGroup->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ContactGroup->id = $id;
		if (!$this->ContactGroup->exists()) {
			throw new NotFoundException(__('Invalid contact group'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ContactGroup->delete()) {
			$this->Session->setFlash(__('The contact group has been deleted.'));
		} else {
			$this->Session->setFlash(__('The contact group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
