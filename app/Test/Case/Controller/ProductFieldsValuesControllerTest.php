<?php
App::uses('ProductFieldsValuesController', 'Controller');

/**
 * ProductFieldsValuesController Test Case
 *
 */
class ProductFieldsValuesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_fields_value',
		'app.product_field',
		'app.product',
		'app.product_type',
		'app.contact',
		'app.custom_field_value',
		'app.custom_field'
	);

}
