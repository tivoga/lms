<?php
App::uses('ContactGroup', 'Model');

/**
 * ContactGroup Test Case
 *
 */
class ContactGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contact_group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ContactGroup = ClassRegistry::init('ContactGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ContactGroup);

		parent::tearDown();
	}

}
