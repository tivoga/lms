<?php
App::uses('ContactsProduct', 'Model');

/**
 * ContactsProduct Test Case
 *
 */
class ContactsProductTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contacts_product',
		'app.contact',
		'app.communication',
		'app.user',
		'app.group',
		'app.email',
		'app.uploaded_file',
		'app.emails_uploaded_file',
		'app.product_fields_value',
		'app.product_field',
		'app.product',
		'app.product_type',
		'app.custom_field_value',
		'app.custom_field'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ContactsProduct = ClassRegistry::init('ContactsProduct');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ContactsProduct);

		parent::tearDown();
	}

}
