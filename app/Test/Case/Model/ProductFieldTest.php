<?php
App::uses('ProductField', 'Model');

/**
 * ProductField Test Case
 *
 */
class ProductFieldTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_field',
		'app.product',
		'app.product_type',
		'app.contacts',
		'app.contacts_product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductField = ClassRegistry::init('ProductField');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductField);

		parent::tearDown();
	}

}
