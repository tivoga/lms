<?php
App::uses('ProductFieldsValue', 'Model');

/**
 * ProductFieldsValue Test Case
 *
 */
class ProductFieldsValueTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_fields_value',
		'app.product_field',
		'app.product',
		'app.product_type',
		'app.contact',
		'app.custom_field_value',
		'app.custom_field'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductFieldsValue = ClassRegistry::init('ProductFieldsValue');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductFieldsValue);

		parent::tearDown();
	}

}
