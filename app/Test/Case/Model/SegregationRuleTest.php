<?php
App::uses('SegregationRule', 'Model');

/**
 * SegregationRule Test Case
 *
 */
class SegregationRuleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.segregation_rule',
		'app.user',
		'app.group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SegregationRule = ClassRegistry::init('SegregationRule');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SegregationRule);

		parent::tearDown();
	}

}
