<?php
App::uses('Communication', 'Model');

/**
 * Communication Test Case
 *
 */
class CommunicationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.communication',
		'app.user',
		'app.group',
		'app.contact',
		'app.custom_field_value',
		'app.custom_field'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Communication = ClassRegistry::init('Communication');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Communication);

		parent::tearDown();
	}

}
