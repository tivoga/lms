<?php
App::uses('Contact', 'Model');

/**
 * Contact Test Case
 *
 */
class ContactTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contact',
		'app.communication',
		'app.user',
		'app.group',
		'app.email',
		'app.uploaded_file',
		'app.emails_uploaded_file',
		'app.product_fields_value',
		'app.product_field',
		'app.product',
		'app.product_type',
		'app.products',
		'app.contacts_product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Contact = ClassRegistry::init('Contact');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contact);

		parent::tearDown();
	}

}
