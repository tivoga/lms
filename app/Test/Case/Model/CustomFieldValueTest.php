<?php
App::uses('CustomFieldValue', 'Model');

/**
 * CustomFieldValue Test Case
 *
 */
class CustomFieldValueTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.custom_field_value'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CustomFieldValue = ClassRegistry::init('CustomFieldValue');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CustomFieldValue);

		parent::tearDown();
	}

}
