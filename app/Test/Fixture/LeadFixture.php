<?php
/**
 * LeadFixture
 *
 */
class LeadFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'Leads';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'Id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'OpportunityTitle' => array('type' => 'string', 'null' => true, 'default' => null),
		'ContactID' => array('type' => 'integer', 'null' => true, 'default' => null),
		'AffiliateId' => array('type' => 'integer', 'null' => true, 'default' => null),
		'UserID' => array('type' => 'integer', 'null' => true, 'default' => null),
		'StageID' => array('type' => 'integer', 'null' => true, 'default' => null),
		'StatusID' => array('type' => 'integer', 'null' => true, 'default' => null),
		'Leadsource' => array('type' => 'string', 'null' => true, 'default' => null),
		'Objection' => array('type' => 'string', 'null' => true, 'default' => null),
		'ProjectedRevenueLow' => array('type' => 'float', 'null' => true, 'default' => null),
		'ProjectedRevenueHigh' => array('type' => 'float', 'null' => true, 'default' => null),
		'OpportunityNotes' => array('type' => 'string', 'null' => true, 'default' => null),
		'DateCreated' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'LastUpdated' => array('type' => 'timestamp', 'null' => true),
		'LastUpdatedBy' => array('type' => 'integer', 'null' => true, 'default' => null),
		'CreatedBy' => array('type' => 'integer', 'null' => true, 'default' => null),
		'EstimatedCloseDate' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'NextActionDate' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'NextActionNotes' => array('type' => 'string', 'null' => true, 'default' => null),
		'indexes' => array(
			
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'Id' => 1,
			'OpportunityTitle' => 'Lorem ipsum dolor sit amet',
			'ContactID' => 1,
			'AffiliateId' => 1,
			'UserID' => 1,
			'StageID' => 1,
			'StatusID' => 1,
			'Leadsource' => 'Lorem ipsum dolor sit amet',
			'Objection' => 'Lorem ipsum dolor sit amet',
			'ProjectedRevenueLow' => 1,
			'ProjectedRevenueHigh' => 1,
			'OpportunityNotes' => 'Lorem ipsum dolor sit amet',
			'DateCreated' => '2014-06-09 12:52:43',
			'LastUpdated' => 1402298563,
			'LastUpdatedBy' => 1,
			'CreatedBy' => 1,
			'EstimatedCloseDate' => '2014-06-09 12:52:43',
			'NextActionDate' => '2014-06-09 12:52:43',
			'NextActionNotes' => 'Lorem ipsum dolor sit amet'
		),
	);

}
