<?php
/**
 * ContactsProductFixture
 *
 */
class ContactsProductFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'Contacts_products';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'contact_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'product_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'contract_period' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'contract_start' => array('type' => 'date', 'null' => true, 'default' => null),
		'contract_status' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'contract_quoted' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'contract_paid' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'by_contact' => array('column' => 'contact_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'contact_id' => 1,
			'product_id' => 1,
			'contract_period' => 1,
			'contract_start' => '2014-07-22',
			'contract_status' => 'Lorem ipsum dolor sit amet',
			'contract_quoted' => 1,
			'contract_paid' => 1
		),
	);

}
