<div class="customFields form">
<?php echo $this->Form->create('CustomField'); ?>
	<fieldset>
		<legend><?php echo __('Add Custom Field'); ?></legend>
	<?php
		echo $this->Form->input('Label');
		echo $this->Form->input('Values');
		echo $this->Form->input('DataType');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Custom Fields'), array('action' => 'index')); ?></li>
	</ul>
</div>
