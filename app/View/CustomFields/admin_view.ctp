<div class="customFields view">
<h2><?php echo __('Custom Field'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($customField['CustomField']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Label'); ?></dt>
		<dd>
			<?php echo h($customField['CustomField']['Label']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Values'); ?></dt>
		<dd>
			<?php echo h($customField['CustomField']['Values']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DataType'); ?></dt>
		<dd>
			<?php echo h($customField['CustomField']['DataType']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Custom Field'), array('action' => 'edit', $customField['CustomField']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Custom Field'), array('action' => 'delete', $customField['CustomField']['id']), array(), __('Are you sure you want to delete # %s?', $customField['CustomField']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Custom Fields'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Custom Field'), array('action' => 'add')); ?> </li>
	</ul>
</div>
