<div class="productTypes form wid90">
<?php echo $this->Form->create('ProductType'); ?>
	<fieldset>
		<legend><?php echo __('Add Product Type'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description');
        echo $this->Form->input('user_id',array('type' =>'hidden', 'default' => $this->Session->read('Auth.User')['id']));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Add Product Type')); ?>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Types'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
