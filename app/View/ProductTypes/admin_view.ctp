<div class="productTypes view wid90">
<h2><?php echo __('Product Type'); ?></h2>
	<dl class="dl-horizontal">
<!--		<dt>--><?php //echo __('Id'); ?><!--</dt>-->
<!--		<dd>-->
<!--			--><?php //echo h($productType['ProductType']['id']); ?>
<!--			&nbsp;-->
<!--		</dd>-->
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($productType['ProductType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($productType['ProductType']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($productType['ProductType']['created']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Created By'); ?></dt>
        <dd>
            <?php echo h($productType['CreatedBy']['username']); ?>
            &nbsp;
        </dd>
	</dl>

    <h3><?php echo __('Related Products'); ?></h3>
    <?php if (!empty($productType['Product'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <!--		<th>--><?php //echo __('Id'); ?><!--</th>-->
                <th><?php echo __('Name'); ?></th>
                <th><?php echo __('Description'); ?></th>
                <th><?php echo __('Created'); ?></th>
                <!--		<th>--><?php //echo __('Product Type Id'); ?><!--</th>-->
                <th class=""><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($productType['Product'] as $product): ?>
                <tr>
<!--                    <td>--><?php //echo $product['id']; ?><!--</td>-->
                    <td><?php echo $product['name']; ?></td>
                    <td><?php echo $product['description']; ?></td>
                    <td><?php echo $product['created']; ?></td>
<!--                    <td>--><?php //echo $product['product_type_id']; ?><!--</td>-->
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('controller' => 'products', 'action' => 'view', $product['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('controller' => 'products', 'action' => 'edit', $product['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'products', 'action' => 'delete', $product['id']), array(), __('Are you sure you want to delete # %s?', $product['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    <?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')
        , array('role' =>'button', 'class'=>'btn btn-success')); ?>

</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('Edit Product Type'), array('action' => 'edit', $productType['ProductType']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete Product Type'), array('action' => 'delete', $productType['ProductType']['id']), array(), __('Are you sure you want to delete # %s?', $productType['ProductType']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Types'), array('action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Type'), array('action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
<!--<div class="related">-->
<!---->
<!--</div>-->
