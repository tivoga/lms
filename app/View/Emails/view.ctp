<div class="emails view">
<h2><?php echo __('Email'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($email['Email']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subject'); ?></dt>
		<dd>
			<?php echo h($email['Email']['subject']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd>
			<?php echo h($email['Email']['body']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($email['User']['username'], array('controller' => 'users', 'action' => 'view', $email['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($email['Email']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact'); ?></dt>
		<dd>
			<?php echo $this->Html->link($email['Contact']['FirstName'], array('controller' => 'contacts', 'action' => 'view', $email['Contact']['Id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Email'), array('action' => 'edit', $email['Email']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Email'), array('action' => 'delete', $email['Email']['id']), array(), __('Are you sure you want to delete # %s?', $email['Email']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Emails'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Email'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploaded Files'), array('controller' => 'uploaded_files', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uploaded File'), array('controller' => 'uploaded_files', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Uploaded Files'); ?></h3>
	<?php if (!empty($email['UploadedFile'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Location'); ?></th>
		<th><?php echo __('Contact Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($email['UploadedFile'] as $uploadedFile): ?>
		<tr>
			<td><?php echo $uploadedFile['id']; ?></td>
			<td><?php echo $uploadedFile['user_id']; ?></td>
			<td><?php echo $uploadedFile['created']; ?></td>
			<td><?php echo $uploadedFile['location']; ?></td>
			<td><?php echo $uploadedFile['contact_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'uploaded_files', 'action' => 'view', $uploadedFile['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'uploaded_files', 'action' => 'edit', $uploadedFile['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'uploaded_files', 'action' => 'delete', $uploadedFile['id']), array(), __('Are you sure you want to delete # %s?', $uploadedFile['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Uploaded File'), array('controller' => 'uploaded_files', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
