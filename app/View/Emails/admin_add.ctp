<div class="emails form">
<?php echo $this->Form->create('Email'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Email'); ?></legend>
	<?php
		echo $this->Form->input('subject');
		echo $this->Form->input('body');
		echo $this->Form->input('user_id');
		echo $this->Form->input('contact_id');
		echo $this->Form->input('UploadedFile');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Emails'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploaded Files'), array('controller' => 'uploaded_files', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uploaded File'), array('controller' => 'uploaded_files', 'action' => 'add')); ?> </li>
	</ul>
</div>
