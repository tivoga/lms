<div class="productFields view">
<h2><?php echo __('Product Field'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productField['ProductField']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Label'); ?></dt>
		<dd>
			<?php echo h($productField['ProductField']['label']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($productField['ProductField']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productField['Product']['name'], array('controller' => 'products', 'action' => 'view', $productField['Product']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product Field'), array('action' => 'edit', $productField['ProductField']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product Field'), array('action' => 'delete', $productField['ProductField']['id']), array(), __('Are you sure you want to delete # %s?', $productField['ProductField']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Fields'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Field'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
	</ul>
</div>
