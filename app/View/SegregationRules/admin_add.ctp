<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_seg_rules');
?>
<div class="segregationRules form wid90">
<?php echo $this->Form->create('SegregationRule'); ?>
	<fieldset>
		<h2><?php echo __('Add Segregation Rule'); ?></h2>
	<?php
        echo $this->Form->input('name');
        echo $this->Form->input('query');
        echo $this->Form->input('user_id',array('label'=>'Account Manager'));
        echo $this->Form->input('created_by',array('type' =>'hidden', 'default' => $this->Session->read('Auth.User')['id']));
        echo $this->Form->input('over_ride');
	?>
        <p>Precedes other Normal Rules</p>
	</fieldset>

    <div class="clearfix">
        <div class="pull-left">
            <?php echo $this->Form->submit('Create Rule',
                array('class' => 'btn btn-success', 'title' => 'Save changes')
            ); ?>
        </div>
        <div class="pull-left" style="margin-left: 10px;">
            <div class="submit">
                <?php echo $this->Html->link(__('Cancel Changes'), array('action' => 'index')
                    , array('role' =>'button', 'class'=>'btn btn-danger')); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    <?php echo $this->element('seg_ref', array('fields' => $fields));?>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!---->
<!--		<li>--><?php //echo $this->Html->link(__('List Segregation Rules'), array('action' => 'index')); ?><!--</li>-->
<!--	</ul>-->
<!--</div>-->
