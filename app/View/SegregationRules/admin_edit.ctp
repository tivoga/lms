<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_seg_rules');
?>
<div class="segregationRules form wid90">
    <?php echo $this->Form->create('SegregationRule'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Segregation Rule'); ?></legend>
	<?php
        echo $this->Form->input('id');
        echo $this->Form->input('name');
        echo $this->Form->input('query');
        echo $this->Form->input('user_id', array('label'=>'Account Manager'));
        echo $this->Form->input('over_ride');
	?>
        <p>Precedes other Normal Rules</p>
	</fieldset>
<?php echo $this->Form->end(__('Save Changes')); ?>
    <?php echo $this->element('seg_ref', array('fields' => $fields));?>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!---->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('SegregationRule.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SegregationRule.id'))); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Segregation Rules'), array('action' => 'index')); ?><!--</li>-->
<!--	</ul>-->
<!--</div>-->
