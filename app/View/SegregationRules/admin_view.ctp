<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_seg_rules');
?>
<div class="segregationRules view wid90">
<!--    --><?php //debug($segregationRule); ?>
<h2><?php echo __('Segregation Rule'); ?></h2>
	<dl>
<!--		<dt>--><?php //echo __('Id'); ?><!--</dt>-->
<!--		<dd>-->
<!--			--><?php //echo h($segregationRule['SegregationRule']['id']); ?>
<!--			&nbsp;-->
<!--		</dd>-->
        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($segregationRule['SegregationRule']['name']); ?>
            &nbsp;
        </dd>
		<dt><?php echo __('Query'); ?></dt>
		<dd>
			<?php echo h($segregationRule['SegregationRule']['query']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Account Manager'); ?></dt>
		<dd>
			<?php echo h($segregationRule['User']['username']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Created By'); ?></dt>
        <dd>
            <?php echo h($segregationRule['CreatedBy']['username']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Over Ride'); ?></dt>
		<dd>
			<?php echo h($segregationRule['SegregationRule']['over_ride']); ?>
			&nbsp;
		</dd>
	</dl>
    <?php echo $this->Html->link(__('Edit Segregation Rule'),
        array('action' => 'edit', $segregationRule['SegregationRule']['id'])
        , array('role' =>'button', 'class'=>'btn btn-success')); ?>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('Edit Segregation Rule'), array('action' => 'edit', $segregationRule['SegregationRule']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete Segregation Rule'), array('action' => 'delete', $segregationRule['SegregationRule']['id']), array(), __('Are you sure you want to delete # %s?', $segregationRule['SegregationRule']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Segregation Rules'), array('action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Segregation Rule'), array('action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
