<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_seg_rules');
//debug($segregationRules );
?>
<div class="segregationRules index wid90">
	<h2><?php echo __('Segregation Rules'); ?></h2>
<!--    <button title="Sync Custom Fields with Infusion Soft" type="button" class="btn btn-success ">Sync with Infusion</button>-->
    <div class="row">
        <div class="col-md-2">
            <?php echo $this->Html->link(__('Sync to Infusionsoft'),
                array('controller' => 'CustomFields', 'action' => 'admin_import'),
                array('role'=> "button", 'class'=>"btn btn-success", 'title'=>"Sync Custom Fields with Infusion Soft")); ?>
        </div>
        <div class="col-md-2">
            <?php echo $this->Html->link(__('New Rule'), array('action' => 'add')
                , array('role' =>'button', 'class'=>'btn btn-success')); ?>
        </div>
    </div>
    <p></p>
	<table cellpadding="0" cellspacing="0">
	<tr>
<!--			<th>--><?php //echo $this->Paginator->sort('id'); ?><!--</th>-->
<!--			<th>--><?php //echo $this->Paginator->sort('query'); ?><!--</th>-->
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('user_id'); ?></th>
        <th><?php echo $this->Paginator->sort('created_by'); ?></th>
        <th><?php echo $this->Paginator->sort('over_ride'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($segregationRules as $segregationRule): ?>
	<tr>
<!--		<td>--><?php //echo h($segregationRule['SegregationRule']['id']); ?><!--&nbsp;</td>-->
<!--		<td>--><?php //echo h($segregationRule['SegregationRule']['query']); ?><!--&nbsp;</td>-->
        <td><?php echo h($segregationRule['SegregationRule']['name']); ?>&nbsp;</td>
        <td><?php echo $this->Html->link($segregationRule['User']['username'],
            array('controller' => 'users', 'action' => 'view', $segregationRule['User']['id'])); ?></td>

        <td><?php echo h($segregationRule['CreatedBy']['username']); ?>&nbsp;</td>
        <td><?php echo h($segregationRule['SegregationRule']['over_ride']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $segregationRule['SegregationRule']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $segregationRule['SegregationRule']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $segregationRule['SegregationRule']['id']), array(), __('Are you sure you want to delete # %s?', $segregationRule['SegregationRule']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Segregation Rule'), array('action' => 'add')); ?><!--</li>-->
<!--	</ul>-->
<!--</div>-->
