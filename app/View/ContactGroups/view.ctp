<div class="contactGroups view">
<h2><?php echo __('Contact Group'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($contactGroup['ContactGroup']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($contactGroup['ContactGroup']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contact Group'), array('action' => 'edit', $contactGroup['ContactGroup']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contact Group'), array('action' => 'delete', $contactGroup['ContactGroup']['id']), array(), __('Are you sure you want to delete # %s?', $contactGroup['ContactGroup']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contact Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact Group'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Contacts'); ?></h3>
	<?php if (!empty($contactGroup['Contact'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('JobTitle'); ?></th>
		<th><?php echo __('LastName'); ?></th>
		<th><?php echo __('FirstName'); ?></th>
		<th><?php echo __('MiddleName'); ?></th>
		<th><?php echo __('Nickname'); ?></th>
		<th><?php echo __('OwnerID'); ?></th>
		<th><?php echo __('Groups'); ?></th>
		<th><?php echo __('LastUpdated'); ?></th>
		<th><?php echo __('LastUpdatedBy'); ?></th>
		<th><?php echo __('Leadsource'); ?></th>
		<th><?php echo __('LeadSourceId'); ?></th>
		<th><?php echo __('Phone1'); ?></th>
		<th><?php echo __('Phone1Ext'); ?></th>
		<th><?php echo __('Phone1Type'); ?></th>
		<th><?php echo __('Phone2'); ?></th>
		<th><?php echo __('Phone2Ext'); ?></th>
		<th><?php echo __('Phone2Type'); ?></th>
		<th><?php echo __('Phone3'); ?></th>
		<th><?php echo __('Phone3Ext'); ?></th>
		<th><?php echo __('Phone3Type'); ?></th>
		<th><?php echo __('Phone4'); ?></th>
		<th><?php echo __('Phone4Ext'); ?></th>
		<th><?php echo __('Phone4Type'); ?></th>
		<th><?php echo __('Phone5'); ?></th>
		<th><?php echo __('Phone5Ext'); ?></th>
		<th><?php echo __('Phone5Type'); ?></th>
		<th><?php echo __('PostalCode'); ?></th>
		<th><?php echo __('PostalCode2'); ?></th>
		<th><?php echo __('PostalCode3'); ?></th>
		<th><?php echo __('ReferralCode'); ?></th>
		<th><?php echo __('SpouseName'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('State2'); ?></th>
		<th><?php echo __('State3'); ?></th>
		<th><?php echo __('StreetAddress1'); ?></th>
		<th><?php echo __('StreetAddress2'); ?></th>
		<th><?php echo __('Suffix'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Validated'); ?></th>
		<th><?php echo __('Website'); ?></th>
		<th><?php echo __('Address1Type'); ?></th>
		<th><?php echo __('Address2Street1'); ?></th>
		<th><?php echo __('Address2Street2'); ?></th>
		<th><?php echo __('Address2Type'); ?></th>
		<th><?php echo __('Address3Street1'); ?></th>
		<th><?php echo __('Address3Street2'); ?></th>
		<th><?php echo __('Address3Type'); ?></th>
		<th><?php echo __('Anniversary'); ?></th>
		<th><?php echo __('AssistantName'); ?></th>
		<th><?php echo __('AssistantPhone'); ?></th>
		<th><?php echo __('BillingInformation'); ?></th>
		<th><?php echo __('Birthday'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('City2'); ?></th>
		<th><?php echo __('City3'); ?></th>
		<th><?php echo __('Company'); ?></th>
		<th><?php echo __('AccountId'); ?></th>
		<th><?php echo __('CompanyID'); ?></th>
		<th><?php echo __('ContactNotes'); ?></th>
		<th><?php echo __('ContactType'); ?></th>
		<th><?php echo __('Country'); ?></th>
		<th><?php echo __('Country2'); ?></th>
		<th><?php echo __('Country3'); ?></th>
		<th><?php echo __('CreatedBy'); ?></th>
		<th><?php echo __('DateCreated'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('EmailAddress2'); ?></th>
		<th><?php echo __('EmailAddress3'); ?></th>
		<th><?php echo __('Fax1'); ?></th>
		<th><?php echo __('Fax1Type'); ?></th>
		<th><?php echo __('Fax2'); ?></th>
		<th><?php echo __('Fax2Type'); ?></th>
		<th><?php echo __('ZipFour1'); ?></th>
		<th><?php echo __('ZipFour2'); ?></th>
		<th><?php echo __('ZipFour3'); ?></th>
		<th><?php echo __('Archived'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('FirstName2'); ?></th>
		<th><?php echo __('LastName2'); ?></th>
		<th><?php echo __('FirstName3'); ?></th>
		<th><?php echo __('LastName3'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($contactGroup['Contact'] as $contact): ?>
		<tr>
			<td><?php echo $contact['Id']; ?></td>
			<td><?php echo $contact['JobTitle']; ?></td>
			<td><?php echo $contact['LastName']; ?></td>
			<td><?php echo $contact['FirstName']; ?></td>
			<td><?php echo $contact['MiddleName']; ?></td>
			<td><?php echo $contact['Nickname']; ?></td>
			<td><?php echo $contact['OwnerID']; ?></td>
			<td><?php echo $contact['Groups']; ?></td>
			<td><?php echo $contact['LastUpdated']; ?></td>
			<td><?php echo $contact['LastUpdatedBy']; ?></td>
			<td><?php echo $contact['Leadsource']; ?></td>
			<td><?php echo $contact['LeadSourceId']; ?></td>
			<td><?php echo $contact['Phone1']; ?></td>
			<td><?php echo $contact['Phone1Ext']; ?></td>
			<td><?php echo $contact['Phone1Type']; ?></td>
			<td><?php echo $contact['Phone2']; ?></td>
			<td><?php echo $contact['Phone2Ext']; ?></td>
			<td><?php echo $contact['Phone2Type']; ?></td>
			<td><?php echo $contact['Phone3']; ?></td>
			<td><?php echo $contact['Phone3Ext']; ?></td>
			<td><?php echo $contact['Phone3Type']; ?></td>
			<td><?php echo $contact['Phone4']; ?></td>
			<td><?php echo $contact['Phone4Ext']; ?></td>
			<td><?php echo $contact['Phone4Type']; ?></td>
			<td><?php echo $contact['Phone5']; ?></td>
			<td><?php echo $contact['Phone5Ext']; ?></td>
			<td><?php echo $contact['Phone5Type']; ?></td>
			<td><?php echo $contact['PostalCode']; ?></td>
			<td><?php echo $contact['PostalCode2']; ?></td>
			<td><?php echo $contact['PostalCode3']; ?></td>
			<td><?php echo $contact['ReferralCode']; ?></td>
			<td><?php echo $contact['SpouseName']; ?></td>
			<td><?php echo $contact['State']; ?></td>
			<td><?php echo $contact['State2']; ?></td>
			<td><?php echo $contact['State3']; ?></td>
			<td><?php echo $contact['StreetAddress1']; ?></td>
			<td><?php echo $contact['StreetAddress2']; ?></td>
			<td><?php echo $contact['Suffix']; ?></td>
			<td><?php echo $contact['Title']; ?></td>
			<td><?php echo $contact['Username']; ?></td>
			<td><?php echo $contact['Validated']; ?></td>
			<td><?php echo $contact['Website']; ?></td>
			<td><?php echo $contact['Address1Type']; ?></td>
			<td><?php echo $contact['Address2Street1']; ?></td>
			<td><?php echo $contact['Address2Street2']; ?></td>
			<td><?php echo $contact['Address2Type']; ?></td>
			<td><?php echo $contact['Address3Street1']; ?></td>
			<td><?php echo $contact['Address3Street2']; ?></td>
			<td><?php echo $contact['Address3Type']; ?></td>
			<td><?php echo $contact['Anniversary']; ?></td>
			<td><?php echo $contact['AssistantName']; ?></td>
			<td><?php echo $contact['AssistantPhone']; ?></td>
			<td><?php echo $contact['BillingInformation']; ?></td>
			<td><?php echo $contact['Birthday']; ?></td>
			<td><?php echo $contact['City']; ?></td>
			<td><?php echo $contact['City2']; ?></td>
			<td><?php echo $contact['City3']; ?></td>
			<td><?php echo $contact['Company']; ?></td>
			<td><?php echo $contact['AccountId']; ?></td>
			<td><?php echo $contact['CompanyID']; ?></td>
			<td><?php echo $contact['ContactNotes']; ?></td>
			<td><?php echo $contact['ContactType']; ?></td>
			<td><?php echo $contact['Country']; ?></td>
			<td><?php echo $contact['Country2']; ?></td>
			<td><?php echo $contact['Country3']; ?></td>
			<td><?php echo $contact['CreatedBy']; ?></td>
			<td><?php echo $contact['DateCreated']; ?></td>
			<td><?php echo $contact['Email']; ?></td>
			<td><?php echo $contact['EmailAddress2']; ?></td>
			<td><?php echo $contact['EmailAddress3']; ?></td>
			<td><?php echo $contact['Fax1']; ?></td>
			<td><?php echo $contact['Fax1Type']; ?></td>
			<td><?php echo $contact['Fax2']; ?></td>
			<td><?php echo $contact['Fax2Type']; ?></td>
			<td><?php echo $contact['ZipFour1']; ?></td>
			<td><?php echo $contact['ZipFour2']; ?></td>
			<td><?php echo $contact['ZipFour3']; ?></td>
			<td><?php echo $contact['archived']; ?></td>
			<td><?php echo $contact['user_id']; ?></td>
			<td><?php echo $contact['firstName2']; ?></td>
			<td><?php echo $contact['lastName2']; ?></td>
			<td><?php echo $contact['firstName3']; ?></td>
			<td><?php echo $contact['lastName3']; ?></td>
			<td><?php echo $contact['group_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'contacts', 'action' => 'view', $contact['Id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'contacts', 'action' => 'edit', $contact['Id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'contacts', 'action' => 'delete', $contact['Id']), array(), __('Are you sure you want to delete # %s?', $contact['Id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
