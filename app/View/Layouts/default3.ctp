<?php
/**
 *
 *
 * Sevron Lead management Tool :
 * Suite to manage client leads collected from infusion soft campaigns
 *
 *
 * @copyright     Copyright (c) Arka Soft Technologies - India.
 * @link
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'Sevron LMS: Lead Management System');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
        echo $this->Html->css('style-final');
		echo $this->Html->css('cake.generic');
		echo $this->Html->css('style');

//		echo $this->Html->css('bootstrap.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<link rel ='stylesheet' href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
	<style>
            .gridbox div.objbox table.obj tbody tr.ev_dhx_terrace td
            {
                height:30px !important;
            }
            .gridbox div.objbox table.obj tbody tr.odd_dhx_terrace td
            {
                height:30px !important;
            }
            html body div#content div.inner div table tbody tr td div#sdsSearch.gridbox div.xhdr table.hdr tbody tr td select
            {
                height:25px;
                margin-left:20px;
                vertical-align:middle;
                padding:3px;
            }
            html body.dhtmlx_winviewport div#guidely-number-7.guidely-number
            {
                /*opacity:0;
                filter:alpha(opacity=0);*/
            }
        </style>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.1/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<!--        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.0/css/jquery.dataTables.css">-->
        <link rel ='stylesheet' href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">
        <link rel ='stylesheet' href="//cdn.datatables.net/responsive/1.0.1/css/dataTables.responsive.css">

        <script type="application/javascript">
            $(document).ready(function() {
                var linkID = '<?php echo $this->fetch('linkId');?>';
                    $("#"+linkID).parent().parent().parent().attr('class', 'on');

                $("#content input:submit").addClass('btn btn-success');
                $("td.actions > a:not(:contains('Delete'))").addClass('btn btn-info');
                $("td.actions a:contains('Delete')").addClass('btn btn-danger');
                $(".actions ul").addClass('nav nav-pills nav-stacked');
                $(".view dl").addClass('dl-horizontal');

                // for the admin forms
                $("fieldset:not(.no-override)>div").addClass("input-group");
                $("#content input:not(.no-override, :checkbox, :submit), #content select:not(.no-override)").addClass("form-control");
                $("label:not(.no-override)").addClass("input-group-addon");

                //for the admin tables
                $("#content table:not(.no-override, .table-bordered)").addClass('table hover table-striped table-bordered');

                //for check boxes
                //the input is inside the label
//                $(".input.checkbox input:checkbox").parent().addClass("form-control");
//                $(".input.checkbox input:checkbox").parent().parent().prepend("<div class='input-group-addon'></div>");

            } );
        </script>

</head>
<body class="dhtmlx_winviewport dhtmlx_skin_dhx_terrace">
	<div id="container">
        <div style="float: right;"><h3>
                <?php echo $this->Html->link(
                    'Hello '.AuthComponent::user('username'),
                    array('admin'=>false,'controller'=>'users','action'=>'edit','plugin' => null,AuthComponent::user('id')), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                );
                ?></h3></div>
		<div id="header">
		        <div class="inner">
                    <?php echo $this->Html->link(
                    					$this->Html->image('sevron-logo.gif', array('alt' => $cakeDescription, 'border' => '0')),
                    					'/', array('escape' => false, 'id' => 'logo')
                    				);
                    			?>
                    <?php if (AuthComponent::user('id')){
                        echo $this->element('public_nav_elem');
                    }?>

                </div>


		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">

			<p>
				<?php echo $cakeVersion; ?>
			</p>
		</div>
	</div>
</body>
</html>
