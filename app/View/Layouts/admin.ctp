<?php
/**
 *
 *
 * Sevron Lead management Tool :
 * Suite to manage client leads collected from infusion soft campaigns
 *
 *
 * @copyright     Copyright (c) Arka Soft Technologies - India.
 * @link
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'Sevron LMS: Lead Management System');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<link rel ='stylesheet' href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<style>
            .gridbox div.objbox table.obj tbody tr.ev_dhx_terrace td
            {
                height:30px !important;
            }
            .gridbox div.objbox table.obj tbody tr.odd_dhx_terrace td
            {
                height:30px !important;
            }
            html body div#content div.inner div table tbody tr td div#sdsSearch.gridbox div.xhdr table.hdr tbody tr td select
            {
                height:25px;
                margin-left:20px;
                vertical-align:middle;
                padding:3px;
            }
            html body.dhtmlx_winviewport div#guidely-number-7.guidely-number
            {
                /*opacity:0;
                filter:alpha(opacity=0);*/
            }
        </style>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="application/javascript">
        $(document).ready(function() {
            var linkID = '<?php echo $this->fetch('linkId');?>';
            $("#"+linkID).parent().parent().parent().attr('class', 'on');
        } );
    </script>

</head>
<body class="dhtmlx_winviewport dhtmlx_skin_dhx_terrace">
	<div id="container">
		<div id="header">
		        <div class="inner">
                    <?php echo $this->Html->link(
                    					$this->Html->image('sevron-logo.gif', array('alt' => $cakeDescription, 'border' => '0')),
                    					'/', array('target' => '_blank', 'escape' => false, 'id' => 'logo')
                    				);?>


                </div>


		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">

			<p>
				<?php echo $cakeVersion; ?>
			</p>
		</div>
	</div>
</body>
</html>
