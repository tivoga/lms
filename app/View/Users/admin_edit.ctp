<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_admin_users');
?>
    <div class="users form wid90">
	<?php echo $this->Form->create($model); ?>
		<fieldset>
			<legend><?php echo __('Edit User'); ?></legend>
			<?php
				echo $this->Form->input('id');
				echo $this->Form->input('username', array(
					'label' => __('Username')));
				echo $this->Form->input('email', array(
					'label' => __('Email')));
                if (!empty($roles)) {
                    echo $this->Form->input('role', array(
                        'label' => __('Role'), 'values' => $roles));
                }
                if (!empty($groups)) {
                    echo $this->Form->input('group_id', array(
                        'label' => __( 'Group'), 'values' => $groups));
                }
                echo $this->Form->input('senders_email', array(
                    'label' => __( 'Senders Email')));
                echo $this->Form->input('senders_email_password', array(
                    'label' => __( 'Senders Email Password')));?>


            <div class="col-md-2 checkbox">

                <label for="UserIsAdmin" class="input-group-addon">
                    <?php
                    echo $this->Form->input('is_admin',array('label'=>false,'div'=>false));
                    ?>Is Admin
                </label>
            </div>
            <?php
                    //echo $this->Form->input('active', array(
                    //    'label' => __('Active')));
			?>
		</fieldset>
	<?php echo $this->Form->end('Save Changes'); ?>
</div>
<?php //echo $this->element('Users/admin_sidebar'); ?>