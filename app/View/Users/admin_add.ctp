<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_admin_users');
?>
    <div class="users form wid90">
	<?php echo $this->Form->create($model); ?>
		<fieldset>
			<legend><?php echo __( 'Add User'); ?></legend>
			<?php
				echo $this->Form->input('username', array(
					'label' => __( 'Username')));
				echo $this->Form->input('email', array(
					'label' => __( 'E-mail (used as login)'),
					'error' => array('isValid' => __( 'Must be a valid email address'),
						'isUnique' => __( 'An account with that email already exists'))));
				echo $this->Form->input('password', array(
					'label' => __( 'Password'),
					'type' => 'password'));
				echo $this->Form->input('temppassword', array(
					'label' => __( 'Password (confirm)'),
					'type' => 'password'));
				if (!empty($roles)) {
					echo $this->Form->input('role', array(
						'label' => __( 'Role'), 'values' => $roles));
				}
                if (!empty($groups)) {
                    echo $this->Form->input('group_id', array(
                        'label' => __( 'Group'), 'values' => $groups));
                }
                echo $this->Form->input('senders_email', array(
                    'label' => __( 'Senders Email')));
                echo $this->Form->input('senders_email_password', array(
                    'label' => __( 'Senders Email Password'))); ?>


            <div class="col-md-2 checkbox">

                <label for="UserIsAdmin" class="input-group-addon">
                <?php
                echo $this->Form->input('is_admin',array('label'=>false,'div'=>false));
                ?>Is Admin
                </label>
            </div>
            <?php
				echo $this->Form->input('active', array(
					'label' => __( 'Active'), 'type'=>'hidden', 'default'=>'1'));
                echo $this->Form->input('created_by',array(
                    'type' =>'hidden', 'default' => $this->Session->read('Auth.User')['id']));
//                echo $this->Form->input('active', array(
//                    'label' => __( 'Active'), 'type'=>'hidden', 'default'=>'1'));
			?>
		</fieldset>
	<?php echo $this->Form->end('Create Account'); ?>
</div>
<?php //echo $this->element('Users/admin_sidebar'); ?>