<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="users_index" style="width: 450px;margin: auto;">
	<h2><?php echo __( 'Login'); ?></h2>
	<?php echo $this->Session->flash('auth');?>
	<fieldset>
		<?php
			echo $this->Form->create($model, array(
				'action' => 'login',
				'id' => 'LoginForm',
                'inputDefaults' => array(
//                    'div' => 'form-group',
                    'label' => false,
//                    'wrapInput' => false,
//                    'class' => 'form-control'
                ),
                'class' => 'well'
            ));
			echo $this->Form->input('email', array(
                'placeholder' => 'Email'));
			echo $this->Form->input('password',  array(
                'placeholder' => 'Password'));
        ?>
        <div class="input checkbox">
        <label for="UserRememberMe" class="input-group-addon">
        <?php
			echo $this->Form->input('remember_me', array('type'=>'checkbox','div'=>false));?>
            Remember Me</label>
        </div>
        <?php
			echo '<p>' . $this->Html->link(__( 'I forgot my password'), array('action' => 'reset_password')) . '</p>';

			echo $this->Form->hidden('User.return_to', array(
				'value' => $return_to));
			echo $this->Form->end(__( 'Login'));
		?>
	</fieldset>
</div>
<?php //echo $this->element('Users.Users/sidebar'); ?>
