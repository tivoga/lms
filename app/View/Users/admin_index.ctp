<?php
/**
 * Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2013, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_admin_users');
?>
<div class="users wid90">
	<h2><?php echo 'Users'; ?></h2>

	<h3><?php echo  'Filter'; ?></h3>
	<?php 
	echo $this->Form->create($model, array('action' => 'index','class' => 'form-horizontal',
        'inputDefaults' => array(
            'div' => 'form-group',
            'label' => false,
            'wrapInput' => false,
            'class' => 'form-control',
        'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline'))
        ),
//            'after' => '</div>',
//            'between' => '<div class="controls">',
        'class' => 'well form-inline','role'=>'form'
        ));?>
    <?php
		echo $this->Form->input('username', array('placeholder' => 'UserName'));
		echo $this->Form->input('email', array('placeholder' => 'Email'));
        echo $this->Form->button('Search', array('div' => 'form-group','class' => 'btn btn-success'));
        echo $this->Form->end();
    ?>

    <?php echo $this->Html->link(__('Add Users'), array('admin' => true,
        'action'=>'add'), array('role' =>'button', 'class'=>'btn btn-success'));?>
    <br>
    <p></p>
<!--	--><?php //echo $this->element('pagination'); ?>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('username'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
			<th><?php echo $this->Paginator->sort('email_verified'); ?></th>
			<th><?php echo $this->Paginator->sort('active'); ?></th>
			<th><?php echo $this->Paginator->sort('created by'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __d('users', 'Actions'); ?></th>
		</tr>
			<?php
			$i = 0;
			foreach ($users as $user):
				$class = null;
				if ($i++ % 2 == 0) {
					$class = ' class="altrow"';
				}
			?>
			<tr<?php echo $class;?>>
				<td>
					<?php echo $user[$model]['username']; ?>
				</td>
				<td>
					<?php echo $user[$model]['email']; ?>
				</td>
				<td>
					<?php echo $user[$model]['email_verified'] == 1 ? __( 'Yes') : __( 'No'); ?>
				</td>
				<td>
					<?php echo $user[$model]['active'] == 1 ? __( 'Yes') : __( 'No'); ?>
				</td>
                <td>
                    <?php echo $user['CreatedBy']['username']; ?>
                </td>
                <td>
					<?php echo $user[$model]['created']; ?>
				</td>
				<td class="actions">
					<?php echo $this->Html->link(__( 'View'), array('action'=>'view', $user[$model]['id'])); ?>
					<?php echo $this->Html->link(__( 'Edit'), array('action'=>'edit', $user[$model]['id'])); ?>
					<?php echo $this->Html->link(__( 'Delete'), array('action'=>'delete', $user[$model]['id']), null, sprintf(__( 'Are you sure you want to delete # %s?'), $user[$model]['username'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php echo $this->element('pagination'); ?>
    <?php echo $this->element('paging'); ?>
</div>
<?php //echo $this->element('Users/admin_sidebar'); ?>
