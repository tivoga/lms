<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_7kXtGhfLPsl9AaBx4TAPFHIz2e7ln3U"></script>
<script>
    var map;
    function initialize() {
        var center = new google.maps.LatLng(<?php echo($center['Postcodelatlng']['latitude'].', '.$center['Postcodelatlng']['longitude'])?>);
        var mapOptions = {
            zoom: 11,
            center: center
        };
        map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
        var infowindow = new google.maps.InfoWindow({
            content: 'Destination Postal Code'
        });
        var marker = new google.maps.Marker({
            position: center,
            map: map,
            title:"Distination"
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });
        infowindow.open(map,marker);

        <?php foreach($codes as $code): ?>
        <?php foreach($contacts as $contact): ?>
            <?php if($contact['Contact']['ZipFour1'] ==
            $code['postcodelatlng']['postcode']): ?>
        var infowindow<?php echo$contact['Contact']['Id'] ?>
            = new google.maps.InfoWindow({
            content: '<?php echo $this->Html->link(h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id'])); ?>'
        });
        var marker<?php echo$contact['Contact']['Id'] ?> = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo($code['postcodelatlng']['latitude'].', '.$code['postcodelatlng']['longitude'])?>),
            map: map,
            title:'<?php echo($contact['Contact']['FirstName'])?>'
        });
        google.maps.event.addListener(marker<?php echo$contact['Contact']['Id'] ?>, 'click', function() {
            infowindow<?php echo$contact['Contact']['Id'] ?>.open(map,marker<?php echo$contact['Contact']['Id'] ?>);
        });
        infowindow<?php echo$contact['Contact']['Id'] ?>.open(map,marker<?php echo$contact['Contact']['Id'] ?>);
        <?php break; endif ?>
        <?php endforeach ?>
        <?php endforeach ?>
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

  <?php //print("<pre>".print_r($contact,true)."</pre>");
      $this->assign('linkId', 'nav_proximity');
  ?>
<div class="wid90">
    <h3>See Contacts close by</h3>
    <form class="inline_form">
    </form>


    <?php echo $this->Form->create('Proximity',
        array('class'=>'inline_form')); ?>
<!--    <fieldset>-->
        <!--    <div class="container-fluid">-->
        <div class="row">
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Account Manager:</span>
                    <?php echo $this->Form->input('user',array('class'=>"form-control",'label'=>false,'empty'=>'Select a User')); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Product or Service:</span>
                    <?php echo $this->Form->input('product',array('class'=>"form-control",'label'=>false,'empty'=>'Select a Product')); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Contract Status:</span>
                    <?php echo $this->Form->input('contract',array('class'=>"form-control",'label'=>false,'empty'=>'Select a Contract Status')); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Industry:</span>
                    <?php echo $this->Form->input('industry',array('class'=>"form-control",'label'=>false,'empty'=>'Select an Industry')); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Post Code:</span>
                    <?php echo $this->Form->input('code',array('class'=>"form-control",'type' => 'textfield','label'=>false,'placeholder'=>'Postal code')); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Distance:</span>
                    <?php echo $this->Form->input('distance',array('class'=>"form-control",'type' => 'number','label'=>false,'placeholder'=>'Distance in Miles')); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-offset-11 col-md-1">
                <?php echo $this->Form->end(array('div'=>false,'label'=>'Go','value'=>'Go')); ?>
            </div>
        </div>
<!--    </div>-->
<!--    </fieldset>-->

<div class="clearfix" style="width: 440px;" >
    <div class="pull-left">

        <div class="pull-left">
<!--            --><?php //echo $this->Form->input('code',array('class'=>"form-control",'type' => 'textfield','label'=>false,'placeholder'=>'Postal code')); ?>
        </div>
            <div class="pull-left">
        <div class="pull-left" style="margin-left: 20px;">
<!--        --><?php //echo $this->Form->input('distance',array('class'=>"form-control",'type' => 'number','label'=>false,'placeholder'=>'Distance in Miles')); ?>
        </div>

        </div>
<div class="pull-right">
<!--    --><?php //echo $this->Form->end(array('div'=>false,'label'=>'Go','value'=>'Go')); ?>
</div>
</div>
    </div>
<!--    <p>Put in a zip code :  <input id="zipcode" type="text" style="width: 100px;" placeholder="PR26 7PA"> </p>-->

<div class="contacts_index">
    <div id="map-canvas" style="width: 100%; height: 600px; margin: auto;">
    </div>
<!--    <div id="porx_conctact" style="float: left;margin-left:5px;width:18%;min-width: 150px;">-->
<!--        <p>List of Contacts close by:</p>-->
<!--        <ul>-->
<!--        --><?php //foreach($contacts as $contact){
//            echo '<li>'.$this->Html->link(h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id'])).'</li>';
//        }?>
<!--        </ul>-->
<!--    </div>-->
<!--    --><?php //debug($codes);
//    debug($center);
//    debug($contacts);?>
</div>
</div>