<div class="col-md-3">

    <?php echo $this->element('contact_summary_elem', array('contact' => $contact['Contact'], 'user'=>$contact['User']));?>
    <!--        <ul class="nav nav-pills nav-stacked">-->
    <!--            <li class="active"><a href="#">Home</a></li>-->
    <!--            <li><a href="#">Profile</a></li>-->
    <!--            <li><a href="#">Messages</a></li>-->
    <!--        </ul>-->
    <!--	<ul>-->
    <!--		<li>--><?php //echo $this->Form->postLink(__('Delete Contact'), array('action' => 'delete', $contact['Contact']['Id']), array(), __('Are you sure you want to delete # %s?', $contact['Contact']['Id'])); ?><!-- </li>-->
    <!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?><!-- </li>-->
    <!--		<li>--><?php //echo $this->Html->link(__('New Contact'), array('action' => 'add')); ?><!-- </li>-->
    <!--	</ul>-->
<!--    <ul class="nav nav-stacked">-->
<!--        <li>-->
    <div class="col-md-9">
            <?php echo $this->Html->link(__('Edit Contact'),
                array('action' => 'edit', $contact['Contact']['Id']),
                array('role'=> "button", 'class'=>"btn-block btn btn-success")); ?>
<!--        </li>-->
<!--        <li>-->
            <?php

            if($contact['Contact']['archived']===null){
                echo $this->Html->link(__('Move to Main List'),
                    array('action' => 'unarchive_contact', $contact['Contact']['Id']),
                    array('role'=> "button", 'class'=>"btn-block btn btn-info"));
            }elseif($contact['Contact']['archived']==true){
                echo $this->Html->link(__('UnArchive Contact'),
                    array('action' => 'unarchive_contact', $contact['Contact']['Id']),
                    array('role'=> "button", 'class'=>"btn-block btn btn-info"));
            }else{
                echo $this->Html->link(__('Archive Contact'),
                    array('action' => 'archive_contact', $contact['Contact']['Id']),
                    array('role'=> "button", 'class'=>"btn-block btn btn-info"));
            }
            echo('<p/>');
            if($contact['Contact']['group_id']){
                echo $this->Html->link(__('More Contacts in Group'),
                    array('action' => 'grouped_contacts', $contact['Contact']['group_id']),
                    array('role'=> "button", 'class'=>"btn-block btn btn-success"));
                echo '<br>';
            }
            ?>
    </div>
<!--    </li>-->
<!--    </ul>-->

    <!--    <a href="#" ></a>-->


</div>
<div class="col-md-9">
<!--   --><?php //debug($contact);?>
<!--<h2>-->
<!--    --><?php //echo h($contact['Contact']['Title']).' '; ?>
<!--    --><?php //echo h($contact['Contact']['FirstName']).' '; ?>
<!--    --><?php //echo h($contact['Contact']['MiddleName']).' '; ?>
<!--    --><?php //echo h($contact['Contact']['LastName']).' '; ?>
<!--    --><?php //echo h($contact['Contact']['Suffix']).', '; ?>
<!--    --><?php //if ($contact['Contact']['JobTitle']){
//        echo h($contact['Contact']['JobTitle']);echo(' , ');
//    }
//    echo h($contact['Contact']['Company']);?><!--</h2>-->

<script type="application/javascript">
    $('#main_tab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
<script type="application/javascript">
    $('#contacts a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
<p></p>
<ul id="main_tab" class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#Contacts" role="tab" data-toggle="tab">Contact Details</a></li>
    <li><a href="#Communications" role="tab" data-toggle="tab">Communications</a></li>
    <li><a href="#Products" role="tab" data-toggle="tab">Products</a></li>
    <li><a href="#Files" role="tab" data-toggle="tab">Files</a></li>
    <li><a href="#Misc" role="tab" data-toggle="tab">Extra Data</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane " id="Communications">
        <?php echo $this->element('communications_elem', array('contact' => $contact));?>
    </div>
    <div class="tab-pane" id="Products">
        <?php echo $this->element('products_elem', array('contact' => $contact));?>
    </div>
    <div class="tab-pane" id="Files">
        <?php echo $this->element('files_elem', array('contact' => $contact));?>
    </div>
    <div class="tab-pane active" id="Contacts">
        <br>
<!--            <a role="button" href="#" class="btn btn-success">Edit Contact Data</a>-->
        <p></p>
        <ul id="contacts" class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#pcont" role="tab" data-toggle="tab">Primary Contact</a></li>
            <li><a href="#scont" role="tab" data-toggle="tab">Additional contacts</a></li>
            <li><a href="#Addresses" role="tab" data-toggle="tab">Addresses</a></li>
            <li><a href="#Fax" role="tab" data-toggle="tab">Fax</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane" id="Fax">
                <!--                    --><?php //echo $this->Html->link(__('Edit Faxes'), array('action' => 'edit', $contact['Contact']['Id']),array('role'=>"button", 'class'=>"btn btn-primary")); ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo __('Fax1') .'-'. h($contact['Contact']['Fax1Type']); ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php echo h($contact['Contact']['Fax1']); ?>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo __('Fax2') .'-'. h($contact['Contact']['Fax2Type']); ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php echo h($contact['Contact']['Fax2']); ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane active" id="pcont">
                <div class="panel panel-default">
<!--                    <div class="panel-heading">-->
<!--                        <h3 class="panel-title">--><?php //echo __('Primary Email'); ?><!--</h3>-->
<!--                    </div>-->
                    <div class="panel-body">
                        <dl>
<!--                            <dt>--><?php //echo __('Primary Contact'); ?><!--</dt>-->
<!--                            <dd>-->
<!--                                --><?php //echo h($contact['Contact']['firstName2']); ?>
<!--                                &nbsp;-->
<!--                                --><?php //echo h($contact['Contact']['lastName2']); ?>
<!--                                &nbsp;-->
<!--                            </dd>-->

                            <dt><?php echo __('Primary Email'); ?></dt>
                            <dd>
                                <?php echo h($contact['Contact']['Email']); ?>
                                &nbsp;
                            </dd>
                            <dt><?php echo __('Phone'); ?></dt>
                            <dd>
                                <?php echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone1'));?>
                            </dd>
<!--                            --><?php //echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone3'));?>
<!--                            --><?php //echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone4'));?>
<!--                            --><?php //echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone5'));?>
                        </dl>


<!--                        <dl>-->
                                <?php //echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone1'));?>
                            <!--                <dt>--><?php //echo __('EmailAddress2'); ?><!--</dt>-->
                            <!--                <dd>-->
<!--                            --><?php //echo h($contact['Contact']['EmailAddress2']); ?>
<!--                            &nbsp;-->
                            <!--                </dd>-->
                            <!--                <dt>--><?php //echo __('EmailAddress3'); ?><!--</dt>-->
                            <!--                <dd>-->
<!--                            --><?php //echo h($contact['Contact']['EmailAddress3']); ?>
<!--                            &nbsp;-->
                            <!--                </dd>-->
<!--                        </dl>-->
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="scont">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo 'Secondary'; ?></h3>
                    </div>
                    <div class="panel-body">
                        <dl>
                            <dt><?php echo __('Name'); ?></dt>
                            <dd>
                                <?php echo h($contact['Contact']['firstName2']); ?>
                                &nbsp;
                                <?php echo h($contact['Contact']['lastName2']); ?>
                                &nbsp;
                            </dd>
                            <dt><?php echo __('Email'); ?></dt>
                            <dd>
                                <?php echo h($contact['Contact']['EmailAddress2']); ?>
                                &nbsp;
                            </dd>
                            <dt><?php echo __('Phones'); ?></dt>
                            <dd>
                                <?php echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone2'));?>
                            </dd>
                            <?php echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone3'));?>
                            <?php echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone4'));?>
                            <?php echo $this->element('phone_display_elem', array('contact' => $contact['Contact'], 'phone' =>'Phone5'));?>
                        </dl>
                    </div>
                </div>

            </div>
            <div class="tab-pane" id="Addresses">
                <?php echo $this->element('address_display_elem', array('contact' => $contact['Contact'], 'add_num' =>'1'));?>

            </div>
        </div>
    </div>
    <div class="tab-pane" id="Misc">
        <dl class="dl-horizontal" style="">

<!--            <dt>--><?php //echo __('JobTitle'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['JobTitle']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('Company'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['Company']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('ContactNotes'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['ContactNotes']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('ContactType'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['ContactType']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('Nickname'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['Nickname']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('SpouseName'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['SpouseName']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('Username'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['Username']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('Validated'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['Validated']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('Website'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['Website']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('Anniversary'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['Anniversary']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('AssistantName'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['AssistantName']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('AssistantPhone'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['AssistantPhone']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('BillingInformation'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['BillingInformation']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
<!--            <dt>--><?php //echo __('Birthday'); ?><!--</dt>-->
<!--            <dd>-->
<!--                --><?php //echo h($contact['Contact']['Birthday']); ?>
<!--                &nbsp;-->
<!--            </dd>-->
            <h4>Custom data from Infusion Soft</h4>

            <?php foreach ($contact['customfield'] as $custom_field): ?>
                <dt><?php echo $custom_field['customfield']['Label']; ?></dt>
                <dd><?php echo $custom_field['value']; ?></dd>
            <?php endforeach; ?>


            <!--		<dt>--><?php //echo __('Groups'); ?><!--</dt>-->
            <!--		<dd>-->
            <!--			--><?php //echo h($contact['Contact']['Groups']); ?>
            <!--			&nbsp;-->
            <!--		</dd>-->
            <dt><?php echo __('Leadsource'); ?></dt>
            <dd>
                <?php echo h($contact['Contact']['Leadsource']); ?>
                &nbsp;
            </dd>
        </dl>
    </div>
</div>

</div>
