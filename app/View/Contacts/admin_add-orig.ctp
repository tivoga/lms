<div class="contacts form">
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#anniversary" ).datepicker({ dateFormat: "yy-mm-dd" });
        });
        $(function() {
            $( "#birthday" ).datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>
    <?php echo $this->Form->create('Contact',
    array('class' => 'form-horizontal bsform',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'label' => array('class' => 'control-label col-sm-3'),
            'input' =>array('class' =>'form-control col-sm-9'),
            'between' => '<div class="col-sm-9">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Contact'); ?></legend>
	<?php
		echo $this->Form->input('JobTitle', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('LastName', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('FirstName', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('MiddleName', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Nickname', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('OwnerID', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Groups', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
//		echo $this->Form->input('LastUpdated', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
//		echo $this->Form->input('LastUpdatedBy', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Leadsource', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('LeadSourceId', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone1', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone1Ext', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone1Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone2Ext', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone2Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone3Ext', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone3Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone4', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone4Ext', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone4Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone5', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone5Ext', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Phone5Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('PostalCode', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('PostalCode2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('PostalCode3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('ReferralCode', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('SpouseName', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('State', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('State2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('State3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('StreetAddress1', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('StreetAddress2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Suffix', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Title', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Username', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Validated', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Website', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Address1Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Address2Street1', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Address2Street2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Address2Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Address3Street1', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Address3Street2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Address3Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Anniversary', array('type'=>'text','id'=>'anniversary','class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('AssistantName', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('AssistantPhone', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('BillingInformation', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Birthday', array('type'=>'text','id'=>'birthday','class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('City', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('City2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('City3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Company', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('AccountId', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('CompanyID', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('ContactNotes', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('ContactType', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Country', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Country2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Country3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
//		echo $this->Form->input('CreatedBy', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
//		echo $this->Form->input('DateCreated', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Email', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('EmailAddress2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('EmailAddress3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Fax1', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Fax1Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Fax2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('Fax2Type', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('ZipFour1', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('ZipFour2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
		echo $this->Form->input('ZipFour3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
        echo $this->Form->input('user_id', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
        echo $this->Form->input('firstName2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
        echo $this->Form->input('lastName2', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
        echo $this->Form->input('firstName3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
        echo $this->Form->input('lastName3', array('class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
        echo '<div class ="col-sm-offset-3 col-sm-9">';
        echo '<div class ="checkbox">';
        echo $this->Form->input('archived', array('class' =>'form-control col-sm-9'));
        echo '</div>';
        echo '</div>';

    ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?></li>
	</ul>
</div>
