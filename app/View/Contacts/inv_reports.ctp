<?php //print("<pre>".print_r($contact,true)."</pre>");
    $this->assign('linkId', 'nav_inv_reports');
?>
<div class="wid90">
    <h3>Invoice Report:</h3>
<script>
    $(document).ready(function() {
        $('#example').dataTable({
            "order": [[ 4, "asc" ]]
        });
    } );
</script>
<p> Click on one to send an Invoice.</p>
<table id="example" class="table hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Company</th>
        <th>Renewal Date</th>
        <th>Product / Service</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    <?php foreach($contacts as $contact):?>
        <tr>
            <td><?php echo $this->Html->link(h($contact['Contact']['FirstName'].' '.$contact['Contact']['LastName']), array('controller'=>'Emails','action' => 'add_invoice',
                    'contact_id' => $contact['Contact']['Id'],'contract_id'=>$contact['ContactsProduct']['id'] )); ?>
            </td>
            <!--        <td>--><?php //echo $this->Html->link(h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id']));?><!--&nbsp;</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['LastName']); ?><!--&nbsp;</td>-->
            <td><?php echo h($contact['Contact']['Phone1']); ?>&nbsp;</td>
            <td><?php echo h($contact['Contact']['Company']); ?>&nbsp;</td>
            <td><?php echo h($contact['ContactsProduct']['contract_end']); ?>&nbsp;</td>
            <td><?php echo h($contact['Product']['name']); ?>&nbsp;</td>
            <td>
                    <?php echo $this->Html->link('
                    <button title="Update new contract and renewal date" type="button" class="btn btn-success ">
                    Update
                    </button>', array('controller'=>'Products','action' => 'edit_product_values',
                        $contact['ContactsProduct']['product_id'],'contact_id' => $contact['Contact']['Id']), array('escape' => false)); ?>
                    <?php echo $this->Html->link('
                    <button title="Move contact to Cancelled" type="button" class="btn btn-danger ">
                    Cancel
                    </button>', array('controller'=>'Contacts','action' => 'move_contract_to_cancel',
                    'contract_id'=>$contact['ContactsProduct']['id']), array('escape' => false)); ?>


            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
</div>