<?php //print("<pre>".print_r($contact,true)."</pre>");
    $this->assign('linkId', 'nav_financials');
//    debug($contacts);
?>
<script>
    $(document).ready(function() {
        $('#example').dataTable({
            "order": [[ 4, "asc" ]]
        });
    } );
</script>
<div class="wid90">
<h3>Financial Reports:</h3>
    <?php echo $this->Form->create('reports',
        array('class'=>'inline_form','type'=>'get')); ?>
<!--    <fieldset>-->
        <!--    <div class="container-fluid">-->
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">Product or Service:</span>
                    <?php echo $this->Form->input('Product',array('class'=>"form-control",'label'=>false,'empty'=>'Select a Product or Service')); ?>
                </div>
            </div>
<!--            <div class="col-md-3">-->
<!--                <div class="input-group">-->
<!--                    <span class="input-group-addon">Service:</span>-->
<!--                    --><?php //echo $this->Form->input('Service',array('class'=>"form-control",'label'=>false,'empty'=>'Select a Service')); ?>
<!--                </div>-->
<!--            </div>-->
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">Contract Length:</span>
                    <?php echo $this->Form->input('clength',array('class'=>"form-control",'label'=>false,'empty'=>'Select a Period')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">Contract Start :</span>
                    <?php echo $this->Form->date('cont_startDate',array('class'=>"form-control",'label'=>false)); ?>

                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">Until:</span>
                    <?php echo $this->Form->date('cont_endDate',array('class'=>"form-control",'label'=>false)); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">Renewal Start :</span>
                    <?php echo $this->Form->date('ren_startDate',array('class'=>"form-control",'label'=>false)); ?>

                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">Until:</span>
                    <?php echo $this->Form->date('ren_endDate',array('class'=>"form-control",'label'=>false)); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">Costs From :</span>
                    <?php echo $this->Form->text('costs_start',array('class'=>"form-control",'label'=>false,'type'=>'number')); ?>

                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">To:</span>
                    <?php echo $this->Form->text('costs_end',array('class'=>"form-control",'label'=>false,'type'=>'number')); ?>
                </div>
            </div>
        </div>
    <div class="row col-md-3">
        <?php echo $this->Form->submit('GO',array('div'=>false)); ?>
    </div>
<!--        <div class="panel panel-default">-->
<!--            <div class="panel-heading">-->
<!--                <h3 class="panel-title">Contract Starting Between</h3>-->
<!--            </div>-->
<!--            <div class="panel-body">-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="panel panel-default">-->
<!--            <div class="panel-heading">-->
<!--                <h3 class="panel-title">Renewal Date Between</h3>-->
<!--            </div>-->
<!--            <div class="panel-body">-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="panel panel-default">-->
<!--            <div class="panel-heading">-->
<!--                <h3 class="panel-title">Costs/ Values Between</h3>-->
<!--            </div>-->
<!--            <div class="panel-body">-->
<!---->
<!--            </div>-->
<!--        </div>-->
    <?php echo $this->Form->end(); ?>
        <div class="row">

<!--            <div class="col-md-3">-->
<!--                <div class="input-group">-->
<!--                    <span class="input-group-addon">Contract :</span>-->
<!--                    --><?php //echo $this->Form->date('startDate',array('class'=>"form-control",'label'=>false)); ?>
<!---->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-3">-->
<!--                <div class="input-group">-->
<!--                    <span class="input-group-addon">Until:</span>-->
<!--                    --><?php //echo $this->Form->date('endDate',array('class'=>"form-control",'label'=>false)); ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-5">-->
<!--                <div class="input-group">-->
<!--                    <span class="input-group-addon">Product Type:</span>-->
<!--                    --><?php //echo $this->Form->input('ptype',array('class'=>"form-control",'label'=>false,'empty'=>'Select an Product Type')); ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-3">-->
<!--                <div class="input-group">-->
<!--                    <span class="input-group-addon">Costs :</span>-->
<!--                    --><?php //echo $this->Form->text('startDate',array('class'=>"form-control",'label'=>false,'type'=>'number')); ?>
<!---->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-3">-->
<!--                <div class="input-group">-->
<!--                    <span class="input-group-addon">Until:</span>-->
<!--                    --><?php //echo $this->Form->text('endDate',array('class'=>"form-control",'label'=>false,'type'=>'number')); ?>
<!--                </div>-->
<!--            </div>-->
        </div>
<!--        <br>-->
        <!--    </div>-->
<!--    </fieldset>-->



<hr style="width: 100%;border-top-width: 1px;border-color: pink;">

<table id="example" class="table hover table-striped table-bordered display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Name</th>
        <th>Contract Started at</th>
        <th>Quoted</th>
        <th>Paid</th>
        <th>Next Renewal at</th>
        <th>Contract Length (Years)</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($contacts as $contact):?>
        <?php foreach($contact['ContactsProduct'] as $contract):?>
        <?php if(!$contract['renew_letter_sent']){
                continue;
            }?>

        <tr>
            <td><?php echo $this->Html->link(h($contact['Contact']['FirstName'].' '.$contact['Contact']['LastName']), array('controller'=>'Emails','action' => 'add_renew',
                    'contact_id' => $contact['Contact']['Id'],'contract_id'=>$contract['id'] )); ?>
            </td>
            <!--        <td>--><?php //echo $this->Html->link(h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id']));?><!--&nbsp;</td>-->
<!--            <td>--><?php //echo h(); ?><!--&nbsp;</td>-->
            <td><?php echo h($contract['contract_start']); ?>&nbsp;</td>
<!--            <td>--><?php //echo h($contact['ContactsProduct']['contract_status']); ?><!--&nbsp;</td>-->
            <td><?php echo h($contract['contract_quoted']); ?>&nbsp;</td>
            <td><?php echo h($contract['contract_paid']); ?>&nbsp;</td>
            <td><?php echo h($contract['contract_end']); ?>&nbsp;</td>
            <td><?php echo h($contract['contract_period']); ?>&nbsp;</td>
        </tr>
        <?php endforeach ?>
    <?php endforeach ?>
    </tbody>
</table>
    <div class="clear-fix">
        <?php
        echo $this->Html->link(h('Export data as C.S.V'),
            array('action' => 'export_financial_report', '?'=>$this->request->query),array('class'=>'btn btn-success'));?>
        <!--<button type="submit" class="btn btn-success" id="submit">Export data as C.S.V </button>'-->
    </div>
<!--<button type="submit" class="btn btn-success" id="submit">Export data as C.S.V </button>-->
</div>