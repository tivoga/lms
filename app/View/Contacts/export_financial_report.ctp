<?php
echo 'First Name, Last Name, Started at, Quoted, Paid, Next Renewal at, Contract Length'."\n";

foreach($contacts as $contact){
    foreach($contact['ContactsProduct'] as $contract){
        if(!$contract['renew_letter_sent']){
            continue;
        }
        echo h($contact['Contact']['FirstName']).', ';
        echo h($contact['Contact']['LastName']).', ';
        echo h($contract['contract_start']).', ';
        echo h($contract['contract_quoted']).', ';
        echo h($contract['contract_paid']).', ';
        echo h($contract['contract_end']).', ';
        echo h($contract['contract_period']);
        echo "\n";
    }
}


