<?php //print("<pre>".print_r($contact,true)."</pre>");
    $this->assign('linkId', 'nav_cancel_reports');
?>
<div class="wid90">
    <h3>All Cancelled Contacts:</h3>
<script>
    $(document).ready(function() {
        $('#example').dataTable({
            "order": [[ 4, "asc" ]]
        });
    } );
</script>
<?php echo $this->Form->create('reports',
    array('class'=>'inline_form')); ?>
<!--<fieldset>-->
    <!--    <div class="container-fluid">-->
    <div class="row">
        <div class="col-md-4">
            <div class="input-group">
                <span class="input-group-addon">Date From:</span>
                <?php echo $this->Form->date('startDate',array('class'=>"form-control",'label'=>false)); ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                <span class="input-group-addon">Date To:</span>
                <?php echo $this->Form->date('endDate',array('class'=>"form-control",'label'=>false)); ?>
            </div>
        </div>
        <div class="col-md-1">
            <?php echo $this->Form->end(array('div'=>false,'label'=>'GO','value'=>'Go')); ?>
        </div>
    </div>
    <!--    </div>-->
<!--</fieldset>-->


<!---->
<!--<form>-->
<!--    <div id="form_div" style="width: 620px; float: left; clear: none;">-->
<!--        <div class="input-group">-->
<!--            <span class="input-group-addon">Starting:</span>-->
<!--            <input type="date" class="form-control" name="startDate">-->
<!--            <span class="input-group-addon">Untill:</span>-->
<!--            <input type="date" class="form-control" name="endDate">-->
<!--        </div>-->
<!--    </div>-->
<!--    <div id="form_submit" style="clear: none;float: left; margin-top: 5px;">-->
<!--        <button type="submit" value=" Send" class="btn btn-success" id="submit"> Go</button>-->
<!--    </div>-->
<!--</form>-->
<table id="example" class="table hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Name</th>
<!--        <th>Last Name</th>-->
        <th>Phone</th>
        <th>Company</th>
        <th>Renewal Date</th>
        <th>Product</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($contacts as $contact):?>
        <tr>
            <td><?php echo $this->Html->link(h($contact['Contact']['FirstName'].' '. $contact['Contact']['LastName']), array('controller'=>'Emails','action' => 'add_renew',
                    'contact_id' => $contact['Contact']['Id'],'contract_id'=>$contact['ContactsProduct']['id'] )); ?>
            </td>
            <!--        <td>--><?php //echo $this->Html->link(h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id']));?><!--&nbsp;</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['LastName']); ?><!--&nbsp;</td>-->
            <td><?php echo h($contact['Contact']['Phone1']); ?>&nbsp;</td>
            <td><?php echo h($contact['Contact']['Company']); ?>&nbsp;</td>
            <td><?php echo h($contact['ContactsProduct']['contract_end']); ?>&nbsp;</td>
            <td><?php echo h($contact['Product']['name']); ?>&nbsp;</td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
</div>
