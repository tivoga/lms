<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<p></p>
<div class="contacts wid90">
    <?php echo $this->Form->create('Contact'); ?>

        <h2><?php echo __('Edit Contact'); ?></h2>
        <?php
            echo($this->element('contact_field_display_elem',array('edit'=>true)));

//        echo $this->Form->input('MiddleName');
//        echo $this->Form->input('Nickname');
//        echo $this->Form->input('OwnerID');
//        echo $this->Form->input('Groups');
        //        echo $this->Form->input('LastUpdated');
        //        echo $this->Form->input('LastUpdatedBy');
//        echo $this->Form->input('Leadsource');
        //        echo $this->Form->input('LeadSourceId');

//        echo $this->Form->input('Phone1Ext');
//        echo $this->Form->input('Phone1Type');

//        echo $this->Form->input('Phone2Ext');
//        echo $this->Form->input('Phone2Type');

//        echo $this->Form->input('Phone3Ext');
//        echo $this->Form->input('Phone3Type');
//        echo $this->Form->input('Phone4');
//        echo $this->Form->input('Phone4Ext');
//        echo $this->Form->input('Phone4Type');
//        echo $this->Form->input('Phone5');
//        echo $this->Form->input('Phone5Ext');
//        echo $this->Form->input('Phone5Type');
//        echo $this->Form->input('SpouseName');
//        echo $this->Form->input('ReferralCode');
//        echo $this->Form->input('Suffix');

//        echo $this->Form->input('Title');
//        echo $this->Form->input('Username');
//        echo $this->Form->input('Validated');
//        echo $this->Form->input('Website');

//        echo '<hr style="width: 100%; border-top: 1px solid #F00;">';

        //        echo $this->Form->input('CreatedBy');

//        echo '<hr style="width: 100%; border-top: 1px solid #F00;">';
//        echo $this->Form->input('Anniversary', array('type'=>'text', 'id'=>'anniversary','class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));
//        echo $this->Form->input('AssistantName');
//        echo $this->Form->input('AssistantPhone');
//        echo $this->Form->input('BillingInformation');
//        echo $this->Form->input('Birthday', array('type'=>'text', 'id'=>'birthday', 'class' =>'form-control col-sm-9','label' => array('class' => 'control-label col-sm-3')));

//        echo $this->Form->input('AccountId');
//        echo $this->Form->input('CompanyID');
//        echo $this->Form->input('ContactNotes');
//        echo $this->Form->input('ContactType');
        //        echo $this->Form->input('DateCreated');

        ?>

    <div class="clearfix">
        <div class="pull-left">
            <?php echo $this->Form->submit('Save Changes',
                array('class' => 'btn btn-success', 'title' => 'Save changes')
            ); ?>
        </div>
        <div class="pull-left" style="margin-left: 10px;">
            <div class="submit">
                <?php //debug($this->data['Contact']['Id']);
                echo $this->Html->link(__('Cancel Changes'), array('controller' =>'Contacts','action' => 'view',
                        $this->data['Contact']['Id'])
                    , array('role' =>'button', 'class'=>'btn btn-danger')); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
<!--    --><?php //echo $this->Form->end(__('Save Changes')); ?>
</div>
<!--<div class="actions">-->
<!--    <h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--    <ul>-->

        <!--		<li>--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Contact.Id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Contact.Id'))); ?><!--</li>-->
        <!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?><!--</li>-->
<!--    </ul>-->
<!--</div>-->
