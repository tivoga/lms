<?php
echo 'First Name, Last Name, Company, Industry, Products Interested In'."\n";

foreach ($contacts as $row):
    echo $row['Contact']['FirstName'].', ';
    echo $row['Contact']['LastName'].', ';
    echo $row['Contact']['Company'].', ';
    foreach($row['customfield'] as $cfv ){
        if($cfv['custom_field_id']==54){
            echo h($cfv['value']);
            break;
        }
    }
    echo ', ';
    foreach($row['Product'] as $c_p){
        echo h($c_p['name']).' | ';
    }
    echo "\n";

endforeach;
