<div class="contacts form">

	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1><?php echo __('Admin Edit Contact'); ?></h1>
			</div>
		</div>
	</div>



	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">

																<li><?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Delete'), array('action' => 'delete', $this->Form->value('Contact.Id')), array('escape' => false), __('Are you sure you want to delete # %s?', $this->Form->value('Contact.Id'))); ?></li>
																<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Contacts'), array('action' => 'index'), array('escape' => false)); ?></li>
									<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Users'), array('controller' => 'users', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New User'), array('controller' => 'users', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Communications'), array('controller' => 'communications', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Communication'), array('controller' => 'communications', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Emails'), array('controller' => 'emails', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Email'), array('controller' => 'emails', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Product Fields Values'), array('controller' => 'product_fields_values', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Product Fields Value'), array('controller' => 'product_fields_values', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Uploaded Files'), array('controller' => 'uploaded_files', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Uploaded File'), array('controller' => 'uploaded_files', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Custom Field Values'), array('controller' => 'custom_field_values', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Customfield'), array('controller' => 'custom_field_values', 'action' => 'add'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Products'), array('controller' => 'products', 'action' => 'index'), array('escape' => false)); ?> </li>
		<li><?php echo $this->Html->link(__('<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Product'), array('controller' => 'products', 'action' => 'add'), array('escape' => false)); ?> </li>
							</ul>
						</div>
					</div>
				</div>			
		</div><!-- end col md 3 -->
		<div class="col-md-9">
			<?php echo $this->Form->create('Contact', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('Id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('JobTitle', array('class' => 'form-control', 'placeholder' => 'JobTitle'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('LastName', array('class' => 'form-control', 'placeholder' => 'LastName'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('FirstName', array('class' => 'form-control', 'placeholder' => 'FirstName'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('MiddleName', array('class' => 'form-control', 'placeholder' => 'MiddleName'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Nickname', array('class' => 'form-control', 'placeholder' => 'Nickname'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('OwnerID', array('class' => 'form-control', 'placeholder' => 'OwnerID'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Groups', array('class' => 'form-control', 'placeholder' => 'Groups'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('LastUpdated', array('class' => 'form-control', 'placeholder' => 'LastUpdated'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('LastUpdatedBy', array('class' => 'form-control', 'placeholder' => 'LastUpdatedBy'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Leadsource', array('class' => 'form-control', 'placeholder' => 'Leadsource'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('LeadSourceId', array('class' => 'form-control', 'placeholder' => 'LeadSourceId'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone1', array('class' => 'form-control', 'placeholder' => 'Phone1'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone1Ext', array('class' => 'form-control', 'placeholder' => 'Phone1Ext'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone1Type', array('class' => 'form-control', 'placeholder' => 'Phone1Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone2', array('class' => 'form-control', 'placeholder' => 'Phone2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone2Ext', array('class' => 'form-control', 'placeholder' => 'Phone2Ext'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone2Type', array('class' => 'form-control', 'placeholder' => 'Phone2Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone3', array('class' => 'form-control', 'placeholder' => 'Phone3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone3Ext', array('class' => 'form-control', 'placeholder' => 'Phone3Ext'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone3Type', array('class' => 'form-control', 'placeholder' => 'Phone3Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone4', array('class' => 'form-control', 'placeholder' => 'Phone4'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone4Ext', array('class' => 'form-control', 'placeholder' => 'Phone4Ext'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone4Type', array('class' => 'form-control', 'placeholder' => 'Phone4Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone5', array('class' => 'form-control', 'placeholder' => 'Phone5'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone5Ext', array('class' => 'form-control', 'placeholder' => 'Phone5Ext'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Phone5Type', array('class' => 'form-control', 'placeholder' => 'Phone5Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('PostalCode', array('class' => 'form-control', 'placeholder' => 'PostalCode'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('PostalCode2', array('class' => 'form-control', 'placeholder' => 'PostalCode2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('PostalCode3', array('class' => 'form-control', 'placeholder' => 'PostalCode3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ReferralCode', array('class' => 'form-control', 'placeholder' => 'ReferralCode'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('SpouseName', array('class' => 'form-control', 'placeholder' => 'SpouseName'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('State', array('class' => 'form-control', 'placeholder' => 'State'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('State2', array('class' => 'form-control', 'placeholder' => 'State2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('State3', array('class' => 'form-control', 'placeholder' => 'State3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('StreetAddress1', array('class' => 'form-control', 'placeholder' => 'StreetAddress1'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('StreetAddress2', array('class' => 'form-control', 'placeholder' => 'StreetAddress2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Suffix', array('class' => 'form-control', 'placeholder' => 'Suffix'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Title', array('class' => 'form-control', 'placeholder' => 'Title'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Username', array('class' => 'form-control', 'placeholder' => 'Username'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Validated', array('class' => 'form-control', 'placeholder' => 'Validated'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Website', array('class' => 'form-control', 'placeholder' => 'Website'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Address1Type', array('class' => 'form-control', 'placeholder' => 'Address1Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Address2Street1', array('class' => 'form-control', 'placeholder' => 'Address2Street1'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Address2Street2', array('class' => 'form-control', 'placeholder' => 'Address2Street2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Address2Type', array('class' => 'form-control', 'placeholder' => 'Address2Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Address3Street1', array('class' => 'form-control', 'placeholder' => 'Address3Street1'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Address3Street2', array('class' => 'form-control', 'placeholder' => 'Address3Street2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Address3Type', array('class' => 'form-control', 'placeholder' => 'Address3Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Anniversary', array('class' => 'form-control', 'placeholder' => 'Anniversary'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('AssistantName', array('class' => 'form-control', 'placeholder' => 'AssistantName'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('AssistantPhone', array('class' => 'form-control', 'placeholder' => 'AssistantPhone'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('BillingInformation', array('class' => 'form-control', 'placeholder' => 'BillingInformation'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Birthday', array('class' => 'form-control', 'placeholder' => 'Birthday'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('City', array('class' => 'form-control', 'placeholder' => 'City'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('City2', array('class' => 'form-control', 'placeholder' => 'City2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('City3', array('class' => 'form-control', 'placeholder' => 'City3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Company', array('class' => 'form-control', 'placeholder' => 'Company'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('AccountId', array('class' => 'form-control', 'placeholder' => 'AccountId'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('CompanyID', array('class' => 'form-control', 'placeholder' => 'CompanyID'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ContactNotes', array('class' => 'form-control', 'placeholder' => 'ContactNotes'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ContactType', array('class' => 'form-control', 'placeholder' => 'ContactType'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Country', array('class' => 'form-control', 'placeholder' => 'Country'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Country2', array('class' => 'form-control', 'placeholder' => 'Country2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Country3', array('class' => 'form-control', 'placeholder' => 'Country3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('CreatedBy', array('class' => 'form-control', 'placeholder' => 'CreatedBy'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('DateCreated', array('class' => 'form-control', 'placeholder' => 'DateCreated'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Email', array('class' => 'form-control', 'placeholder' => 'Email'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('EmailAddress2', array('class' => 'form-control', 'placeholder' => 'EmailAddress2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('EmailAddress3', array('class' => 'form-control', 'placeholder' => 'EmailAddress3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Fax1', array('class' => 'form-control', 'placeholder' => 'Fax1'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Fax1Type', array('class' => 'form-control', 'placeholder' => 'Fax1Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Fax2', array('class' => 'form-control', 'placeholder' => 'Fax2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Fax2Type', array('class' => 'form-control', 'placeholder' => 'Fax2Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ZipFour1', array('class' => 'form-control', 'placeholder' => 'ZipFour1'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ZipFour2', array('class' => 'form-control', 'placeholder' => 'ZipFour2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ZipFour3', array('class' => 'form-control', 'placeholder' => 'ZipFour3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('archived', array('class' => 'form-control', 'placeholder' => 'Archived'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('user_id', array('class' => 'form-control', 'placeholder' => 'User Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('firstName2', array('class' => 'form-control', 'placeholder' => 'FirstName2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('lastName2', array('class' => 'form-control', 'placeholder' => 'LastName2'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('firstName3', array('class' => 'form-control', 'placeholder' => 'FirstName3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('lastName3', array('class' => 'form-control', 'placeholder' => 'LastName3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('Product', array('class' => 'form-control', 'placeholder' => 'LastName3'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

		</div><!-- end col md 12 -->
	</div><!-- end row -->
</div>
