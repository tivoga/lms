<?php //print("<pre>".print_r($contact,true)."</pre>");
    $this->assign('linkId', 'nav_renew_reports');
//    debug($contacts);
?>
<div class="wid90">
    <h3>Renewal Report:</h3>

<script>
    $(document).ready(function() {
        $('#example').dataTable({
            "order": [[ 4, "asc" ]]
        });
    } );
</script>
<p> Click on one to send Renewal Letter.</p>

<table id="example" class="table hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Name</th>
<!--        <th>Last Name</th>-->
        <th>Phone</th>
        <th>Company</th>
        <th>Renewal Date</th>
        <th>Product</th>
    </tr>
    </thead>


    <tbody>
    <?php foreach($contacts as $contact):?>
    <tr>
        <td><?php echo $this->Html->link(h($contact['Contact']['FirstName'].' '.$contact['Contact']['LastName']), array('controller'=>'Emails','action' => 'add_renew',
            'contact_id' => $contact['Contact']['Id'],'contract_id'=>$contact['ContactsProduct']['id'] )); ?>
        </td>
<!--        <td>--><?php //echo $this->Html->link(h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id']));?><!--&nbsp;</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['LastName']); ?><!--&nbsp;</td>-->
            <td><?php echo h($contact['Contact']['Phone1']); ?>&nbsp;</td>
        <td><?php echo h($contact['Contact']['Company']); ?>&nbsp;</td>
        <td><?php echo h($contact['ContactsProduct']['contract_end']); ?>&nbsp;</td>
        <td><?php echo h($contact['Product']['name']); ?>&nbsp;</td>
    </tr>
    <?php endforeach ?>
    </tbody>
</table>
<!--<p> To get a client of the list, You can send a templated Renewal Letter to-->
<!--    the client in the Messages section of Contact details</p>-->
</div>