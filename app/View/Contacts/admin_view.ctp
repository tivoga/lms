<div class="contacts view">
<h2><?php echo __('Contact'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('JobTitle'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['JobTitle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LastName'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['LastName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('FirstName'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['FirstName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MiddleName'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['MiddleName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nickname'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Nickname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('OwnerID'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['OwnerID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Groups'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Groups']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LastUpdated'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['LastUpdated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LastUpdatedBy'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['LastUpdatedBy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Leadsource'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Leadsource']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LeadSourceId'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['LeadSourceId']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone1'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone1Ext'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone1Ext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone1Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone1Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone2Ext'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone2Ext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone2Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone2Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone3'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone3Ext'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone3Ext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone3Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone3Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone4'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone4']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone4Ext'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone4Ext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone4Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone4Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone5'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone5']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone5Ext'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone5Ext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone5Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Phone5Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostalCode'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['PostalCode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostalCode2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['PostalCode2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PostalCode3'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['PostalCode3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ReferralCode'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['ReferralCode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SpouseName'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['SpouseName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['State']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['State2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State3'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['State3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('StreetAddress1'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['StreetAddress1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('StreetAddress2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['StreetAddress2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Suffix'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Suffix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Validated'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Validated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Website'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Website']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address1Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Address1Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address2Street1'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Address2Street1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address2Street2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Address2Street2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address2Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Address2Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address3Street1'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Address3Street1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address3Street2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Address3Street2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address3Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Address3Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Anniversary'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Anniversary']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AssistantName'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['AssistantName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AssistantPhone'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['AssistantPhone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BillingInformation'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['BillingInformation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Birthday'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Birthday']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['City']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['City2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City3'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['City3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AccountId'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['AccountId']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CompanyID'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['CompanyID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ContactNotes'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['ContactNotes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ContactType'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['ContactType']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Country']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Country2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country3'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Country3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CreatedBy'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['CreatedBy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DateCreated'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['DateCreated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('EmailAddress2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['EmailAddress2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('EmailAddress3'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['EmailAddress3']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax1'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Fax1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax1Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Fax1Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Fax2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax2Type'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['Fax2Type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ZipFour1'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['ZipFour1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ZipFour2'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['ZipFour2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ZipFour3'); ?></dt>
		<dd>
			<?php echo h($contact['Contact']['ZipFour3']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contact'), array('action' => 'edit', $contact['Contact']['Id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contact'), array('action' => 'delete', $contact['Contact']['Id']), array(), __('Are you sure you want to delete # %s?', $contact['Contact']['Id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('action' => 'add')); ?> </li>
	</ul>
</div>
