<?php //print("<pre>".print_r($contact,true)."</pre>");
    $this->assign('linkId', 'nav_reports');
//debug($contacts);
//debug($firstcontact);
//debug($this->request->query);
?>
<script>
    $(document).ready(function() {
        $('#example').dataTable({
            "order": []
        });
    } );
</script>
<div class="wid90">
<h3>Lead Report:</h3>
    <?php echo $this->Form->create('reports',
        array('class'=>'inline_form','type'=>'get')); ?>
<!--    <fieldset>-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Date From:</span>
                    <?php echo $this->Form->date('startDate',array('class'=>"form-control",'label'=>false)); ?>

                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Date To:</span>
                    <?php echo $this->Form->date('endDate',array('class'=>"form-control",'label'=>false)); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Industry:</span>
                    <?php echo $this->Form->input('industry',array('class'=>"form-control",'label'=>false,'empty'=>'Select an Industry')); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Product Interested In:</span>
                    <?php echo $this->Form->input('Product',array('class'=>"form-control",'label'=>false,'empty'=>'Select a Product')); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">Lead Source:</span>
                    <?php echo $this->Form->input('lead_source',array('class'=>"form-control",'type' => 'textfield','label'=>false)); ?>
                </div>
            </div>
            <div class="col-md-offset-3 col-md-1">
                <?php echo $this->Form->end(array('div'=>false,'label'=>'GO','value'=>'Go')); ?>
            </div>
        </div>
    </div>
<!--    </fieldset>-->

<hr style="width: 100%;border-top-width: 1px;border-color: pink;">

<table id="example" class="display table hover table-striped table-bordered" cellspacing="0" width="100%">
<thead>
<tr>
    <th>Name</th>
    <th>Company</th>
    <th>Industry</th>
    <th>Products Interested In </th>
</tr>
</thead>

<tbody>
<?php foreach ($contacts as $contact): ?>
    <tr>
        <td><?php echo $this->Html->link(h($contact['Contact']['FirstName'].' '.$contact['Contact']['LastName']), array('action' => 'view', $contact['Contact']['Id']));?>&nbsp;</td>
        <td><?php echo h($contact['Contact']['Company']); ?>&nbsp;</td>
        <td><?php
            foreach($contact['customfield'] as $cfv ){
                if($cfv['custom_field_id']==54){
                    echo h($cfv['value']);
                    break;
                }
            }
             ?>&nbsp;</td>
        <td><?php
            foreach($contact['Product'] as $c_p){
                echo h($c_p['name']).', ';
            }
            ?>&nbsp;</td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>
    <p>
<!--        --><?php
//        echo $this->Paginator->counter(array(
//            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
//        ));
//        ?><!--	</p>-->
<!--    <div class="pagination">-->
<!--        --><?php
//        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
//        echo $this->Paginator->numbers(array('separator' => ''));
//        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
//        ?>
<!--    </div>-->
<div class="clear-fix">
    <?php echo $this->Html->link(h('Export data as C.S.V'),
        array('action' => 'export_lead_report', '?'=>$this->request->query),array('class'=>'btn btn-success'));?>
<!--<button type="submit" class="btn btn-success" id="submit">Export data as C.S.V </button>'-->
</div>
</div>