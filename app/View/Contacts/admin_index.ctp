<div class="contacts index">
	<h2><?php echo __('Contacts'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('Id'); ?></th>
			<th><?php echo $this->Paginator->sort('JobTitle'); ?></th>
			<th><?php echo $this->Paginator->sort('LastName'); ?></th>
			<th><?php echo $this->Paginator->sort('FirstName'); ?></th>
			<th><?php echo $this->Paginator->sort('MiddleName'); ?></th>
			<th><?php echo $this->Paginator->sort('Nickname'); ?></th>
			<th><?php echo $this->Paginator->sort('OwnerID'); ?></th>
			<th><?php echo $this->Paginator->sort('Groups'); ?></th>
			<th><?php echo $this->Paginator->sort('LastUpdated'); ?></th>
			<th><?php echo $this->Paginator->sort('LastUpdatedBy'); ?></th>
			<th><?php echo $this->Paginator->sort('Leadsource'); ?></th>
			<th><?php echo $this->Paginator->sort('LeadSourceId'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone1'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone1Ext'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone1Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone2'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone2Ext'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone2Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone3'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone3Ext'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone3Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone4'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone4Ext'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone4Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone5'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone5Ext'); ?></th>
			<th><?php echo $this->Paginator->sort('Phone5Type'); ?></th>
			<th><?php echo $this->Paginator->sort('PostalCode'); ?></th>
			<th><?php echo $this->Paginator->sort('PostalCode2'); ?></th>
			<th><?php echo $this->Paginator->sort('PostalCode3'); ?></th>
			<th><?php echo $this->Paginator->sort('ReferralCode'); ?></th>
			<th><?php echo $this->Paginator->sort('SpouseName'); ?></th>
			<th><?php echo $this->Paginator->sort('State'); ?></th>
			<th><?php echo $this->Paginator->sort('State2'); ?></th>
			<th><?php echo $this->Paginator->sort('State3'); ?></th>
			<th><?php echo $this->Paginator->sort('StreetAddress1'); ?></th>
			<th><?php echo $this->Paginator->sort('StreetAddress2'); ?></th>
			<th><?php echo $this->Paginator->sort('Suffix'); ?></th>
			<th><?php echo $this->Paginator->sort('Title'); ?></th>
			<th><?php echo $this->Paginator->sort('Username'); ?></th>
			<th><?php echo $this->Paginator->sort('Validated'); ?></th>
			<th><?php echo $this->Paginator->sort('Website'); ?></th>
			<th><?php echo $this->Paginator->sort('Address1Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Address2Street1'); ?></th>
			<th><?php echo $this->Paginator->sort('Address2Street2'); ?></th>
			<th><?php echo $this->Paginator->sort('Address2Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Address3Street1'); ?></th>
			<th><?php echo $this->Paginator->sort('Address3Street2'); ?></th>
			<th><?php echo $this->Paginator->sort('Address3Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Anniversary'); ?></th>
			<th><?php echo $this->Paginator->sort('AssistantName'); ?></th>
			<th><?php echo $this->Paginator->sort('AssistantPhone'); ?></th>
			<th><?php echo $this->Paginator->sort('BillingInformation'); ?></th>
			<th><?php echo $this->Paginator->sort('Birthday'); ?></th>
			<th><?php echo $this->Paginator->sort('City'); ?></th>
			<th><?php echo $this->Paginator->sort('City2'); ?></th>
			<th><?php echo $this->Paginator->sort('City3'); ?></th>
			<th><?php echo $this->Paginator->sort('Company'); ?></th>
			<th><?php echo $this->Paginator->sort('AccountId'); ?></th>
			<th><?php echo $this->Paginator->sort('CompanyID'); ?></th>
			<th><?php echo $this->Paginator->sort('ContactNotes'); ?></th>
			<th><?php echo $this->Paginator->sort('ContactType'); ?></th>
			<th><?php echo $this->Paginator->sort('Country'); ?></th>
			<th><?php echo $this->Paginator->sort('Country2'); ?></th>
			<th><?php echo $this->Paginator->sort('Country3'); ?></th>
			<th><?php echo $this->Paginator->sort('CreatedBy'); ?></th>
			<th><?php echo $this->Paginator->sort('DateCreated'); ?></th>
			<th><?php echo $this->Paginator->sort('Email'); ?></th>
			<th><?php echo $this->Paginator->sort('EmailAddress2'); ?></th>
			<th><?php echo $this->Paginator->sort('EmailAddress3'); ?></th>
			<th><?php echo $this->Paginator->sort('Fax1'); ?></th>
			<th><?php echo $this->Paginator->sort('Fax1Type'); ?></th>
			<th><?php echo $this->Paginator->sort('Fax2'); ?></th>
			<th><?php echo $this->Paginator->sort('Fax2Type'); ?></th>
			<th><?php echo $this->Paginator->sort('ZipFour1'); ?></th>
			<th><?php echo $this->Paginator->sort('ZipFour2'); ?></th>
			<th><?php echo $this->Paginator->sort('ZipFour3'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($contacts as $contact): ?>
	<tr>
		<td><?php echo h($contact['Contact']['Id']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['JobTitle']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['LastName']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['FirstName']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['MiddleName']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Nickname']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['OwnerID']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Groups']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['LastUpdated']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['LastUpdatedBy']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Leadsource']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['LeadSourceId']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone1']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone1Ext']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone1Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone2Ext']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone2Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone3']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone3Ext']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone3Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone4']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone4Ext']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone4Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone5']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone5Ext']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Phone5Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['PostalCode']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['PostalCode2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['PostalCode3']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['ReferralCode']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['SpouseName']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['State']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['State2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['State3']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['StreetAddress1']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['StreetAddress2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Suffix']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Title']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Username']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Validated']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Website']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Address1Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Address2Street1']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Address2Street2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Address2Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Address3Street1']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Address3Street2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Address3Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Anniversary']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['AssistantName']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['AssistantPhone']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['BillingInformation']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Birthday']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['City']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['City2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['City3']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Company']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['AccountId']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['CompanyID']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['ContactNotes']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['ContactType']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Country']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Country2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Country3']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['CreatedBy']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['DateCreated']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Email']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['EmailAddress2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['EmailAddress3']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Fax1']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Fax1Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Fax2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['Fax2Type']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['ZipFour1']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['ZipFour2']); ?>&nbsp;</td>
		<td><?php echo h($contact['Contact']['ZipFour3']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $contact['Contact']['Id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $contact['Contact']['Id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contact['Contact']['Id']), array(), __('Are you sure you want to delete # %s?', $contact['Contact']['Id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Contact'), array('action' => 'add')); ?></li>
	</ul>
</div>
