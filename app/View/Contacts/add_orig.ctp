<div class="contacts form">
<?php echo $this->Form->create('Contact'); ?>
	<fieldset>
		<legend><?php echo __('Add Contact'); ?></legend>
	<?php
		echo $this->Form->input('JobTitle');
		echo $this->Form->input('LastName');
		echo $this->Form->input('FirstName');
		echo $this->Form->input('MiddleName');
		echo $this->Form->input('Nickname');
		echo $this->Form->input('OwnerID');
		echo $this->Form->input('Groups');
		echo $this->Form->input('LastUpdated');
		echo $this->Form->input('LastUpdatedBy');
		echo $this->Form->input('Leadsource');
		echo $this->Form->input('LeadSourceId');
		echo $this->Form->input('Phone1');
		echo $this->Form->input('Phone1Ext');
		echo $this->Form->input('Phone1Type');
		echo $this->Form->input('Phone2');
		echo $this->Form->input('Phone2Ext');
		echo $this->Form->input('Phone2Type');
		echo $this->Form->input('Phone3');
		echo $this->Form->input('Phone3Ext');
		echo $this->Form->input('Phone3Type');
		echo $this->Form->input('Phone4');
		echo $this->Form->input('Phone4Ext');
		echo $this->Form->input('Phone4Type');
		echo $this->Form->input('Phone5');
		echo $this->Form->input('Phone5Ext');
		echo $this->Form->input('Phone5Type');
		echo $this->Form->input('PostalCode');
		echo $this->Form->input('PostalCode2');
		echo $this->Form->input('PostalCode3');
		echo $this->Form->input('ReferralCode');
		echo $this->Form->input('SpouseName');
		echo $this->Form->input('State');
		echo $this->Form->input('State2');
		echo $this->Form->input('State3');
		echo $this->Form->input('StreetAddress1');
		echo $this->Form->input('StreetAddress2');
		echo $this->Form->input('Suffix');
		echo $this->Form->input('Title');
		echo $this->Form->input('Username');
		echo $this->Form->input('Validated');
		echo $this->Form->input('Website');
		echo $this->Form->input('Address1Type');
		echo $this->Form->input('Address2Street1');
		echo $this->Form->input('Address2Street2');
		echo $this->Form->input('Address2Type');
		echo $this->Form->input('Address3Street1');
		echo $this->Form->input('Address3Street2');
		echo $this->Form->input('Address3Type');
		echo $this->Form->input('Anniversary');
		echo $this->Form->input('AssistantName');
		echo $this->Form->input('AssistantPhone');
		echo $this->Form->input('BillingInformation');
		echo $this->Form->input('Birthday');
		echo $this->Form->input('City');
		echo $this->Form->input('City2');
		echo $this->Form->input('City3');
		echo $this->Form->input('Company');
		echo $this->Form->input('AccountId');
		echo $this->Form->input('CompanyID');
		echo $this->Form->input('ContactNotes');
		echo $this->Form->input('ContactType');
		echo $this->Form->input('Country');
		echo $this->Form->input('Country2');
		echo $this->Form->input('Country3');
		echo $this->Form->input('CreatedBy');
		echo $this->Form->input('DateCreated');
		echo $this->Form->input('Email');
		echo $this->Form->input('EmailAddress2');
		echo $this->Form->input('EmailAddress3');
		echo $this->Form->input('Fax1');
		echo $this->Form->input('Fax1Type');
		echo $this->Form->input('Fax2');
		echo $this->Form->input('Fax2Type');
		echo $this->Form->input('ZipFour1');
		echo $this->Form->input('ZipFour2');
		echo $this->Form->input('ZipFour3');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Contacts'), array('action' => 'index')); ?></li>
	</ul>
</div>
