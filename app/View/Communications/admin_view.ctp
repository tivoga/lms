<div class="communications view">
<h2><?php echo __('Communication'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($communication['Communication']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($communication['User']['username'], array('controller' => 'users', 'action' => 'view', $communication['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($communication['Communication']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Message'); ?></dt>
		<dd>
			<?php echo h($communication['Communication']['message']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact'); ?></dt>
		<dd>
			<?php echo $this->Html->link($communication['Contact']['FirstName'], array('controller' => 'contacts', 'action' => 'view', $communication['Contact']['Id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Communication'), array('action' => 'edit', $communication['Communication']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Communication'), array('action' => 'delete', $communication['Communication']['id']), array(), __('Are you sure you want to delete # %s?', $communication['Communication']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Communications'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Communication'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
