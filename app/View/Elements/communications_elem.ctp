<?php
$allEntities = array();

foreach ($contact['Communication'] as $k => $v) {
    $allEntities[] = $v;
}
foreach ($contact['Email'] as $k => $v) {
    $allEntities[] = $v;
}

if (sizeof($allEntities) > 1) {
    $allEntities = Set::sort($allEntities, '/created', 'DESC');
}
//debug($allEntities);
?>
<script>
    $(document).ready(function() {
        $( "#Interactions" ).change(function() {
            if($( "#Interactions option:selected" ).attr('value')>0){
                var int_show = $( "#Interactions option:selected" ).text();
                $(".Interaction").hide();
                $('.'+int_show).show();
            }else if ($( "#Interactions option:selected" ).attr('value')=="0"){
                $(".Interaction").hide();
            }else{
                $(".Interaction").show();
            }

        });
        $( "#emails" ).change(function() {
            $(".Email").toggle();
        });
    });
</script>
<br>
<div class="clearfix">
    <div class="col-md-5">
        <?php echo $this->Html->link(__('Send Email'), array('controller'=>'Emails','action' => 'add',
            'contact_id' => $contact['Contact']['Id'] ), array('role' =>'button', 'class'=>'btn btn-success')); ?>
        <!--        <a role="button" href="#" class="btn btn-success">Send an Email</a>-->
        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#addInteraction">
            Record Interaction
        </button>
    </div>
    <div class="col-md-3">
        <div class="input-group">
<!--      <div class="input-group-addon" style="min-width:0px;"></div>-->
            <label class="form-control">Show emails
                <input id="emails" type="checkbox" checked>
            </label>
        </div>
        </div>

        <div class="col-md-4">
            <fieldset>
                <?php
                $ctypes = $communication_types;
                array_unshift($ctypes,'None');
                echo $this->Form->input('Interaction_Categories',array('id'=>'Interactions','options'=>$ctypes,'empty'=>'Show All')); ?>
            </fieldset>
    </div>
</div>

<div id="addInteraction" class="collapse">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo 'Add An Interaction'; ?></h3>
        </div>
        <div class="panel-body">
            <?php echo $this->Form->create('Communication', array('url' => array('controller' =>'Communications' ,'action' => 'add_communication_to_contact'))); ?>
            <fieldset>
                <?php
                echo $this->Form->input('user_id',array('type'=>'hidden', 'default'=> AuthComponent::user('id')));
                echo $this->Form->input('title');
                echo $this->Form->input('message',array('label'=>'Notes'));
                echo $this->Form->input('category',array('options'=>$communication_types));
                echo $this->Form->input('contact_id', array('type'=>'hidden', 'default' =>$contact['Contact']['Id']));
                ?>
            </fieldset>
            <?php echo $this->Form->end(__('Add Interaction')); ?>
        </div>
    </div>


</div>
<div class="panel-group" id="comm_accordion">
    <?php foreach ($allEntities as $key=>$template): ?>
        <div class="panel panel-default <?php if (array_key_exists('subject',$template)){
            echo 'Email';}
        else{echo 'Interaction ';
            if($template['category'] != Null){
                echo $communication_types[$template['category']];
            }
        }
        ?>">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#comm_accordion" href="<?php echo '#collapse_comm'.$key; ?>">
                        <div class="clearfix">
                            <div class="pull-left">
                            <?php if (array_key_exists('subject',$template)){
                                echo 'Email : '.  $template['subject'];}
                                else{echo 'Interaction : '.$template['title'];}
                            ?>
                            </div>
                            <div class="pull-right">
                                <?php echo 'Added On : '.  $template['created'];?>
                                <?php echo 'Added By : '.  $template['User']['username'];?>
                            </div>
                        </div>
                    </a>
                </h4>
            </div>
            <div id="<?php echo 'collapse_comm'.$key; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                    <?php
                    if (array_key_exists('body',$template)){
                        echo $template['body'];}
                    else{echo $template['message'];}
                    if (array_key_exists('UploadedFile',$template)){
                        echo '<p> Files Uploaded:</p>';
                        foreach ($template['UploadedFile'] as $file) {
                            echo $file['filename'];
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>