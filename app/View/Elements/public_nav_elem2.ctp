<nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php echo $this->Html->link(
                'Lead Management',
                '/', array('escape' => false,'class'=>'navbar-brand')
            );
            ?>
<!--            <a class="navbar-brand" href="#">Sevron Lead Management</a>-->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contacts <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li >
                            <?php echo $this->Html->link("Add New Contact",
                                array('controller' => 'Contacts', 'action' => 'add','plugin'=>null,'admin'=>false),
                                array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                        </li>
                        <li class="divider"></li>
                        <li >
                            <?php echo $this->Html->link("My Main Contacts",
                                array('controller' => 'Contacts', 'action' => 'mainContacts','plugin'=>null,'admin'=>false),
                                array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link("My Archived Contacts",
                                array('controller' => 'Contacts', 'action' => 'archives','plugin'=>null,'admin'=>false),
                                array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                        </li>
                        <li class="divider"></li>
                        <li >
                            <?php echo $this->Html->link("All Main Contacts",
                                array('controller' => 'Contacts', 'action' => 'mainContacts', 'true','plugin'=>null,'admin'=>false),
                                array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                        </li>
                        <li >
                            <?php echo $this->Html->link("All Archived Contacts",
                                array('controller' => 'Contacts', 'action' => 'archives','true','plugin'=>null,'admin'=>false),
                                array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                        </li>
                        <li class="divider"></li>
                        <?php if($this->Session->read('Auth.User.is_admin')){
                            echo '<li >'.$this->Html->link("UnSegregated Contacts",
                                    array('controller' => 'Contacts', 'action' => 'unsegregated','plugin'=>null,'admin'=>false),
                                    array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")).'</li>';
                        } ?>
                    </ul>

                </li>
                <li ><?php echo $this->Html->link(
                        "Live Leads",
                        array('plugin'=>null,'controller'=>'Contacts','admin'=>false,'action'=>'index'),
                        array('escape' => false, 'tabindex' => '-1')
                    );?>
                </li>

                <li ><?php echo $this->Html->link(
                        "Proximity Map",
                        array('plugin'=>null,'controller'=>'Contacts','admin'=>false,'action'=>'proximity'
                        ), array('escape' => false, 'tabindex' => '-1')
                    );?></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">General Reports <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <?php echo $this->Html->link(
                                "Lead Reports",
                                array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                            );?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(
                                "Renewal Report",
                                array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'renewal_reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                            );?>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?php echo $this->Html->link(
                                "Invoicing Report",
                                array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'inv_reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                            );?></li>
                        <li>
                            <?php echo $this->Html->link(
                                "Cancellations Report",
                                array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'cancel_reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                            );?></li>


                    </ul>
                </li>
                <li><?php echo $this->Html->link(
                        "Financial Reports",
                        array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'financials'), array('escape' => false, 'tabindex' => '-1')
                    );?></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if($this->Session->read('Auth.User.is_admin')) : ?>
                        <?php echo $this->element('admin_nav_elem2');?>
                <?php endif; ?>
<!--                <li><a href="#">Link</a></li>-->
                <li>
                    <?php echo $this->Html->link(
                        'Hello '.AuthComponent::user('username'),
                        array('admin'=>false,'controller'=>'users','action'=>'edit','plugin' => null,AuthComponent::user('id')), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                    );
                    ?>
                </li>
                <li ><?php echo $this->Html->link(
                        'Log out',
                        '/logout', array('plugin'=>null,'admin'=>false,'escape' => false, 'tabindex' => '-1')
                    );?></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>