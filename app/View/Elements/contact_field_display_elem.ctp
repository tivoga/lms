<script type="application/javascript">
    $('#main_tab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
    $('#addresses_tab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })
</script>
<script>
    $(function() {
        $( "#anniversary" ).datepicker({ dateFormat: "yy-mm-dd" });
    });
    $(function() {
        $( "#birthday" ).datepicker({ dateFormat: "yy-mm-dd" });
    });
</script>

<ul id="main_tab" class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#primary_c" role="tab" data-toggle="tab">Primary Contact</a></li>
    <li><a href="#contact_m" role="tab" data-toggle="tab">Contact Management Information</a></li>
    <li><a href="#add_contact" role="tab" data-toggle="tab">Additional Contact 1</a></li>
    <li><a href="#addresses" role="tab" data-toggle="tab">Addresses</a></li>
    <li><a href="#faxes" role="tab" data-toggle="tab">Fax Numbers</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="primary_c">
        <fieldset>
            <p></p>
            <?php
            if($edit){echo $this->Form->input('Id');}
            echo $this->Form->input('LastName');
            echo $this->Form->input('FirstName');
            echo $this->Form->input('JobTitle');
            echo $this->Form->input('Company');
            echo $this->Form->input('Email');
            echo $this->Form->input('Phone1');
            ?>
        </fieldset>
    </div>
    <div class="tab-pane" id="contact_m">
        <fieldset>
            <p></p>
            <?php
            if($this->Session->read('Auth.User.is_admin')){
                echo $this->Form->input('user_id',array('label'=>'Account Manager','empty'=>'Select a User'));
                echo $this->Form->input('group_id',array('empty'=>'Select a Group'));
            }
            ?>
        </fieldset>
    </div>
    <div class="tab-pane" id="add_contact">
        <fieldset>
            <p></p>
            <?php
            echo $this->Form->input('firstName2');
            echo $this->Form->input('lastName2');
            echo $this->Form->input('Phone2');
            echo $this->Form->input('EmailAddress2');

            echo $this->Form->input('firstName3');
            echo $this->Form->input('lastName3');
            echo $this->Form->input('Phone3');
            echo $this->Form->input('EmailAddress3');

            ?>
        </fieldset>
    </div>
    <div class="tab-pane" id="addresses">
        <p></p>
        <ul id="adresses_tab" class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#site_a" role="tab" data-toggle="tab">Site Address</a></li>
            <li><a href="#billing_a" role="tab" data-toggle="tab">Billing Address</a></li>
            <li><a href="#other_a" role="tab" data-toggle="tab">Other Address</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="site_a">
                <fieldset>
                    <p></p>
                    <?php
                    echo $this->Form->input('Address1Type');
                    echo $this->Form->input('StreetAddress1');
                    echo $this->Form->input('StreetAddress2');
                    echo $this->Form->input('City');
                    echo $this->Form->input('State');
                    echo $this->Form->input('Country');
                    echo $this->Form->input('ZipFour1');
                    echo $this->Form->input('PostalCode');
                    ?>
                </fieldset>
            </div>
            <div class="tab-pane " id="billing_a">
                <fieldset>
                    <p></p>
                    <?php
                    echo $this->Form->input('Address2Type');
                    echo $this->Form->input('Address2Street1');
                    echo $this->Form->input('Address2Street2');
                    echo $this->Form->input('City2');
                    echo $this->Form->input('State2');
                    echo $this->Form->input('Country2');
                    echo $this->Form->input('ZipFour2');
                    echo $this->Form->input('PostalCode2');
                    ?>
                </fieldset>
            </div>
            <div class="tab-pane " id="other_a">
                <fieldset>
                    <p></p>
                    <?php
                    echo $this->Form->input('Address3Type');
                    echo $this->Form->input('Address3Street1');
                    echo $this->Form->input('Address3Street2');
                    echo $this->Form->input('City3');
                    echo $this->Form->input('State3');
                    echo $this->Form->input('Country3');
                    echo $this->Form->input('ZipFour3');
                    echo $this->Form->input('PostalCode3');
                    ?>
                </fieldset>

            </div>
        </div>

    </div>
    <div class="tab-pane" id="faxes">
        <fieldset>
            <p></p>
            <?php
            echo $this->Form->input('Fax1');
            //        echo $this->Form->input('Fax1Type');
            echo $this->Form->input('Fax2');
            //        echo $this->Form->input('Fax2Type');
            ?>
        </fieldset>
    </div>
</div>