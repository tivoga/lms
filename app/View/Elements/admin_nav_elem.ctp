<a id="drop5" role="button" data-toggle="dropdown" href="#"><span><div class="dropdown">Admin Pages</div></span></a>
<ul id="menu2" class="dropdown-menu nav" role="menu" aria-labelledby="drop5">
<!--    <li role="presentation">--><?php //echo $this->Html->link(
//            "<span><div id='nav_admin_home'>Admin Home</div></span>",
//            array('admin'=>true,'plugin'=>null,'controller'=>'Contacts','action'=>'index'),array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
//        );?><!--</li>-->
    <li role="presentation"><?php echo $this->Html->link(
            "<span><div id='nav_admin_users'>Users</div></span>",
            array('admin'=>true,'controller'=>'users','action'=>'index','plugin' => null), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
        );?></li>
    <li role="presentation"><?php echo $this->Html->link(
            "<span><div id='nav_user_groups'>User Groups</div></span>",
            array('admin'=>true,'controller'=>'groups','action'=>'index','plugin' => null), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
        );?></li>
    <li role="presentation"><?php echo $this->Html->link(
            "<span><div id='nav_permissions'>Permissions</div></span>",
            array('admin'=>true,'controller'=>'acl','action'=>'index','plugin' => null), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
        );?></li>
    <li role="presentation" ><?php echo $this->Html->link(
            "<span><div id='nav_products'>Email Templates</div></span>",
            array('admin'=>true,'plugin'=>null,'controller'=>'EmailTemplates', 'action'=>'index'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
        );?></li>
    <li role="presentation" ><?php echo $this->Html->link(
            "<span><div id='nav_products'>File Uploads</div></span>",
            array('admin'=>true,'plugin'=>null,'controller'=>'UploadedFiles', 'action'=>'index'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
        );?></li>
    <li role="presentation" ><?php echo $this->Html->link(
            "<span><div id='nav_products'>Products</div></span>",
            array('admin'=>true,'plugin'=>null,'controller'=>'Products', 'action'=>'index'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
        );?></li>
    <li role="presentation" ><?php echo $this->Html->link(
            "<span><div id='nav_seg_rules'>Segregation Rules</div></span>",
            array('admin'=>true,'plugin'=>null, 'controller'=>'SegregationRules','action'=>'index'),
            array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
        );?></li>
<!--    <li role="presentation" >--><?php //echo $this->Html->link(
//            "<span><div id='nav_home'>Front End</div></span>",
//            '/', array('escape' => false, 'tabindex' => '-1')
//        );?><!--</li>-->
<!--    <li role="presentation" >--><?php //echo $this->Html->link(
//            '<span><div id="nav_logOut">Log out</div></span>',
//            array('admin'=>false,'controller'=>'Contacts','action'=>'logout'), array('escape' => false, 'tabindex' => '-1')
//        );?><!--</li>-->
</ul>