<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $address_title?$address_title:__('Address'.$add_num ).h($contact['Address2Type']); ?></h3>
    </div>
    <div class="panel-body">
        <?php echo h($contact['Address'.$add_num.'Street1']); ?>
        &nbsp;
        <?php echo h($contact['Address'.$add_num.'Street2']); ?>
        &nbsp;
        <?php echo h($contact['State'.$add_num]); ?>
        &nbsp;
        <?php echo h($contact['City'.$add_num]); ?>
        &nbsp;
        <?php echo h($contact['Country'.$add_num]); ?>
        &nbsp;
        <?php echo h($contact['PostalCode'.$add_num]); ?>
        &nbsp;
        <?php echo h($contact['ZipFour'.$add_num]); ?>
        &nbsp;
    </div>
</div>