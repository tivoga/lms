<div id="segregation_referrence">
    <h3>Infusionsoft Field References</h3>
    <div class="row">
        <div class="col-md-6">
            <p> Select a field to view the available options</p>
    <select size="11" id="seg_rules_field">
        <?php foreach ($fields as $field): ?>
            <option><?php echo h($field['CustomField']['name']); ?></option>
        <?php endforeach; ?>
    </select>
        </div>
        <div class="col-md-6">
    <p>Available field options</p>
    <textarea id="seg_reff_field_values" rows="10"></textarea>
    </div>
    </div>
</div>
<script>
    $("#seg_rules_field").change(function() {
        var fields= <?php echo json_encode($fields)?>;
        var text = $('#seg_rules_field option:selected').text();
        for (var i =0; i< fields.length; i++){
            if (fields[i].CustomField.name == text){
                $('#seg_reff_field_values').val(fields[i].CustomField.Values);
            }
        }
    });
</script>