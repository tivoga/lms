<ul id="navigation" class="nav nav-pills">

    <li>
        <a id="drop5" role="button" data-toggle="dropdown" href="#"><span><div class="dropdown">Contacts</div></span></a>
        <ul id="menu2" class="dropdown-menu nav" role="menu" aria-labelledby="drop5">
            <li role="presentation">
            <?php echo $this->Html->link("<span><div id='nav_new_cont'>Add New Contact</div></span>",
                    array('controller' => 'Contacts', 'action' => 'add','plugin'=>null,'admin'=>false),
                    array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                </li>
            <li role="presentation">
            <?php echo $this->Html->link("<span><div id='nav_main_cont'>My Main Contacts</div></span>",
                    array('controller' => 'Contacts', 'action' => 'mainContacts','plugin'=>null,'admin'=>false),
                    array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                </li>
            <li role="presentation">
            <?php echo $this->Html->link("<span><div id='nav_arch_cont'>My Archived Contacts</div></span>",
                    array('controller' => 'Contacts', 'action' => 'archives','plugin'=>null,'admin'=>false),
                    array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
            </li>
            <li role="presentation">
            <?php echo $this->Html->link("<span><div id='nav_all_main'>All Main Contacts</div></span>",
                    array('controller' => 'Contacts', 'action' => 'mainContacts', 'true','plugin'=>null,'admin'=>false),
                    array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                </li>
            <li role="presentation">
            <?php echo $this->Html->link("<span><div id='nav_all_arch'>All Archived Contacts</div></span>",
                    array('controller' => 'Contacts', 'action' => 'archives','true','plugin'=>null,'admin'=>false),
                    array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")); ?>
                </li>
                <?php if($this->Session->read('Auth.User.is_admin')){
                    echo '<li role="presentation">'.$this->Html->link("<span><div id='nav_reports'>UnSegregated Contacts</div></span>",
                        array('controller' => 'Contacts', 'action' => 'unsegregated','plugin'=>null,'admin'=>false),
                        array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")).'</li>';
                } ?>
        </ul>
    </li>

    <li ><?php echo $this->Html->link(
            "<span><div id='nav_leads'>Live Leads</div></span>",
            array('plugin'=>null,'controller'=>'Contacts','admin'=>false,'action'=>'index'),
            array('escape' => false, 'tabindex' => '-1')
        );?>
    </li>

<!--    <li >--><?php //echo $this->Html->link(
//            "<span><div id='nav_archives'>Contacts List</div></span>",
//            array('plugin'=>null,'controller'=>'Contacts','admin'=>false,'action'=>'mainContacts'
//            ), array('escape' => false, 'tabindex' => '-1')
//        );?><!--</li>-->

    <li ><?php echo $this->Html->link(
            "<span><div id='nav_proximity'>Proximity Map</div></span>",
            array('plugin'=>null,'controller'=>'Contacts','admin'=>false,'action'=>'proximity'
            ), array('escape' => false, 'tabindex' => '-1')
        );?></li>
    <li>
        <a id="drop4" role="button" data-toggle="dropdown" href="#"><span><div class="dropdown">General Reports</div></span></a>
        <ul id="menu1" class="dropdown-menu nav" role="menu" aria-labelledby="drop4">
            <li role="presentation">
                <?php echo $this->Html->link(
                    "<span><div id='nav_reports'>Lead Report</div></span>",
                    array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                );?>
            </li>
            <li role="presentation">
                <?php echo $this->Html->link(
                    "<span><div id='nav_renew_reports'>Renewal Report</div></span>",
                    array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'renewal_reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                );?>
            </li>
            <li role="presentation">
                <?php echo $this->Html->link(
                    "<span><div id='nav_inv_reports'>Invoice Report</div></span>",
                    array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'inv_reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                );?></li>
            <li role="presentation">
                <?php echo $this->Html->link(
                    "<span><div id='nav_cancel_reports'>Cancellations Report</div></span>",
                    array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'cancel_reports'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
                );?></li>
        </ul>
    </li>

    <li><?php echo $this->Html->link(
            "<span><div id='nav_financials'>Financial Reports</div></span>",
            array('plugin'=>null,'admin'=>false,'controller'=>'Contacts','action'=>'financials'), array('escape' => false, 'tabindex' => '-1')
        );?></li>
    <?php if($this->Session->read('Auth.User.is_admin')) : ?>
        <li >
            <?php                         echo $this->element('admin_nav_elem');    ?>
<!--            --><?php //echo $this->Html->link(
//                '<span><div id="nav_admin">Admin Pages</div></span>',
//                array('controller'=>'Contacts','action'=>'index','admin'=>true), array('escape' => false, 'tabindex' => '-1')
//            );?>
        </li>
        <!--                            <li>--><?php //echo $this->Html->link(__d('users', 'List Users'), array('admin' => true, 'action'=>'index'));?><!--</li>-->
    <?php endif; ?>

    <li ><?php echo $this->Html->link('<span><div id="nav_logOut">Log out</div></span>',
            '/logout', array('plugin'=>null,'admin'=>false,'escape' => false, 'tabindex' => '-1')
        );?></li>

</ul>