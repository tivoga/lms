<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo __('Address1 ').h($contact['Address1Type']); ?></h3>
    </div>
    <div class="panel-body">
        <?php echo h($contact['StreetAddress1']); ?>
        &nbsp;
        <?php echo h($contact['StreetAddress2']); ?>
        &nbsp;
        <?php echo h($contact['State']); ?>
        &nbsp;
        <?php echo h($contact['City']); ?>
        &nbsp;
        <?php echo h($contact['Country']); ?>
        &nbsp;
        <?php echo h($contact['PostalCode']); ?>
        &nbsp;
        <?php echo ($contact['ZipFour1']); ?>
        &nbsp;
    </div>
</div>
