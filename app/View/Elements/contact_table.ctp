
<script type="application/javascript">
    $(document).ready(function() {
        $('#<?php echo $table_id; ?>').datatable({
            "order": []
        });
    } );
</script>

<table id="<?php echo $table_id; ?>" class="table hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Phone1</th>
        <th>Company</th>
        <th>ContactType</th>
        <th>Email</th>
        <th>User Group</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($contacts as $contact): ?>
        <tr>
            <td><?php echo $this->Html->link( h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id'])); ?></td>
            <td><?php echo h($contact['Contact']['LastName']); ?></td>
            <td><?php echo h($contact['Contact']['Phone1']); ?></td>
            <td><?php echo h($contact['Contact']['Company']); ?></td>
            <td><?php echo h($contact['Contact']['ContactType']); ?></td>
            <td><?php echo h($contact['Contact']['Email']); ?></td>
            <td><?php echo h($contact['Contact']['user_group']); ?></td>
            <!--td class="actions">
<!--                            --><?php //echo $this->Html->link(__('View'), array('action' => 'view', $contact['Contact']['Id'])); ?>
<!--                            --><?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $contact['Contact']['Id'])); ?>
<!--                            --><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contact['Contact']['Id']), array(), __('Are you sure you want to delete # %s?', $contact['Contact']['Id'])); ?>
<!--                        </td>-->
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>