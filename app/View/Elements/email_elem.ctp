<?php echo $this->Html->script('tinymce/tinymce.min.js');?>
<script type="text/javascript">
    tinyMCE.init({
//        theme : 'advanced',
        mode : "textareas",
        convert_urls : false,
        resize: "both",
        plugins: [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen autoresize",
            "insertdatetime  table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link"
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
    $("#email_template").change(function() {
        var fields= <?php echo json_encode($templates)?>;
        var text = $('#email_template option:selected').text();
        for (var i =0; i< fields.length; i++){
            if (fields[i].EmailTemplate.subject == text.trim()){
                tinyMCE.activeEditor.setContent(fields[i].EmailTemplate.body);
                $('#email_subject').val(fields[i].EmailTemplate.subject);
                break;
            }
        }
    });
    });
</script>

<?php echo $this->Form->create('Email'); ?>
<fieldset>
    <!--        --><?php //echo $templates[0]['subject']; ?>
    <div>
        <label for="email_template" class="input-group-addon">Email Template</label>
        <div class="input select">
            <select id="email_template">
                <option value="" disabled selected>Select a Template (Selecting a new tempate will discard all changes)</option>
                <?php foreach ($templates as $template): ?>
                    <option>
                        <?php echo $template['EmailTemplate']['subject']; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <!--        <p>Selecting a new template will discard all changes</p>-->
    <p></p>
    <br>
    <br>
    <!--        --><?php //debug($templates)?>

    <?php
    echo $this->Form->input('subject', array('id' => 'email_subject'));
    echo $this->Form->input('body', array('id' => 'email_body'));
    echo $this->Form->input('user_id',array('type' =>'hidden', 'default' => $this->Session->read('Auth.User')['id']));
    echo $this->Form->input('contact_id', array('type' => 'hidden','default' => $this->params->named['contact_id']));
    echo $this->Form->input('UploadedFile');
    ?>
</fieldset>
<div class="clearfix">
    <div class="pull-left">
        <?php echo $this->Form->submit('Send Email',
            array('class' => 'btn btn-success', 'title' => 'Save changes')
        ); ?>
    </div>
    <div class="pull-left" style="margin-left: 10px;">
        <div class="submit">
            <?php //debug($this->data['Contact']['Id']);
            echo $this->Html->link(__('Cancel Changes'), array('controller' =>'Contacts','action' => 'view',
                    $this->params['named']['contact_id'])
                , array('role' =>'button', 'class'=>'btn btn-danger')); ?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<p></p>