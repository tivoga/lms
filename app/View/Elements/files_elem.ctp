<br>
<!--<a role="button" href="#" class="btn btn-success"></a>-->
<button type="button" class="btn btn-success" data-toggle="collapse" data-target="#addFile">
    Upload new File
</button>
<div id="addFile" class="collapse">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo 'Add new File'; ?></h3>
        </div>
        <div class="panel-body">
            <?php echo $this->Form->create('UploadedFile', array('class'=>'inline_form','type'=>'file',
                'url' => array('controller' =>'UploadedFiles' ,'action' => 'addFile')
            )); ?>
            <!--    <legend>--><?php //echo __('Upload a New File'); ?><!--</legend>-->
            <div class="clearfix">

                <div class="pull-left">
                    <fieldset>
                        <?php echo $this->Form->input('filename',array('type' => 'textfield','label' =>'File Name')); ?>
                        <?php echo $this->Form->input('file',array('type' => 'file','label' =>'Select File to Upload')); ?>
                        <?php echo $this->Form->input('contact_id',array('type' => 'hidden','default' =>$contact['Contact']['Id'])); ?>
                    </fieldset>
                </div>
                <div class="pull-right">
                    <?php echo $this->Form->end(__('Upload File')); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php //echo $this->Html->link(__('Upload New File'), array('controller'=>'UploadedFiles','action' => 'addFile',
//    'contact_id' => $contact['Contact']['Id'] ), array('role' =>'button', 'class'=>'btn btn-success')); ?>
        <dl class="dl-horizontal dl-freestyle" style="padding-top: 10px;">
    <?php foreach ($contact['UploadedFile'] as $key=>$file): ?>
<!--        --><?php //debug($file['location'])?>
            <dt><?php echo $this->Html->link($file['filename'],
                    array('controller'=>'UploadedFiles','action' => 'downloadFile',$file['id'],'contact_id' => $contact['Contact']['Id'])
                ); ?></dt>
<!--            <dt>--><?php //echo $file['filename']?><!--</dt>-->
            <dd><div class="clearfix">
                <div class="pull-left">
                    <b>Added on : </b> <?php echo $file['created']?>&nbsp;&nbsp;&nbsp;&nbsp;
                    <b>Added by: </b> <?php echo $file['User']['username']?>
                </div>
                    <div class="pull-right">
                        <?php echo $this->Html->link("Delete",
                            array('controller'=>'UploadedFiles','action' => 'deleteFile',$file['id'],'contact_id' => $contact['Contact']['Id']),
                            array('escape' => false, 'role' =>'button', 'class'=>'btn btn-xs btn-danger')); ?>
                    </div>


                </div> </dd>
        <p/>
            <?php endforeach; ?>
        </dl>
