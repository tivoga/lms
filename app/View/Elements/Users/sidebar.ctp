<div class="actions">
	<ul>
		<?php if (!$this->Session->read('Auth.User.id')) : ?>
			<li><?php echo $this->Html->link(__( 'Login'), array('action' => 'login')); ?></li>
            <?php if (!empty($allowRegistration) && $allowRegistration)  : ?>
			<li><?php echo $this->Html->link(__( 'Register an account'), array('action' => 'add')); ?></li>
            <?php endif; ?>
		<?php else : ?>
<!--			<li>--><?php //echo $this->Html->link(__( 'Logout'), array('action' => 'logout')); ?>
			<li><?php echo $this->Html->link(__( 'My Account'), array('action' => 'edit')); ?>
			<li><?php echo $this->Html->link(__( 'Change password'), array('action' => 'change_password')); ?>
		<?php endif ?>
		<?php if($this->Session->read('Auth.User.is_admin')) : ?>
            <li>&nbsp;</li>
            <li><?php echo $this->Html->link(__( 'List Users'), array('admin' => true, 'action'=>'index'));?></li>
        <?php endif; ?>
	</ul>
</div>
