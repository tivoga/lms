<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin Pages <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li><?php echo $this->Html->link(
                "Users",
                array('admin'=>true,'controller'=>'users','action'=>'index','plugin' => null), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
            );?></li>
        <li><?php echo $this->Html->link(
                "User Groups",
                array('admin'=>true,'controller'=>'groups','action'=>'index','plugin' => null), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
            );?></li>
        <li class="divider"></li>
        <li><?php echo $this->Html->link(
                "Permissions",
                array('admin'=>true,'controller'=>'acl','action'=>'index','plugin' => null), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
            );?></li>
        <li class="divider"></li>
        <li ><?php echo $this->Html->link(
                "Email Templates",
                array('admin'=>true,'plugin'=>null,'controller'=>'EmailTemplates', 'action'=>'index'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
            );?></li>
        <li ><?php echo $this->Html->link(
                "Products",
                array('admin'=>true,'plugin'=>null,'controller'=>'Products', 'action'=>'index'), array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
            );?></li>
        <li ><?php echo $this->Html->link(
                "Segregation Rules",
                array('admin'=>true,'plugin'=>null, 'controller'=>'SegregationRules','action'=>'index'),
                array('escape' => false, 'tabindex' => '-1', 'role'=>"menuitem")
            );?></li>
    </ul>
</li>