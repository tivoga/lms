
<script type="application/javascript">
    $(document).ready(function() {
        var table = $('#<?php echo $table_id; ?>').DataTable({
            "processing": true,
//            responsive: true,
            "ajax": {
                "url": "contacts/index.json",
                "dataSrc": "contacts"
            },
            "deferRender": true,
            "order": [],
            "aoColumns": [
                {mData: "Contact.FirstName"},
//                {mData: "Contact.LastName"},
                {mData: "Contact.Phone1"},
                {mData: "Contact.Company"},
//                {mData: "Contact.ContactType"},
                {mData: "Contact.Email"},
                {mData: "customfield"}
            ],
            "fnCreatedRow": function(nRow, aData, iDataIndex){
                var source = '';
                if(aData.customfield.length){
                    for (i = 0; i < aData.customfield.length; i++) {
                        if( aData.customfield[i].customfield.name == 'FormSubmitted'){
                            source = aData.customfield[i].value;
                        }
                    }
                }
                $('td:eq(0)', nRow).html(
                    '<a href="./Contacts/view/'+aData.Contact.Id+'">'+aData.Contact.FirstName +' '+ aData.Contact.LastName+'</a>');
                $('td:eq(4)', nRow).html(source);
            }
        });
        setInterval( function () {
            table.ajax.reload();
        }, 300000 );
    } );
</script>

<table id="<?php echo $table_id; ?>" class="table hover table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Name</th>
<!--        <th>LastName</th>-->
        <th>Phone</th>
        <th>Company</th>
<!--        <th>ContactType</th>-->
        <th>Email</th>
        <th>Contact Source</th>
    </tr>
    </thead>
    <tbody>
<!--    --><?php //foreach ($contacts as $contact): ?>
<!--        <tr>-->
<!--            <td>--><?php //echo $this->Html->link( h($contact['Contact']['FirstName']), array('action' => 'view', $contact['Contact']['Id'])); ?><!--</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['LastName']); ?><!--</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['Phone1']); ?><!--</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['Company']); ?><!--</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['ContactType']); ?><!--</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['Email']); ?><!--</td>-->
<!--            <td>--><?php //echo h($contact['Contact']['user_group']); ?><!--</td>-->
            <!--td class="actions">
<!--                            --><?php //echo $this->Html->link(__('View'), array('action' => 'view', $contact['Contact']['Id'])); ?>
<!--                            --><?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $contact['Contact']['Id'])); ?>
<!--                            --><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contact['Contact']['Id']), array(), __('Are you sure you want to delete # %s?', $contact['Contact']['Id'])); ?>
<!--                        </td>-->
<!--        </tr>-->
<!--    --><?php //endforeach; ?>
    </tbody>
</table>