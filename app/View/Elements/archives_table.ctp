<script type="application/javascript">
    $(document).ready(function() {
        $('.groupedcontact').hide();
        $('.expander-button').on('click', function(){
//            $('.groupedcontact').hide();
            var id= this.getAttribute('id');
            $('.'+id+'group').toggle();
            $(this).toggleClass('shown');
        });
        $('#grouparchive').on('click', function(){
            var names = [];
            $('input:checked').each(function() {
                names.push(this.id);
            });
            if(names.length){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo $this->Html->url(array("action" => "archive_contact"));?>',
                    data: {
                        id: names
                    },
                    complete: function () {
                        window.location.reload(true);
                    }
                });
            }
//            console.log(names);
        })
        var table = $('#contacts_table').dataTable({
            "iDisplayLength": 100,
//            "processing": true,
//            "deferRender": true,
            "order": []
        });
    } );
</script>
<div class="bottom-buffer-10">
    <button id="grouparchive" type="button" class="btn btn-primary" value="Archive selected">Archive Selected</button>
</div>
<br>

<table id="contacts_table" cellpadding="0" cellspacing="0" class="table hover table-bordered">
    <thead>
    <tr>
        <th></th>
        <th><?php echo ('Select'); ?></th>
        <th><?php echo ('Name'); ?></th>
        <th><?php echo ('Phone'); ?></th>
        <th><?php echo ('Company'); ?></th>
        <th><?php echo ('Email'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($contacts as $contact): ?>
        <?php
        //debug($contact);
        if($contact['Contact']['group_id'] and empty($groups)){
            echo $this->element('display_group_elem',array('contact'=>$contact));
        }else{
            echo $this->element('display_row_elem',array('contact'=>$contact, 'group'=>empty($groups)?false:empty($groups)));
        } ?>
    <?php endforeach; ?>
    </tbody>
</table>
<p>
<!--    --><?php //echo $this->element('paging')?>
<!--    --><?php //echo $this->element('pagination')?>
