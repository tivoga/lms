<br>
<?php echo $this->Html->link(__('Add New Product'), array('controller'=>'Products','action' => 'add_product_to_contact',
    'contact_id' => $contact['Contact']['Id'] ), array('role' =>'button', 'class'=>'btn btn-success')); ?>
<div class="panel-group" id="prod_accordion">
    <?php foreach ($contact['Product'] as $key=>$product): ?>
    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"">
            <a data-toggle="collapse" data-parent="#prod_accordion" href="<?php echo '#collapse_prod'.$key; ?>">
                <?php echo $product['name'];?>
            </a>
            </h4>
            <div class=" pull-right">
                <?php echo $this->Html->link("Edit",
                    array('controller'=>'Products','action' => 'edit_product_values', $product['id'],
                    'contact_id' => $contact['Contact']['Id'] ),
                    array('escape' => false, 'role' =>'button', 'class'=>'btn btn-success btn-xs',
                        'title' => 'Edit Product Fields')); ?>
                <?php echo $this->Html->link("Delete",
                    array('controller'=>'Products','action' => 'delete_product_from_contact', $product['id'],
                        'contact_id' => $contact['Contact']['Id'] ),
                    array('escape' => false, 'role' =>'button', 'class'=>'btn btn-danger btn-xs',
                        'title' => 'Delete this Product')); ?>
            </div>
        </div>
<?php //debug($product); ?>
        <div id="<?php echo 'collapse_prod'.$key; ?>" class="panel-collapse collapse">
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Contract Status</dt>
                    <dd><?php echo $product['ContactsProduct']['contract_status'];?></dd>
                    <dt><?php echo $product['is_service']?'Purchase date':'Contract Start'?></dt>
                    <dd><?php echo $product['ContactsProduct']['contract_start'];?></dd>
                    <?php if(!$product['is_service']):?>
                        <dt>Contract period in Years </dt>
                        <dd><?php echo $product['ContactsProduct']['contract_period'];?></dd>
                        <dt>Contract End </dt>
                        <dd><?php echo $product['ContactsProduct']['contract_end'];?></dd>
                    <?php endif ?>
                    <dt>Sales Person </dt>
                    <dd><?php echo $product['ContactsProduct']['sales_person'];?></dd>
                    <dt>Notes </dt>
                    <dd><?php echo $product['ContactsProduct']['notes'];?></dd>
                    <dt>Price Quoted </dt>
                    <dd><?php echo $product['ContactsProduct']['contract_quoted'];?></dd>
                    <dt>Price Paid </dt>
                    <dd><?php echo $product['ContactsProduct']['contract_paid'];?></dd>
                </dl>

                <dl class="dl-horizontal">
                <?php foreach ($contact['ProductFieldsValue'] as $key=>$product_value): ?>
                    <?php if($product_value['ProductField']['product_id'] != $product['id'] ) continue;?>
                    <dt><?php echo $product_value['ProductField']['label'];?></dt>
                    <dd><?php echo $product_value['value'];?></dd>
                <?php endforeach; ?>
                </dl>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>