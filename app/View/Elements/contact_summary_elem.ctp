<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Contact Summary:</h3>
    </div>
    <div class="panel-body">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Contact Name:</h3>
            </div>
            <div class="panel-body">
                <p><b>
                        <?php echo h($contact['Title']).' '; ?>
                        <?php echo h($contact['FirstName']).' '; ?>
                        <?php echo h($contact['MiddleName']).' '; ?>
                        <?php echo h($contact['LastName']).' '; ?>
                        <?php echo h($contact['Suffix']).', ';
                        if ($contact['JobTitle']){
                            echo h($contact['JobTitle']);
                        }?>
                    </b>
                </p>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Company:</h3>
            </div>
            <div class="panel-body">
                <?php echo h($contact['Company']);?>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Phone</h3>
            </div>
            <div class="panel-body">
                <?php echo $this->element('phone_display_elem', array('contact' => $contact, 'phone' =>'Phone1'));?>
            </div>
        </div>
        <?php echo $this->element('addres1_elem', array('contact' => $contact, 'address_title'=>'Site Address'));?>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Account Manager</h3>
            </div>
            <div class="panel-body">
                <?php  echo $user['username']?$user['username']:'None Assigned';?>
            </div>
        </div>
    </div>
</div>
<!--<div class="contact_summary" style="padding-top: 50px;">-->
<!--<dt>--><?php //echo __('Primary Email'); ?><!--</dt>-->
<!--<dd>-->
<!--    --><?php //echo h($contact['Email']); ?>
    &nbsp;
<!--</dd>-->
<?php //echo $this->element('phone_display_elem', array('contact' => $contact, 'phone' =>'Phone1'));?>
<?php //echo $this->element('addres1_elem', array('contact' => $contact));?>
<!--</div>-->