<div class="productFieldsValues index">
	<h2><?php echo __('Product Fields Values'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('value'); ?></th>
			<th><?php echo $this->Paginator->sort('product_field_id'); ?></th>
			<th><?php echo $this->Paginator->sort('contact_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($productFieldsValues as $productFieldsValue): ?>
	<tr>
		<td><?php echo h($productFieldsValue['ProductFieldsValue']['id']); ?>&nbsp;</td>
		<td><?php echo h($productFieldsValue['ProductFieldsValue']['value']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($productFieldsValue['ProductField']['id'], array('controller' => 'product_fields', 'action' => 'view', $productFieldsValue['ProductField']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($productFieldsValue['Contact']['FirstName'], array('controller' => 'contacts', 'action' => 'view', $productFieldsValue['Contact']['Id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $productFieldsValue['ProductFieldsValue']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $productFieldsValue['ProductFieldsValue']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $productFieldsValue['ProductFieldsValue']['id']), array(), __('Are you sure you want to delete # %s?', $productFieldsValue['ProductFieldsValue']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Product Fields Value'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
