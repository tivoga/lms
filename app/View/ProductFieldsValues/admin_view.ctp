<div class="productFieldsValues view">
<h2><?php echo __('Product Fields Value'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($productFieldsValue['ProductFieldsValue']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($productFieldsValue['ProductFieldsValue']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product Field'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productFieldsValue['ProductField']['id'], array('controller' => 'product_fields', 'action' => 'view', $productFieldsValue['ProductField']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contact'); ?></dt>
		<dd>
			<?php echo $this->Html->link($productFieldsValue['Contact']['FirstName'], array('controller' => 'contacts', 'action' => 'view', $productFieldsValue['Contact']['Id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Product Fields Value'), array('action' => 'edit', $productFieldsValue['ProductFieldsValue']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Product Fields Value'), array('action' => 'delete', $productFieldsValue['ProductFieldsValue']['id']), array(), __('Are you sure you want to delete # %s?', $productFieldsValue['ProductFieldsValue']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Fields Values'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Fields Value'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
