<div class="productFieldsValues form">
<?php echo $this->Form->create('ProductFieldsValue'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Product Fields Value'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('value');
		echo $this->Form->input('product_field_id');
		echo $this->Form->input('contact_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProductFieldsValue.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProductFieldsValue.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Product Fields Values'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
