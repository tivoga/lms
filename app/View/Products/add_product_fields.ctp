<div class="products form">
<?php echo $this->Form->create('Product'); ?>
	<fieldset>
		<legend><?php echo __('Add Product To Contact'); ?></legend>
	<?php
//		echo $this->Form->input('id');
//		echo $this->Form->input('name');
//		echo $this->Form->input('description');
//		echo $this->Form->input('product_type_id');
//		echo $this->Form->input('Contact');
        echo $this->Form->input('Product_id',array('label' => 'Select a Product'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
<?php if (isset($product)): ?>
<!--        debug($product);-->
    <?php echo $this->Form->create('Add_Product_Fields',array('action' =>'add_product_fields')); ?>
    <legend><?php echo __('Add Product Fields'); ?></legend>
    <fieldset>
    <?php foreach ($product['ProductField'] as $field): ?>
            <?php
            echo $this->Form->input($field['label'],array('label' =>$field['label'], 'type' =>'textarea'));
//            echo $this->Form->input('Product_id',array('label' => 'Select a Product'));
            ?>
    <?php endforeach ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
<?php endif ?>

</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Products'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Product Types'), array('controller' => 'product_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Type'), array('controller' => 'product_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
	</ul>
</div>
