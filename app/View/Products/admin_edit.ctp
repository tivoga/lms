<div class="products form wid90">
<?php echo $this->Form->create('Product'); ?>
	<fieldset>
		<legend><?php echo __('Edit Product'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('product_type_id');
        echo $this->Form->input('is_service');
    //		echo $this->Form->input('Contact');
	?>
	</fieldset>
<?php //debug( $this->data); ?>
<?php echo $this->Form->end(__('Save Changes')); ?>

    <div class="">
        <?php echo $this->Form->create('ProductField', array(
            'url' => array('controller' => 'ProductFields', 'action' => 'admin_add_from_products'),
        )); ?>
        <fieldset>
            <legend><?php echo __('Add Product Field'); ?></legend>
            <?php
            echo $this->Form->input('label');
            echo $this->Form->input('pro_id', array('type'=>'hidden', 'default'=>$this->data['Product']['id']));
            ?>
        </fieldset>
        <?php echo $this->Form->end(__('Add Field')); ?>
    </div>
    <div>
        <?php if (!empty($this->data['ProductField'])): ?>
            <table cellpadding = "0" cellspacing = "0">
                <tr>
                    <!--                <th>--><?php //echo __('Id'); ?><!--</th>-->
                    <th><?php echo __('Label'); ?></th>
                    <th><?php echo __('Created'); ?></th>
                    <!--                <th>--><?php //echo __('Product Id'); ?><!--</th>-->
                    <th class="actions actions_in_view"><?php echo __('Actions'); ?></th>
                </tr>
                <?php foreach ($this->data['ProductField'] as $productField): ?>
                    <tr>
                        <!--                    <td>--><?php //echo $productField['id']; ?><!--</td>-->
                        <td><?php echo $productField['label']; ?></td>
                        <td><?php echo $productField['created']; ?></td>
                        <!--                    <td>--><?php //echo $productField['product_id']; ?><!--</td>-->
                        <td class="actions">
                            <!--                        --><?php //echo $this->Html->link(__('View'), array('controller' => 'product_fields', 'action' => 'view', $productField['id'])); ?>
                            <?php echo $this->Html->link(__('Edit'), array('controller' => 'product_fields', 'action' => 'edit', $productField['id'])); ?>
                            <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_fields', 'action' => 'delete', $productField['id']), array(), __('Are you sure you want to delete # %s?', $productField['id'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>



</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!---->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Products'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Types'), array('controller' => 'product_types', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Type'), array('controller' => 'product_types', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
