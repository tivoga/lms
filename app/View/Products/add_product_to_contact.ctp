<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">-->
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
        $( "#datepicker2" ).datepicker({ dateFormat: "yy-mm-dd" });
    });
</script>
<div class="products form wid90">
<?php echo $this->Form->create('Product'
//    array('class' => 'form-horizontal bsform',
//    'inputDefaults' => array(
//        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
//        'label' => array('class' => 'control-label col-sm-3'),
//        'input' =>array('class' =>'form-control col-sm-9'),
//        'between' => '<div class="col-sm-9">',
//        'after' => '</div>',
//        'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
//    )
//    )
); ?>
	<fieldset>
		<h2><?php echo __('Add Product To Contact'); ?></h2>
<!--    <div>-->
        <?php
//		echo $this->Form->input('id');
//		echo $this->Form->input('name');
//		echo $this->Form->input('description');
//		echo $this->Form->input('product_type_id');
//		echo $this->Form->input('Contact');

        echo $this->Form->input('Product_id',array(
            'label' => array('text'=>'Select a Product'),
            ));
	?>
<!--        </div>-->
	</fieldset>
<?php echo $this->Form->end(__('Load Custom Fields')); ?>
    <p></p>
<?php if (isset($product)): ?>
<!--        -->
    <?php echo $this->Form->create('Product_values'
//        array('class' => 'form-horizontal bsform',
//            'inputDefaults' => array(
//                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
//                'between' => '<div class="col-sm-9">',
//                'after' => '</div>',
//                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
//            ))
    ); ?>
<!--    <legend>--><?php //echo __('Add Product Fields'); ?><!--</legend>-->
    <h3><?php echo __($product['Product']['name']); ?></h3>
    <p><?php echo __($product['Product']['description']); ?></p>

        <fieldset>
    <h2><?php echo $product['Product']['is_service']?'Service Information':__('Contract Data'); ?></h2>
    <?php

    echo $this->Form->input('product_id',array('default' =>$product['Product']['id'], 'type' =>'hidden'));
    echo $this->Form->input('c_status',array('type' =>'select', 'label'=>$product['Product']['is_service']?'Service Status':'Contract Status', 'options' =>$contract_status_enum));
    echo $this->Form->input('c_start',array('type' =>'text', 'id' =>'datepicker', 'label' =>array('text' =>$product['Product']['is_service']?'Sale Date':'Contract Start Date')));
    if(!$product['Product']['is_service']){
        echo $this->Form->input('c_end',array('type' =>'text', 'id' =>'datepicker2', 'label' =>array('text' =>'Contract End Date')));
        echo $this->Form->input('c_length',array('type' =>'select', 'label' =>array('text' =>'Contract length in years'), 'options' => $contract_length_enum));
    }
    echo $this->Form->input('c_quoted',array('type' =>'number', 'label' =>array('text' =>'Price Quoted')));
    echo $this->Form->input('c_paid',array('type' =>'number', 'label' =>'Price Paid'));
//    debug($sales_persons);
    echo $this->Form->input('c_salesPerson',array('type'=>'select','label' =>'Sales Person','options'=>$sales_persons));
    echo $this->Form->input('c_notes',array('type' =>'textarea', 'label' =>array('text' =>'Notes')));
    //            echo $this->Form->input('Product_id',array('label' => 'Select a Product'));
        ?>
    <div class="clearfix"></div>

    <p><br></p>
    <?php if(!$product['Product']['is_service'] and $product['ProductField']) :?>
    <h2><?php echo __('Product Data'); ?></h2>
    <?php foreach ($product['ProductField'] as $field): ?>
<!--    <div class="form-group">-->
    <?php
            echo $this->Form->input($field['id'],array('label'=>array('text' =>$field['label']), 'type' =>'textbox'));
//            echo $this->Form->input('Product_id',array('label' => 'Select a Product'));
            ?>
<!--        </div>-->
    <?php endforeach ?>
    <?php endif?>
    </fieldset>

    <div class="clearfix">
        <div class="pull-left">
            <?php echo $this->Form->submit('Add Product/Service',
                array('class' => 'btn btn-success', 'title' => 'Save changes')
            ); ?>
        </div>
        <div class="pull-left" style="margin-left: 10px;">
            <div class="submit">
                <?php
                echo $this->Html->link(__('Cancel'), array('controller' =>'Contacts','action' => 'view',
                        $this->params['named']['contact_id'])
                    , array('role' =>'button', 'class'=>'btn btn-danger')); ?>
            </div>
        </div>
    </div>
<?php echo $this->Form->end(); ?>
    <p></p>
<?php else: ?>
    <?php echo $this->Html->link(__('Cancel'), array('controller' =>'Contacts','action' => 'view',
            $this->params['named']['contact_id'])
        , array('role' =>'button', 'class'=>'btn btn-danger')); ?>
<?php endif ?>
</div>
<!--<div class="actions_set">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--    <ul class="nav nav-pills nav-stacked">-->

<!---->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Products'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Types'), array('controller' => 'product_types', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Type'), array('controller' => 'product_types', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->