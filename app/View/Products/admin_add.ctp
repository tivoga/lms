<div class="wid90">
<?php echo $this->Form->create('Product'); ?>
	<fieldset>
		<legend><?php echo __('Create Product'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description');
		echo $this->Form->input('product_type_id');
		echo $this->Form->input('is_service');
        echo $this->Form->input('user_id',array('type' =>'hidden', 'default' => $this->Session->read('Auth.User')['id']));
//		echo $this->Form->input('Contact');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Create Product')); ?>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->

<!--		<li>--><?php //echo $this->Html->link(__('List Products'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Types'), array('controller' => 'product_types', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Type'), array('controller' => 'product_types', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
