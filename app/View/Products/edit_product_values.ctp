<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script>
    $(function() {
        $( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" } );
    });
</script>
<div class="products form wid90">
<?php echo $this->Form->create('Product'
//    array('class' => 'form-horizontal bsform',
//        'inputDefaults' => array(
//            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
//            'before'=>'<div class="form-group">',
//            'label' => array('class' => 'control-label col-sm-3'),
//            'input' =>array('class' =>'form-control col-sm-9'),
//            'between' => '<div class="col-sm-9">',
//            'after' => '</div></div>',
//            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
//        ))
); ?>
	<fieldset>
		<h2><?php echo __('Edit Product'); ?></h2>
        <p><?php
            echo 'Product :';
            echo($product['Product']['name']); ?></p>
        <?php
        echo '<p> Contact :';
        foreach ($product['Contact'] as $cust){
            if ($cust['Id'] == $this->params['named']['contact_id']){
                echo $cust['FirstName'].' '.$cust['LastName'];
                echo "<p> Company: ".$cust['Company']."</p>";
                break;
            }
        }
        echo '</p>';
        ?>
		<h2><?php echo __('Contract Data'); ?></h2>
        <?php
        echo $this->Form->input('c_status',
            array('type' =>'select', 'label'=>'Contract Status',
                'options' =>$contract_status_enum,
                'value' => $contract['ContactsProduct']['contract_status']
            ));
        echo $this->Form->input('c_start', array(
            'type' =>'text','id'=>'datepicker',
            'label' =>$product['Product']['is_service']?'Sale Date':'Contract Start Date',
            'value' => $contract['ContactsProduct']['contract_start']
        ));
        if(!$product['Product']['is_service']){
            echo $this->Form->date('c_end', array(
//                'type' =>'text','id'=>'datepicker',
                'label' =>'Contract End Date',
                'value' => $contract['ContactsProduct']['contract_end']
            ));
            echo $this->Form->input('c_length',
                array('type' =>'select', 'label' =>'Contract length in years',
                    'options' => $contract_length_enum,
                    'value' => $contract['ContactsProduct']['contract_period']));
        }
        echo $this->Form->input('c_quoted',array(
            'type' =>'number', 'label' =>'Quoted Price',
            'value' => $contract['ContactsProduct']['contract_quoted']
        ));
        echo $this->Form->input('c_paid',array(
            'type' =>'number', 'label' =>'Price Paid',
            'value' => $contract['ContactsProduct']['contract_paid']
        ));
        echo $this->Form->input('c_salesPerson',array('type'=>'select','label' =>'Sales Person','options'=>$sales_persons));
//        echo $this->Form->input('c_salesPerson',array(
//            'type' =>'text', 'label' =>array('text' =>'Sales Person'),
//            'value' => $contract['ContactsProduct']['sales_person']
//        ));
        echo $this->Form->input('c_notes',array(
            'type' =>'textarea', 'label' =>array('text' =>'Notes'),
            'value' => $contract['ContactsProduct']['notes']
        ));
        //            echo $this->Form->input('Product_id',array('label' => 'Select a Product'));
        ?>
        <p></p>
        <h2><?php echo __('Product Data'); ?></h2>

        <?php
        foreach ($this->request->data['values'] as $elem){
		    echo $this->Form->input($elem['ProductFieldsValue']['id'],array(
                'label' =>$elem['ProductField']['label'], 'type' =>'textfield',
                'value' =>$elem['ProductFieldsValue']['value']
            ));
        }
	?>
	</fieldset>
    <br>
    <div class="clearfix">
        <div class="pull-left">
            <?php echo $this->Form->submit('Save Changes',
                array('class' => 'btn btn-success', 'title' => 'Save changes')
            ); ?>
        </div>
        <div class="pull-left" style="margin-left: 10px;">
            <div class="submit">
            <?php
            echo $this->Html->link(__('Cancel'), array('controller' =>'Contacts','action' => 'view',
            $this->params['named']['contact_id'])
            , array('role' =>'button', 'class'=>'btn btn-danger')); ?>
        </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<!--<div class="actions_set">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--        <li>--><?php //echo $this->Html->link(__('Cancel'), array('controller' =>'Contacts','action' => 'view',
//                $this->params['named']['contact_id'])
//            , array('role' =>'button', 'class'=>'btn btn-success')); ?>
<!--        </li>-->

<!--		<li>--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Products'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Types'), array('controller' => 'product_types', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Type'), array('controller' => 'product_types', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
