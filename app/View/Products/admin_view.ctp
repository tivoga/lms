<div class="products view wid90">
<h2><?php echo __('Product Information'); ?></h2>
    <div class="">
	<dl class="dl-horizontal">
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($product['Product']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($product['Product']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($product['Product']['created']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Created By'); ?></dt>
        <dd>
            <?php echo h($product['CreatedBy']['username']); ?>
            &nbsp;
        </dd>
		<dt><?php echo __('Product Type'); ?></dt>
		<dd>
			<?php echo $product['ProductType']['name']; ?>
<!--			--><?php //echo $this->Html->link($product['ProductType']['name'], array('controller' => 'product_types', 'action' => 'view', $product['ProductType']['id'])); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Is Service'); ?></dt>
        <dd>
            <?php echo h($product['Product']['is_service'] ? 'Yes' : 'No'); ?>
            &nbsp;
        </dd>
	</dl>
    </div>
    <h3><?php echo __('Product Fields'); ?></h3>
    <?php if (!empty($product['ProductField'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
<!--                <th>--><?php //echo __('Id'); ?><!--</th>-->
                <th><?php echo __('Label'); ?></th>
                <th><?php echo __('Created'); ?></th>
<!--                <th>--><?php //echo __('Product Id'); ?><!--</th>-->
                <th class="actions actions_in_view"><?php echo __('Actions'); ?></th>
            </tr>
            <?php foreach ($product['ProductField'] as $productField): ?>
                <tr>
<!--                    <td>--><?php //echo $productField['id']; ?><!--</td>-->
                    <td><?php echo $productField['label']; ?></td>
                    <td><?php echo $productField['created']; ?></td>
<!--                    <td>--><?php //echo $productField['product_id']; ?><!--</td>-->
                    <td class="actions">
<!--                        --><?php //echo $this->Html->link(__('View'), array('controller' => 'product_fields', 'action' => 'view', $productField['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('controller' => 'product_fields', 'action' => 'edit', $productField['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_fields', 'action' => 'delete', $productField['id']), array(), __('Are you sure you want to delete # %s?', $productField['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>

<!--    <div class="actions">-->
<!--        <ul>-->
<!--            <li>--><?php //echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?><!-- </li>-->
<!--        </ul>-->
<!--    </div>-->

</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('Edit Product'), array('action' => 'edit', $product['Product']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete Product'), array('action' => 'delete', $product['Product']['id']), array(), __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Products'), array('action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product'), array('action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Types'), array('controller' => 'product_types', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Type'), array('controller' => 'product_types', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Product Fields'), array('controller' => 'product_fields', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Product Field'), array('controller' => 'product_fields', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
<!--<div class="related">-->
<!--</div>-->

