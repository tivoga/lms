<div class="leads index">
<?php print("<pre>".print_r($contact,true)."</pre>"); ?>
	<h2><?php echo __('Leads'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('Id'); ?></th>
			<th><?php echo $this->Paginator->sort('OpportunityTitle'); ?></th>
			<th><?php echo $this->Paginator->sort('Leadsource'); ?></th>
			<th><?php echo $this->Paginator->sort('Objection'); ?></th>
			<th><?php echo $this->Paginator->sort('ProjectedRevenueLow'); ?></th>
			<th><?php echo $this->Paginator->sort('ProjectedRevenueHigh'); ?></th>
			<th><?php echo $this->Paginator->sort('OpportunityNotes'); ?></th>
			<th><?php echo $this->Paginator->sort('DateCreated'); ?></th>
			<th><?php echo $this->Paginator->sort('EstimatedCloseDate'); ?></th>
			<th><?php echo $this->Paginator->sort('NextActionDate'); ?></th>
			<th><?php echo $this->Paginator->sort('NextActionNotes'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($leads as $lead): ?>
	<tr>
		<td><?php echo h($lead['Lead']['Id']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['OpportunityTitle']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['Leadsource']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['Objection']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['ProjectedRevenueLow']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['ProjectedRevenueHigh']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['OpportunityNotes']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['DateCreated']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['EstimatedCloseDate']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['NextActionDate']); ?>&nbsp;</td>
		<td><?php echo h($lead['Lead']['NextActionNotes']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $lead['Lead']['Id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $lead['Lead']['Id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $lead['Lead']['Id']), array(), __('Are you sure you want to delete # %s?', $lead['Lead']['Id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Lead'), array('action' => 'add')); ?></li>
	</ul>
</div>
