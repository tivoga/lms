<div class="leads form">
<?php echo $this->Form->create('Lead'); ?>
	<fieldset>
		<legend><?php echo __('Add Lead'); ?></legend>
	<?php
		echo $this->Form->input('OpportunityTitle');
		echo $this->Form->input('ContactID');
		echo $this->Form->input('AffiliateId');
		echo $this->Form->input('UserID');
		echo $this->Form->input('StageID');
		echo $this->Form->input('StatusID');
		echo $this->Form->input('Leadsource');
		echo $this->Form->input('Objection');
		echo $this->Form->input('ProjectedRevenueLow');
		echo $this->Form->input('ProjectedRevenueHigh');
		echo $this->Form->input('OpportunityNotes');
		echo $this->Form->input('DateCreated');
		echo $this->Form->input('LastUpdated');
		echo $this->Form->input('LastUpdatedBy');
		echo $this->Form->input('CreatedBy');
		echo $this->Form->input('EstimatedCloseDate');
		echo $this->Form->input('NextActionDate');
		echo $this->Form->input('NextActionNotes');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Leads'), array('action' => 'index')); ?></li>
	</ul>
</div>
