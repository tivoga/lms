<div class="leads view">
<h2><?php echo __('Lead'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['Id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('OpportunityTitle'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['OpportunityTitle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ContactID'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['ContactID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AffiliateId'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['AffiliateId']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('UserID'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['UserID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('StageID'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['StageID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('StatusID'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['StatusID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Leadsource'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['Leadsource']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Objection'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['Objection']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ProjectedRevenueLow'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['ProjectedRevenueLow']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ProjectedRevenueHigh'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['ProjectedRevenueHigh']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('OpportunityNotes'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['OpportunityNotes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DateCreated'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['DateCreated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LastUpdated'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['LastUpdated']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('LastUpdatedBy'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['LastUpdatedBy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CreatedBy'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['CreatedBy']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('EstimatedCloseDate'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['EstimatedCloseDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NextActionDate'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['NextActionDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NextActionNotes'); ?></dt>
		<dd>
			<?php echo h($lead['Lead']['NextActionNotes']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Lead'), array('action' => 'edit', $lead['Lead']['Id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Lead'), array('action' => 'delete', $lead['Lead']['Id']), array(), __('Are you sure you want to delete # %s?', $lead['Lead']['Id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Leads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lead'), array('action' => 'add')); ?> </li>
	</ul>
</div>
