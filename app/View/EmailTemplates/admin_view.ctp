<?php echo $this->Html->script('tinymce/tinymce.min.js');?>
<script type="text/javascript">
    tinyMCE.init({
//        theme : 'advanced',
        mode : "textareas",
        convert_urls : false,
        readonly: 1
    });
</script>
<div class="emailTemplates view wid90">
<h2><?php echo __('Email Template'); ?></h2>
	<dl class="dl-horizontal dl-freestyle" >
<!--		<dt>--><?php //echo __('Id'); ?><!--</dt>-->
<!--		<dd>-->
<!--			--><?php //echo h($emailTemplate['EmailTemplate']['id']); ?>
<!--			&nbsp;-->
<!--		</dd>-->
		<dt><?php echo __('Template Name / Subject:'); ?></dt>
		<dd>
			<?php echo h($emailTemplate['EmailTemplate']['subject']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Body'); ?></dt>
		<dd><textarea>
			<?php echo h($emailTemplate['EmailTemplate']['body']); ?>
            </textarea>
            &nbsp;
		</dd>
		<dt><?php echo __('Created By'); ?></dt>
		<dd>
			<?php echo $this->Html->link($emailTemplate['User']['username'], array('controller' => 'users', 'action' => 'view', $emailTemplate['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($emailTemplate['EmailTemplate']['created']); ?>
			&nbsp;
		</dd>
	</dl>
    <?php echo $this->Html->link(__('Edit Email Template'), array('action' => 'edit', $emailTemplate['EmailTemplate']['id'])
        , array('role' =>'button', 'class'=>'btn btn-success')); ?>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('Edit Email Template'), array('action' => 'edit', $emailTemplate['EmailTemplate']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete Email Template'), array('action' => 'delete', $emailTemplate['EmailTemplate']['id']), array(), __('Are you sure you want to delete # %s?', $emailTemplate['EmailTemplate']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Email Templates'), array('action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Email Template'), array('action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
