<?php echo $this->Html->script('tinymce/tinymce.min.js');?>
<script type="text/javascript">
    tinyMCE.init({
//        theme : 'advanced',
        mode : "textareas",
        convert_urls : false,
        resize: 'both',
        plugins: [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen autoresize",
            "insertdatetime  table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link"

    });
</script>
<div class="emailTemplates form wid90">
<?php echo $this->Form->create('EmailTemplate'); ?>
	<fieldset>
		<legend><?php echo __('Create Email Template'); ?></legend>
	<?php
		echo $this->Form->input('subject',array('label'=>'Template Name/Subject'));
		echo $this->Form->input('body');
		echo $this->Form->input('user_id',array('type' =>'hidden', 'default' => $this->Session->read('Auth.User')['id']));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Create New Template')); ?>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->

<!--		<li>--><?php //echo $this->Html->link(__('List Email Templates'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
