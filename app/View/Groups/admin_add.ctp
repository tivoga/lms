<?php //print("<pre>".print_r($contact,true)."</pre>");
$this->assign('linkId', 'nav_user_groups');
?>
<div class="groups form">
<?php echo $this->Form->create('Group'); ?>
	<fieldset>
		<legend><?php echo __('Create New Group'); ?></legend>
	<?php
		echo $this->Form->input('name');
        echo $this->Form->input('created_by',array('type' =>'hidden', 'default' => $this->Session->read('Auth.User')['id']));
    ?>
	</fieldset>
<?php echo $this->Form->end(__('Create Group')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
