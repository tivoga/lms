<div class="uploadedFiles view wid90">
<h2><?php echo __('Uploaded File'); ?></h2>
	<dl>
<!--		<dt>--><?php //echo __('Id'); ?><!--</dt>-->
<!--		<dd>-->
<!--			--><?php //echo h($uploadedFile['UploadedFile']['id']); ?>
<!--			&nbsp;-->
<!--		</dd>-->
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($uploadedFile['User']['username'], array('controller' => 'users', 'action' => 'view', $uploadedFile['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Uploaded On'); ?></dt>
		<dd>
			<?php echo h($uploadedFile['UploadedFile']['created']); ?>
			&nbsp;
		</dd>
<!--		<dt>--><?php //echo __('Location'); ?><!--</dt>-->
<!--		<dd>-->
<!--			--><?php //echo h($uploadedFile['UploadedFile']['location']); ?>
<!--			&nbsp;-->
<!--		</dd>-->
<!--		<dt>--><?php //echo __('Contact'); ?><!--</dt>-->
<!--		<dd>-->
<!--			--><?php //echo $this->Html->link($uploadedFile['Contact']['FirstName'], array('controller' => 'contacts', 'action' => 'view', $uploadedFile['Contact']['Id'])); ?>
<!--			&nbsp;-->
<!--		</dd>-->
	</dl>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('Edit Uploaded File'), array('action' => 'edit', $uploadedFile['UploadedFile']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Form->postLink(__('Delete Uploaded File'), array('action' => 'delete', $uploadedFile['UploadedFile']['id']), array(), __('Are you sure you want to delete # %s?', $uploadedFile['UploadedFile']['id'])); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Uploaded Files'), array('action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Uploaded File'), array('action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Contacts'), array('controller' => 'contacts', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Contact'), array('controller' => 'contacts', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Emails'), array('controller' => 'emails', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Email'), array('controller' => 'emails', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
<!--<div class="related">-->
<!--	<h3>--><?php //echo __('Related Emails'); ?><!--</h3>-->
<!--	--><?php //if (!empty($uploadedFile['Email'])): ?>
<!--	<table cellpadding = "0" cellspacing = "0">-->
<!--	<tr>-->
<!--		<th>--><?php //echo __('Id'); ?><!--</th>-->
<!--		<th>--><?php //echo __('Subject'); ?><!--</th>-->
<!--		<th>--><?php //echo __('Body'); ?><!--</th>-->
<!--		<th>--><?php //echo __('User Id'); ?><!--</th>-->
<!--		<th>--><?php //echo __('Created'); ?><!--</th>-->
<!--		<th>--><?php //echo __('Contact Id'); ?><!--</th>-->
<!--		<th class="actions">--><?php //echo __('Actions'); ?><!--</th>-->
<!--	</tr>-->
<!--	--><?php //foreach ($uploadedFile['Email'] as $email): ?>
<!--		<tr>-->
<!--			<td>--><?php //echo $email['id']; ?><!--</td>-->
<!--			<td>--><?php //echo $email['subject']; ?><!--</td>-->
<!--			<td>--><?php //echo $email['body']; ?><!--</td>-->
<!--			<td>--><?php //echo $email['user_id']; ?><!--</td>-->
<!--			<td>--><?php //echo $email['created']; ?><!--</td>-->
<!--			<td>--><?php //echo $email['contact_id']; ?><!--</td>-->
<!--			<td class="actions">-->
<!--				--><?php //echo $this->Html->link(__('View'), array('controller' => 'emails', 'action' => 'view', $email['id'])); ?>
<!--				--><?php //echo $this->Html->link(__('Edit'), array('controller' => 'emails', 'action' => 'edit', $email['id'])); ?>
<!--				--><?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'emails', 'action' => 'delete', $email['id']), array(), __('Are you sure you want to delete # %s?', $email['id'])); ?>
<!--			</td>-->
<!--		</tr>-->
<!--	--><?php //endforeach; ?>
<!--	</table>-->
<?php //endif; ?>
<!---->
<!--	<div class="actions">-->
<!--		<ul>-->
<!--			<li>--><?php //echo $this->Html->link(__('New Email'), array('controller' => 'emails', 'action' => 'add')); ?><!-- </li>-->
<!--		</ul>-->
<!--	</div>-->
<!--</div>-->
