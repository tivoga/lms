<?php
class AddStatusFlagsForReports extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'this migrations adds flags to the contract
	if a renew mail has been sent, and if the contract has been invoiced and
	if, by whom and when.';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_field' => array(
                'Contacts_products' => array(
                    'renew_letter_sent' => array(
                        'type' => 'boolean'
                    ),'invoiced_by' => array(
                        'type' => 'string'
                    ),'invoiced_on' => array(
                        'type' => 'datetime'
                    ),'contract_end' => array(
                        'type' => 'date'
                    ),
                ),

            ),
		),
		'down' => array(
            'drop_field'=>array(
                'Contacts_products' => array(
                    'renew_letter_sent',
                    'invoiced_by',
                    'invoiced_on',
                    'contract_end'
                ),
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
