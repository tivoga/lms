<?php
class AddCustomDataFieldsToContact extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_field' => array(
                'Leads' => array(
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                ),
                'Contacts' => array(
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                )
            ),
            'create_table' => array(
                'custom_fields' => array(
                    'id' => array(
                        'type'    =>'integer',
                        'null'    => false,
                        'default' => null,
                        'key'     => 'primary'
                    ),
                    'name' => array(
                        'type'    =>'string',
                        'null'    => false,
                        'default' => null
                    ),
                    'values' => array(
                        'type'    =>'string',
                        'null'    => false,
                        'default' => null
                    ),
                    'type' => array(
                        'type'    =>'integer',
                        'null'    => false,
                        'default' => null
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                ),
                'custom_field_values' => array(
                    'id' => array(
                        'type'    => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'contactId' => array(
                        'type'    => 'integer',
                        'null'    => false,
                    ),
                    'custom_field_id' => array(
                        'type'    => 'integer',
                        'null'    => false
                    ),
                    'value' => array(
                        'type'    => 'text',
                        'default' => null
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        ),
                        'contactID' => array(
                            'column' => 'contactId'
                        )
                    )
                ),
                'segregation_rules' => array(
                    'id' => array(
                        'type'    => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'query' => array(
                        'type'    => 'text',
                        'default' => null
                    ),
                    'user_group' => array(
                        'type'    => 'integer',
                        'default' => null
                    ),
                    'over_ride' => array(
                        'type'    => 'boolean',
                        'default' => 0
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                )
            )
		),
		'down' => array(
            'drop_field' => array(
                'Leads' => array(
                    'indexes' => array('PRIMARY')
                ),
                'Contacts' => array(
                    'indexes' => array('PRIMARY' )
                )
            ),
            'drop_table' => array(
                'segregation_rules',
                'custom_field_values',
                'custom_fields'
            ),
		)
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
