<?php
class UserInSegregationRulesTitleInCommunicationsArchivedToContactsFilenameToUploadedFiles extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'alter_field' => array(
                'segregation_rules' => array(
                    'user_group' => array(
                        'type'=>'string'
                    )
                ),
            ),
            'create_field' => array(
                'communications' => array(
                    'title' => array(
                        'type' => 'string'
                    ),
                ),
                'Contacts' => array(
                    'archived' => array(
                        'type' => 'boolean'
                    ),
                ),
                'uploaded_files' => array(
                    'filename' => array(
                        'type' => 'string'
                    ),
                )
            )
        ),
		'down' => array(
            'drop_field' => array(
                'Contacts' => array(
                    'archived'
                ),
                'uploaded_files' => array(
                    'filename'
                ),
                'communications' => array(
                    'title'
                )
            ),
            'alter_field' => array(
                'segregation_rules' => array(
                    'user_group' => array(
                        'type'=>'integer'
                    )
                ),
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
