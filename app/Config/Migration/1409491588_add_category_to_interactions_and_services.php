<?php
class AddCategoryToInteractionsAndServices extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '
	Added:
	category to communications table
	senders_email and senders_email_password to users

	changed:
	Value in custom_fields to 8000 (255 length not
	enough for select options for industry for eg.)
	';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_field' => array(
                'communications' => array(
                    'category' => array(
                        'type' => 'string'
                    )
                ),
                'users'=>array(
                    'senders_email' => array(
                        'type' => 'string'
                    ),
                    'senders_email_password' => array(
                        'type' => 'string'
                    ),
                ),
            ),
            'alter_field' => array(
                'custom_fields' => array(
                    'Values' => array(
                        'length' => 8000
                    )
                )
            )
		),
		'down' => array(
            'drop_field'=>array(
                'communications' => array(
                    'category',
                ),
                'users'=>array(
                    'senders_email',
                    'senders_email_password',
                ),
            ),
            'alter_field' => array(
                'custom_fields' => array(
                    'Values' => array(
                        'length' => 255
                    )
                )
            )
        )
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
