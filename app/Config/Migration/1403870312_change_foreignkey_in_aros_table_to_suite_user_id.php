<?php
class ChangeForeignkeyInArosTableToSuiteUserId extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'alter_field' => array(
                'aros' => array(
                    'foreign_key' => array(
                        'type'=>'string', 'null' => false, 'default' => NULL,
                        'length' => 36,
                    )
                )
            )
		),
		'down' => array(
            'alter_field' => array(
                'aros' => array(
                    'foreign_key' => array(
                        'type'=>'integer', 'default' => NULL,
                        'length' => 10,
                    )
                )
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
