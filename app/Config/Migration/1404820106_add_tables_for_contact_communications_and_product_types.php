<?php
class AddTablesForContactCommunicationsAndProductTypes extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_table' => array(
                'product_types' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'name' => array(
                        'type' => 'string',
                    ),
                    'description' => array(
                        'type' => 'string',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                        'null' => True
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                ),
                'products' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'name' => array(
                        'type' => 'string',
                    ),
                    'description' => array(
                        'type' => 'string',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                        'null' => True
                    ),
                    'product_type_id' => array(
                        'type' => 'integer',

                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                ),
                'product_fields' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'label' => array(
                        'type' => 'string',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                    ),
                    'product_id' => array(
                        'type' => 'integer',
                        'null' => True,
                        'default' => null,
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        ),
                        'by_contact'=> array(
                            'column'=> 'product_id'
                        )
                    )
                ),
                'product_fields_values' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'value' => array(
                        'type' => 'text',
                    ),
                    'product_field_id' => array(
                        'type' => 'integer',
                    ),
                    'contact_id' => array(
                        'type' => 'integer',
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        ),
                        'by_contact'=> array(
                            'column'=> 'contact_id'
                        )
                    )
                ),
                'communications' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'user_id' => array(
                        'type' => 'string',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                        'null' => True
                    ),
                    'message' => array(
                        'type' => 'text',
                    ),
                    'contact_id' => array(
                        'type' => 'integer',
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        ),
                        'by_contact'=> array(
                            'column'=> 'contact_id'
                        )
                    )
                ),
                'email_templates' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'subject' => array(
                        'type' => 'string',
                    ),
                    'body' => array(
                        'type' => 'text',
                    ),
                    'user_id' => array(
                        'type' => 'string',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                        'null' => True
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                ),
                'emails' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'subject' => array(
                        'type' => 'string',
                    ),
                    'body' => array(
                        'type' => 'text',
                    ),
                    'user_id' => array(
                        'type' => 'string',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                        'null' => True
                    ),
                    'contact_id' => array(
                        'type' => 'integer',
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        ),
                        'by_contact'=> array(
                            'column'=> 'contact_id'
                        )
                    )
                ),
                'uploaded_files' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'user_id' => array(
                        'type' => 'string',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                        'null' => True
                    ),
                    'location' => array(
                        'type' => 'string'
                    ),
                    'contact_id' => array(
                        'type' => 'integer',
                    ),
                    'indexes' => array(
                          'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        ),
                        'by_contact'=> array(
                            'column'=>'contact_id'
                        )
                    )
                ),
                'emails_uploaded_files' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'email_id' => array(
                        'type' => 'integer',
                    ),
                    'uploaded_file_id' => array(
                        'type' => 'integer',
                    ),
                    'indexes' => array(
                        'by_emails' => array(
                            'column'=> 'email_id'
                        )
                    )
                ),
                'Contacts_products' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'contact_id' => array(
                        'type' => 'integer',
                    ),
                    'product_id' => array(
                        'type' => 'integer',
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique'=>1
                        ),
                        'by_contact'=>array(
                            'column'=>'contact_id'
                        )
                    )
                ),
            ),
		),
		'down' => array(
            'drop_table' => array(
                'product_types',
                'products',
                'product_fields',
                'product_fields_values',
                'communications',
                'email_templates',
                'emails',
                'uploaded_files',
                'emails_uploaded_files',
                'Contacts_products'
            ),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
