<?php
class AddGppriToContacts extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '
	This is to add a group primary flag to the contact
	';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_field' => array(
                'Contacts' => array(
                    'gp_primary' => array(
                        'type' => 'boolean',
                        'default'=>'1'
                    )
                ),
            ),
        ),
		'down' => array(
            'drop_field'=>array(
                'Contacts' => array(
                    'gp_primary',
                ),

            ),
        )
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
