<?php
class RenameUserGroupInSegRules extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'rename_field' => array(
                'segregation_rules' => array(
                    'user_group' => 'user_id'
                )
            )

        ),
		'down' => array(
            'rename_field' => array(
                'segregation_rules' => array(
                    'user_id' => 'user_group'
                )
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
