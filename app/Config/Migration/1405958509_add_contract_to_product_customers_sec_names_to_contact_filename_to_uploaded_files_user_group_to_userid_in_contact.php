<?php
class AddContractToProductCustomersSecNamesToContactFilenameToUploadedFilesUserGroupToUseridInContact extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'rename_field' => array(
                'Contacts' => array(
                    'user_group' => 'user_id'
                )
            ),
            'create_field' => array(
                'Contacts' => array(
                    'firstName2' => array(
                        'type' => 'string'
                    ),
                    'lastName2' => array(
                        'type' => 'string'
                    ),
                    'firstName3' => array(
                        'type' => 'string'
                    ),
                    'lastName3' => array(
                        'type' => 'string'
                    ),
                ),
                'Contacts_products' => array(
                    'contract_period' => array(
                        'type' => 'integer'
                    ),
                    'contract_start' => array(
                        'type' => 'date'
                    ),
                    'contract_status' => array(
                        'type' => 'string'
                    ),
                    'contract_quoted' => array(
                        'type' => 'float'
                    ),
                    'contract_paid' => array(
                        'type' => 'float'
                    ),
                ),
            ),
            'alter_field' => array(
                'Contacts' => array(
                    'user_id' => array(
                        'type'=>'string'
                    )
                ),
            ),
		),
		'down' => array(
            'rename_field' => array(
                'Contacts' => array(
                    'user_id' => 'user_group'
                )
            ),
            'alter_field' => array(
                'Contacts' => array(
                    'user_group' => array(
                        'type'=>'integer'
                    )
                )
            ),
            'drop_field'=>array(
                'Contacts' => array(
                    'firstName2',
                    'firstName3',
                    'lastName2',
                    'lastName3',
                ),
                'Contacts_products' => array(
                    'contract_period',
                    'contract_start',
                    'contract_status',
                    'contract_quoted',
                    'contract_paid'
                )
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
