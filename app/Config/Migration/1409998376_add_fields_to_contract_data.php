<?php
class AddFieldsToContractData extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '
	Added:
	is_service field to product
	';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_field' => array(
                'products' => array(
                    'is_service' => array(
                        'type' => 'boolean',
                        'default' => false,
                    )
                ),
                'Contacts_products'=>array(
                    'sales_person' => array(
                        'type' => 'string'
                    ),
                    'notes' => array(
                        'type' => 'string'
                    ),
                ),
            ),
		),
		'down' => array(
            'drop_field'=>array(
                'products' => array(
                    'is_service',
                ),
                'Contacts_products'=>array(
                    'sales_person',
                    'notes',
                ),
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
