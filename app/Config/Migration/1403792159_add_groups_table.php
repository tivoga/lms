<?php
class AddRolesTable extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_table' => array(
                'groups' => array(
                    'id' => array(
                        'type' => 'integer',
                        'null'    => false,
                        'key'     => 'primary'
                    ),
                    'name' => array(
                        'type' => 'string',
                    ),
                    'modified' => array(
                        'type' => 'datetime',
                    ),
                    'created' => array(
                        'type' => 'datetime',
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                ),
            ),
            'create_field' => array(
                'users' => array(
                    'group_id' => array(
                        'type' => 'integer'
                    ),
                )
            )
        ),
		'down' => array(
            'drop_table' => array(
                'groups'
            ),
            'drop_field' => array(
                'users' => array(
                    'group_id'
                )
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
