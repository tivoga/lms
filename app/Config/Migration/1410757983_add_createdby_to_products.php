<?php
class AddCreatedbyToProducts extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '
	adding created by fields for
	user,
	groups,
	segregation rules,
	products,
	product types
	';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_field' => array(
                'products' => array(
                    'user_id' => array(
                        'type' => 'string',
                    )
                ),
                'product_types'=>array(
                    'user_id' => array(
                        'type' => 'string'
                    ),
                ),
                'users'=>array(
                    'created_by' => array(
                        'type' => 'string'
                    ),
                ),
                'groups'=>array(
                    'created_by' => array(
                        'type' => 'string'
                    ),
                ),
                'segregation_rules'=>array(
                    'created_by' => array(
                        'type' => 'string'
                    ),
                ),
            ),
		),
		'down' => array(
            'drop_field'=>array(
                'products' => array(
                    'user_id',
                ),
                'Contacts_products'=>array(
                    'user_id',
                ),
                'users'=>array(
                    'created_by',
                ),
                'groups'=>array(
                    'created_by',
                ),
                'segregation_rules'=>array(
                    'created_by',
                ),
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
