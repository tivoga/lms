<?php
class ContactgroupsEtc extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'create_field' => array(
                'Contacts' => array(
                    'group_id' => array(
                        'type' => 'integer'
                    )
                ),

            ),
            'create_table' =>array(
                'contact_group' => array(
                    'id' => array(
                        'type'    =>'integer',
                        'null'    => false,
                        'default' => null,
                        'key'     => 'primary'
                    ),
                    'name' => array(
                        'type'    =>'string',
                        'null'    => false,
                        'default' => null
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
                ),
            )
		),
		'down' => array(
            'drop_table'=>array(
                'contact_group'
            ),
            'drop_field'=>array(
                'Contacts'=>array(
                    'group_id'
                )
            )
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
