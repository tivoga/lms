<?php
class RedoContactIdOnCustomFieldValues extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
            'drop_field' => array(
                'custom_field_values' => array(
                    'contactId'
                )
            ),
            'create_field' => array(
                'custom_field_values' => array(
                    'contactId' => array(
                        'type' => 'integer',
                        'null' => false
                    ),
                    'indexes' => array(
                        'contactID' => array(
                            'column' => 'contactId'
                        )
                    )
                )
            )
		),
		'down' => array(
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
