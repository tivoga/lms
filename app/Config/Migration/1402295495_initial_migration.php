<?php
class InitialMigration extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'Leads' => array(
					'Id' => array(
						type => integer,
                        'null'    => false,
                        'default' => null,
                        'key'     => 'primary'
					),
					'OpportunityTitle' => array(
						type => string,
					),
					'ContactID' => array(
						type => integer,
					),
					'AffiliateId' => array(
						type => integer,
					),
					'UserID' => array(
						type => integer,
					),
					'StageID' => array(
						type => integer,
					),
					'StatusID' => array(
						type => integer,
					),
					'Leadsource' => array(
						type => string,
					),
					'Objection' => array(
						type => string,
					),
					'ProjectedRevenueLow' => array(
						type => float,
					),
					'ProjectedRevenueHigh' => array(
						type => float,
					),
					'OpportunityNotes' => array(
						type => string,
					),
					'DateCreated' => array(
						type => datetime,
					),
					'LastUpdated' => array(
						type => timestamp,
					),
					'LastUpdatedBy' => array(
						type => integer,
					),
					'CreatedBy' => array(
						type => integer,
					),
					'EstimatedCloseDate' => array(
						type => datetime,
					),
					'NextActionDate' => array(
						type => datetime,
					),
					'NextActionNotes' => array(
						type => string,
                    ),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    )
				),
				'Contacts' => array(
					'Id' => array(
						type => integer,
                        'null'    => false,
                        'default' => null,
                        'key'     => 'primary'
					),
					'JobTitle' => array(
						type => string,
					),
					'LastName' => array(
						type => string,
					),
					'FirstName' => array(
						type => string,
					),
					'MiddleName' => array(
						type => string,
					),
					'Nickname' => array(
						type => string,
					),
					'OwnerID' => array(
						type => integer,
					),
					'Groups' => array(
						type => string,
					),
					'LastUpdated' => array(
						type => timestamp,
					),
					'LastUpdatedBy' => array(
						type => integer,
					),
					'Leadsource' => array(
						type => string,
					),
					'LeadSourceId' => array(
						type => integer,
					),
					'Phone1' => array(
						type => string,
					),
					'Phone1Ext' => array(
						type => string,
					),
					'Phone1Type' => array(
						type => string,
					),
					'Phone2' => array(
						type => string,
					),
					'Phone2Ext' => array(
						type => string,
					),
					'Phone2Type' => array(
						type => string,
					),
					'Phone3' => array(
						type => string,
					),
					'Phone3Ext' => array(
						type => string,
					),
					'Phone3Type' => array(
						type => string,
					),
					'Phone4' => array(
						type => string,
					),
					'Phone4Ext' => array(
						type => string,
					),
					'Phone4Type' => array(
						type => string,
					),
					'Phone5' => array(
						type => string,
					),
					'Phone5Ext' => array(
						type => string,
					),
					'Phone5Type' => array(
						type => string,
					),
					'PostalCode' => array(
						type => string,
					),
					'PostalCode2' => array(
						type => string,
					),
					'PostalCode3' => array(
						type => string,
					),
					'ReferralCode' => array(
						type => string,
					),
					'SpouseName' => array(
						type => string,
					),
					'State' => array(
						type => string,
					),
					'State2' => array(
						type => string,
					),
					'State3' => array(
						type => string,
					),
					'StreetAddress1' => array(
						type => string,
					),
					'StreetAddress2' => array(
						type => string,
					),
					'Suffix' => array(
						type => string,
					),
					'Title' => array(
						type => string,
					),
					'Username' => array(
						type => string,
					),
					'Validated' => array(
						type => string,
					),
					'Website' => array(
						type => string,
					),
					'Address1Type' => array(
						type => string,
					),
					'Address2Street1' => array(
						type => string,
					),
					'Address2Street2' => array(
						type => string,
					),
					'Address2Type' => array(
						type => string,
					),
					'Address3Street1' => array(
						type => string,
					),
					'Address3Street2' => array(
						type => string,
					),
					'Address3Type' => array(
						type => string,
					),
					'Anniversary' => array(
						type => date,
					),
					'AssistantName' => array(
						type => string,
					),
					'AssistantPhone' => array(
						type => string,
					),
					'BillingInformation' => array(
						type => string,
					),
					'Birthday' => array(
						type => date,
					),
					'City' => array(
						type => string,
					),
					'City2' => array(
						type => string,
					),
					'City3' => array(
						type => string,
					),
					'Company' => array(
						type => string,
					),
					'AccountId' => array(
						type => integer,
					),
					'CompanyID' => array(
						type => integer,
					),
					'ContactNotes' => array(
						type => string,
					),
					'ContactType' => array(
						type => string,
					),
					'Country' => array(
						type => string,
					),
					'Country2' => array(
						type => string,
					),
					'Country3' => array(
						type => string,
					),
					'CreatedBy' => array(
						type => integer,
					),
					'DateCreated' => array(
						type => datetime,
					),
					'Email' => array(
						type => string,
					),
					'EmailAddress2' => array(
						type => string,
					),
					'EmailAddress3' => array(
						type => string,
					),
					'Fax1' => array(
						type => string,
					),
					'Fax1Type' => array(
						type => string,
					),
					'Fax2' => array(
						type => string,
					),
					'Fax2Type' => array(
						type => string,
					),
					'ZipFour1' => array(
						type => string,
					),
					'ZipFour2' => array(
						type => string,
					),
					'ZipFour3' => array(
						type => string,
					),
                    'indexes' => array(
                        'PRIMARY' => array(
                            'column' => 'id',
                            'unique' => 1
                        )
                    ),
				)
			)
		),
		'down' => array(
			'drop_table' => array(
				'Leads',
				'Contacts',
			),
		)
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 */
	public function after($direction) {
		return true;
	}
}
